package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.AmbienteDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;

public abstract class AmbienteBC<E extends Ambiente> extends PatternBC<E> {

	protected AmbienteBC() {
	}

	@SuppressWarnings("rawtypes")
	public static AmbienteBC getInstance(Ambiente ambiente){
		if(ambiente instanceof CasaShow)
			return CasaShowBC.getInstance();
		else if(ambiente instanceof Cinema)
			return CinemaBC.getInstance();
		else
			return TeatroBC.getInstance();
	}

	@Override
	public long create(E object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = AmbienteDAO.getInstance(object).create(object);
		return result;
	}

	@Override
	public E retrieve(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(E object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = AmbienteDAO.getInstance(object).update(object);
		return result;
	}

	@Override
	public boolean delete(E object) {
		// TODO Auto-generated method stub
		boolean result = AmbienteDAO.getInstance(object).delete(object);
		return result;
	}

	@Override
	protected void validate(Ambiente object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Ambiente nulo" );
		}
		if( object.getNome() == null || object.getNome().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Ambiente n�o preenchido" );
		}
		if( object.getDescricao() == null || object.getDescricao().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Descricao do Ambiente n�o preenchida" );
		}
		if( object.getCapacidade() == 0 ) {
			throw new TicketsEventosBSIException( "Capacidade do Ambiente n�o preenchida" );
		} else if( object.getCapacidade() < 0 ) {
			throw new TicketsEventosBSIException( "Capacidade do Ambiente n�o pode ser negativa" );
		} else {
			int capacidadeSetores = 0;
			for( Setor setor : object.getSetores() ) {
				capacidadeSetores += setor.getCapacidade();
			}
			if( object.getCapacidade() != capacidadeSetores ) 
				throw new TicketsEventosBSIException( "A capacidade do Ambiente n�o � igual a soma das capacidades dos Setores do ambiente" );
		}
		EnderecoBC.getInstance().validate( object.getEndereco() );
		if( object.getSetores() == null || object.getSetores().size() == 0 ) {
			throw new TicketsEventosBSIException( "Ao menos 1 Setor dever� ser informado" );
		} else {
			for( Setor setor : object.getSetores() ) {
				SetorBC.getInstance().validate( setor );
			}
		}
	}
}