package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.ArtistaDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;

public class ArtistaBC extends PatternBC<Artista> {
	private static ArtistaBC instance;
	
	private ArtistaBC() {
	}
	
	public static ArtistaBC getInstance() {
		if( instance == null ) {
			instance = new ArtistaBC();
		}
		return instance;
	}

	@Override
	public long create(Artista object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = ArtistaDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Artista retrieve(long id) {
		// TODO Auto-generated method stub
		Artista result = ArtistaDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Artista> retrieveAll() {
		// TODO Auto-generated method stub
		List<Artista> result = ArtistaDAO.getInstance().retrieveAll();
		return result;
	}

	public List<Artista> retrieveAll(long id) {
		// TODO Auto-generated method stub
		List<Artista> result = ArtistaDAO.getInstance().retrieveAll(id);
		return result;
	}

	@Override
	public boolean update(Artista object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = ArtistaDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Artista object) {
		// TODO Auto-generated method stub
		boolean result = ArtistaDAO.getInstance().delete(object);
		return result;
	}

	@Override
	protected void validate(Artista object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Artista nulo" );
		} else if( object.getNome() == null || object.getNome().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Artista n�o preenchido" );
		}
	}
}