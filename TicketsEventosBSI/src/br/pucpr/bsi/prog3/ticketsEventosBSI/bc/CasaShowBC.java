package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.CasaShowDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;

public class CasaShowBC extends AmbienteBC<CasaShow> {
	private static CasaShowBC instance;
	
	private CasaShowBC() {
	}

	public static CasaShowBC getInstance() {
		if( instance == null ) {
			instance = new CasaShowBC();
		}
		return instance;
	}
	
	@Override
	public CasaShow retrieve(long id) {
		// TODO Auto-generated method stub
		CasaShow result = CasaShowDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<CasaShow> retrieveAll() {
		// TODO Auto-generated method stub
		List<CasaShow> result = CasaShowDAO.getInstance().retrieveAll();
		return result;
	}
}