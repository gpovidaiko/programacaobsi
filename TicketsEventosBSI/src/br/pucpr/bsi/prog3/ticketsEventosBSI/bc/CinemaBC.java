package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.CinemaDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;

public class CinemaBC extends AmbienteBC<Cinema> {
	private static CinemaBC instance;
	
	private CinemaBC() {
	}

	public static CinemaBC getInstance() {
		if( instance == null ) {
			instance = new CinemaBC();
		}
		return instance;
	}
	
	@Override
	public Cinema retrieve(long id) {
		// TODO Auto-generated method stub
		Cinema result = CinemaDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Cinema> retrieveAll() {
		// TODO Auto-generated method stub
		List<Cinema> result = CinemaDAO.getInstance().retrieveAll();
		return result;
	}
}