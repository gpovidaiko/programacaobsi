package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.ClienteDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;

public class ClienteBC extends UsuarioBC<Cliente> {
	private static ClienteBC instance;
	
	private ClienteBC() {
	}
	
	public static ClienteBC getInstance() {
		if( instance == null ) {
			instance = new ClienteBC();
		}
		return instance;
	}

	@Override
	public long create(Cliente object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = ClienteDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Cliente retrieve(long id) {
		// TODO Auto-generated method stub
		Cliente result = ClienteDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Cliente> retrieveAll() {
		// TODO Auto-generated method stub
		List<Cliente> result = ClienteDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Cliente object) {
		// TODO Auto-generated method stub
		boolean result = ClienteDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Cliente object) {
		// TODO Auto-generated method stub
		boolean result = ClienteDAO.getInstance().delete(object);
		return result;
	}
}