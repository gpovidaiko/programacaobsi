package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.CompraDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;

public class CompraBC extends PatternBC<Compra> {
	private static CompraBC instance;
	
	private CompraBC() {
	}
	
	public static CompraBC getInstance() {
		if( instance == null ) {
			instance = new CompraBC();
		}
		return instance;
	}

	@Override
	public long create(Compra object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = CompraDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Compra retrieve(long id) {
		// TODO Auto-generated method stub
		Compra result = CompraDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Compra> retrieveAll() {
		// TODO Auto-generated method stub
		List<Compra> result = CompraDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Compra object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = CompraDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Compra object) {
		// TODO Auto-generated method stub
		boolean result = CompraDAO.getInstance().delete(object);
		return result;
	}

	@Override
	protected void validate(Compra object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Compra nulo" );
		}
		if( object.getSituacao() == null ) {
			throw new TicketsEventosBSIException( "Situacao n�o preenchida" );
		}
		if( object.getIngressos() == null || object.getIngressos().size() == 0 ) {
			throw new TicketsEventosBSIException( "Ao menos 1 ingresso dever� ser informado" );
		} else {
			for( Ingresso ingresso : object.getIngressos() ) {
				IngressoBC.getInstance().validate( ingresso );
			}
		}
		if( object.getCliente() == null ) {
			throw new TicketsEventosBSIException( "O cliente dever� ser informado" );
		} else {
			ClienteBC.getInstance().validate( object.getCliente() );
		}
	}
}