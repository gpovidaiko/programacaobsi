package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.DiretorDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;

public class DiretorBC extends PatternBC<Diretor> {
	private static DiretorBC instance;
	
	private DiretorBC() {
	}
	
	public static DiretorBC getInstance() {
		if( instance == null ) {
			instance = new DiretorBC();
		}
		return instance;
	}

	@Override
	public long create(Diretor object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = DiretorDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Diretor retrieve(long id) {
		// TODO Auto-generated method stub
		Diretor result = DiretorDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Diretor> retrieveAll() {
		// TODO Auto-generated method stub
		List<Diretor> result = DiretorDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Diretor object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = DiretorDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Diretor object) {
		// TODO Auto-generated method stub
		boolean result = DiretorDAO.getInstance().delete(object);
		return result;
	}

	@Override
	protected void validate(Diretor object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Diretor nulo" );
		} else if( object.getNome() == null || object.getNome().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Diretor n�o preenchido" );
		}
	}
}