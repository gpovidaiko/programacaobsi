package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.EnderecoDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;

public class EnderecoBC extends PatternBC<Endereco> {
	private static EnderecoBC instance;
	
	private EnderecoBC() {
	}
	
	public static EnderecoBC getInstance() {
		if( instance == null ) {
			instance = new EnderecoBC();
		}
		return instance;
	}

	@Override
	public long create(Endereco object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = EnderecoDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Endereco retrieve(long id) {
		// TODO Auto-generated method stub
		Endereco result = EnderecoDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Endereco> retrieveAll() {
		// TODO Auto-generated method stub
		List<Endereco> result = EnderecoDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Endereco object) {
		// TODO Auto-generated method stub
		boolean result = EnderecoDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Endereco object) {
		// TODO Auto-generated method stub
		boolean result = EnderecoDAO.getInstance().delete(object);
		return result;
	}

	@Override
	protected void validate(Endereco object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Endereco nulo" );
		}
		if( object.getRua() == null || object.getRua().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Rua n�o preenchida" );
		}
		if( object.getNumero() == 0 ) {
			throw new TicketsEventosBSIException( "N�mero do Endere�o n�o preenchido" );
		} else if( object.getNumero() < 0 ) {
			throw new TicketsEventosBSIException( "N�mero do Endere�o n�o pode ser negativo" );
		}
		if( object.getComplemento() == null || object.getComplemento().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Complemento do Endere�o n�o preenchido" );
		}
		if( object.getBairro() == null || object.getBairro().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Bairro do Endere�o n�o preenchido" );
		}
		if( object.getCidade() == null || object.getCidade().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Cidade do Endere�o n�o preenchida" );
		}
		if( object.getEstado() == null || object.getEstado().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Estado do Endere�o n�o preenchido" );
		}
		if( object.getPais() == null || object.getPais().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Pais do Endere�o n�o preenchido" );
		}
	}
}