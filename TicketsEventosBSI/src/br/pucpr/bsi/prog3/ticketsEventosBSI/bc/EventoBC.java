package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.Calendar;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;

public abstract class EventoBC<E extends Evento> extends PatternBC<E> {

	protected EventoBC() {
	}

	@SuppressWarnings("rawtypes")
	public static EventoBC getInstance(Evento evento){
		if(evento instanceof Filme)
			return FilmeBC.getInstance();
		else if(evento instanceof Peca)
			return PecaBC.getInstance();
		else
			return ShowBC.getInstance();
	}

	@Override
	public long create(E object) {
		// TODO Auto-generated method stub
		this.validate(object);
		return 0;
	}

	@Override
	public E retrieve(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(E object) {
		// TODO Auto-generated method stub
		this.validate(object);
		return false;
	}

	@Override
	public boolean delete(E object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void validate(Evento object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Evento nulo" );
		}
		if( object.getNome() == null || object.getNome().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Evento n�o preenchido" );
		}
		if( object.getDescricao() == null || object.getDescricao().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Evento n�o preenchida" );
		}
		if( object.getCensura() == 0 ) {
			throw new TicketsEventosBSIException( "Censura do Evento n�o preenchida" );
		} else if( object.getCensura() < 0 ) {
			throw new TicketsEventosBSIException( "Censura do Evento n�o pode ser negativa" );
		}
		if( object.getDataInclusao() == null ) {
			throw new TicketsEventosBSIException( "Data de Inclus�o do Evento n�o preenchida" );
		} else if( object.getDataEvento() == null || object.getDataInclusao().after( object.getDataEvento() ) ) {
			throw new TicketsEventosBSIException( "Data de Inclus�o deve ser antes da Data do Evento" );
		}
		if( object.getDataEvento() == null ) {
			throw new TicketsEventosBSIException( "Data do Evento n�o preenchida" );
		} else if( object.getDataEvento().before( Calendar.getInstance().getTime() ) ) {
			throw new TicketsEventosBSIException( "Data do Evento deve ser ap�s a data atual" );
		}
		AmbienteBC.getInstance( object.getAmbiente() ).validate( object.getAmbiente() );
		if( object.getArtistas() == null || object.getArtistas().size() == 0 ) {
			throw new TicketsEventosBSIException( "Ao menos 1 Artista dever� ser informado" );
		} else {
			for( Artista artista : object.getArtistas() ) {
				ArtistaBC.getInstance().validate( artista );
			}
		}
		DiretorBC.getInstance().validate( object.getDiretor() );
	}
}