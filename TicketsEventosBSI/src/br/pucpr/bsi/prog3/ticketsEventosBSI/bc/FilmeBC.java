package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.FilmeDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;

public class FilmeBC extends EventoBC<Filme>{
	private static FilmeBC instance;
	
	private FilmeBC() {
	}

	public static FilmeBC getInstance() {
		if( instance == null ) {
			instance = new FilmeBC();
		}
		return instance;
	}

	@Override
	public long create(Filme object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = FilmeDAO.getInstance().create(object);
		FilmeDAO.getInstance().createEventoArtista(object);
		return result;
	}

	@Override
	public Filme retrieve(long id) {
		// TODO Auto-generated method stub
		Filme result = FilmeDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Filme> retrieveAll() {
		// TODO Auto-generated method stub
		List<Filme> result = FilmeDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Filme object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = FilmeDAO.getInstance().update(object);
		FilmeDAO.getInstance().updateEventoArtista(object);
		return result;
	}

	@Override
	public boolean delete(Filme object) {
		// TODO Auto-generated method stub
		boolean result = FilmeDAO.getInstance().delete(object);
		FilmeDAO.getInstance().deleteEventoArtista(object);
		return result;
	}

	@Override
	protected void validate(Filme object) {
		// TODO Auto-generated method stub
		super.validate(object);
		if( object.getGenero() == null || object.getGenero().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "G�nero do Filme n�o preenchido" );
		}
	}
}