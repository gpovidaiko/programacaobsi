package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.FuncionarioDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Funcionario;

public class FuncionarioBC extends UsuarioBC<Funcionario>{
	private static FuncionarioBC instance;
	
	private FuncionarioBC() {
	}
	
	public static FuncionarioBC getInstance() {
		if( instance == null ) {
			instance = new FuncionarioBC();
		}
		return instance;
	}

	@Override
	public long create(Funcionario object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = FuncionarioDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Funcionario retrieve(long id) {
		// TODO Auto-generated method stub
		Funcionario result = FuncionarioDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Funcionario> retrieveAll() {
		// TODO Auto-generated method stub
		List<Funcionario> result = FuncionarioDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Funcionario object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = FuncionarioDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Funcionario object) {
		// TODO Auto-generated method stub
		boolean result = FuncionarioDAO.getInstance().delete(object);
		return result;
	}
}