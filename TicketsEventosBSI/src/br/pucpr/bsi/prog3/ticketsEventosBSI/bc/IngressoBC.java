package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.IngressoDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;

public class IngressoBC extends PatternBC<Ingresso> {
	private static IngressoBC instance;
	
	private IngressoBC() {
	}
	
	public static IngressoBC getInstance() {
		if( instance == null ) {
			instance = new IngressoBC();
		}
		return instance;
	}

	@Override
	public long create(Ingresso object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = IngressoDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Ingresso retrieve(long id) {
		// TODO Auto-generated method stub
		Ingresso result = IngressoDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Ingresso> retrieveAll() {
		// TODO Auto-generated method stub
		List<Ingresso> result = IngressoDAO.getInstance().retrieveAll();
		return result;
	}

	public List<Ingresso> retrieveAll(long idCompra) {
		// TODO Auto-generated method stub
		List<Ingresso> result = IngressoDAO.getInstance().retrieveAll(idCompra);
		return result;
	}

	@Override
	public boolean update(Ingresso object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = IngressoDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Ingresso object) {
		// TODO Auto-generated method stub
		boolean result = IngressoDAO.getInstance().delete(object);
		return result;
	}

	@Override
	protected void validate(Ingresso object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Ingresso nulo" );
		}
		if( object.getCadeiraNumerada() == 0 ) {
			throw new TicketsEventosBSIException( "Cadeira Numerada n�o preenchida" );
		} else if( object.getCadeiraNumerada() < 0 ) {
			throw new TicketsEventosBSIException( "Cadeira Numerada n�o pode ser negativa" );
		}
		if( object.getSetor() == null ) {
			throw new TicketsEventosBSIException( "O Setor dever� ser informado" );
		} else {
			SetorBC.getInstance().validate( object.getSetor() );
		}
		if( object.getEvento() == null ) {
			throw new TicketsEventosBSIException( "O Evento dever� ser informado" );
		}
		if( object.getCompra() == null ) {
			throw new TicketsEventosBSIException( "A compra a qual o Ingresso pertence dever� ser informada" );
		}
	}
}