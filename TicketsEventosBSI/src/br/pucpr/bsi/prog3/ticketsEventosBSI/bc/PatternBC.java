package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

public abstract class PatternBC<E> {
	
	/**
	 * Utilizado para realizar a validacao do object e a chamada do metodo de
	 * armazenamento correspondente da classe DAO
	 * 
	 * Devera verificar se o objeto esta de acordo com as regras de negocio
	 * para ser atualizado na base de dados.
	 * @param object
	 * @return
	 */
	public abstract long create(E object);
	
	/**
	 * Utilizado para verificar se o id passado nao eh nulo e chamar o metodo
	 * de busca correspondente da classe DAO
	 * 
	 * Devera verificar se o id eh negativo ou null
	 * @param id
	 * @return
	 */
	public abstract E retrieve(long id);
	
	/**
	 * Utilizado para retornar todas as instancias de uma determinada classe,
	 * atraves do metodo de busca correspondente da classe DAO
	 * @return
	 */
	public abstract List<E> retrieveAll();
	
	/**
	 * Utilizado para realizar a validacao do object e a chamada do metodo de
	 * atualizacao correspondente na classe DAO.
	 * 
	 * Devera verificar se o objeto esta de acordo com as regras de negocio
	 * para ser atualizado na base de dados.
	 * @param object
	 * @return
	 */
	public abstract boolean update(E object);
	
	/**
	 * Utilizado para chamar um metodo de delecao correspondente na classe DAO.
	 * 
	 * Devera verificar se o objeto passado nao eh null
	 * @param object
	 * @return
	 */
	public abstract boolean delete(E object);
	
	/**
	 * Realiza a validacao de um objeto para a insercao ou atualizacao correspondente
	 * da classe DAO
	 * 
	 * As validacoes de regras de negocio deverao ser realizadas nesse metodo
	 * @param object
	 */
	protected abstract void validate(E object); 
}
