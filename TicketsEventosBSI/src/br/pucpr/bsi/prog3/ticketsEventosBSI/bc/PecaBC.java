package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.PecaDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;

public class PecaBC extends EventoBC<Peca> {

	private static PecaBC instance;
	
	private PecaBC() {
	}
	
	public static PecaBC getInstance() {
		if( instance == null ) {
			instance = new PecaBC();
		}
		return instance;
	}

	@Override
	public long create(Peca object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = PecaDAO.getInstance().create(object);
		PecaDAO.getInstance().createEventoArtista(object);
		return result;
	}

	@Override
	public Peca retrieve(long id) {
		// TODO Auto-generated method stub
		Peca result = PecaDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Peca> retrieveAll() {
		// TODO Auto-generated method stub
		List<Peca> result = PecaDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	public boolean update(Peca object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = PecaDAO.getInstance().update(object);
		PecaDAO.getInstance().updateEventoArtista(object);
		return result;
	}

	@Override
	public boolean delete(Peca object) {
		// TODO Auto-generated method stub
		boolean result = PecaDAO.getInstance().delete(object);
		PecaDAO.getInstance().deleteEventoArtista(object);
		return result;
	}
	
	@Override
	protected void validate(Peca object) {
		// TODO Auto-generated method stub
		super.validate(object);
		if( object.getCompanhia() == null || object.getCompanhia().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Companhia da Pe�a n�o preenchida" );
		}
		if( object.getGenero() == null || object.getGenero().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "G�nero da Pe�a n�o preenchido" );
		}
	}
}
