package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.SetorDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;

public class SetorBC extends PatternBC<Setor> {
	private static SetorBC instance;
	
	private SetorBC() {
	}
	
	public static SetorBC getInstance() {
		if( instance == null ) {
			instance = new SetorBC();
		}
		return instance;
	}

	@Override
	public long create(Setor object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = SetorDAO.getInstance().create(object);
		return result;
	}

	@Override
	public Setor retrieve(long id) {
		// TODO Auto-generated method stub
		Setor result = SetorDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Setor> retrieveAll() {
		// TODO Auto-generated method stub
		List<Setor> result = SetorDAO.getInstance().retrieveAll();
		return result;
	}

	public List<Setor> retrieveAll(Ambiente ambiente) {
		// TODO Auto-generated method stub
		List<Setor> result = SetorDAO.getInstance().retrieveAll(ambiente);
		return result;
	}

	@Override
	public boolean update(Setor object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = SetorDAO.getInstance().update(object);
		return result;
	}

	@Override
	public boolean delete(Setor object) {
		// TODO Auto-generated method stub
		boolean result = SetorDAO.getInstance().delete(object);
		return result;
	}

	@Override
	protected void validate(Setor object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Setor nulo" );
		}
		if( object.getNome() == null || object.getNome().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Setor n�o preenchido" );
		}
		if( object.getCapacidade() == 0 ) {
			throw new TicketsEventosBSIException( "Capacidade do Setor n�o preenchida" );
		} else if( object.getCapacidade() < 0 ) {
			throw new TicketsEventosBSIException( "Capacidade do Setor n�o pode ser negativa" );
		}
		if( object.getPreco() == 0 ) {
			throw new TicketsEventosBSIException( "Preco do Setor n�o preenchido" );
		} else if( object.getPreco() < 0 ) {
			throw new TicketsEventosBSIException( "Preco do Setor n�o pode ser negativo" );
		}
		if( object.getAmbiente() == null || ! object.getAmbiente().getSetores().contains( object ) ) {
			throw new TicketsEventosBSIException( "O Ambiente dever� ser informado" );
		}
	}
}