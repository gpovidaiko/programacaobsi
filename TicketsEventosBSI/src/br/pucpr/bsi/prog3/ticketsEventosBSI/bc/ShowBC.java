package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.ShowDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;

public class ShowBC extends EventoBC<Show> {
	private static ShowBC instance;
	
	private ShowBC() {
	}
	
	public static ShowBC getInstance() {
		if( instance == null ) {
			instance = new ShowBC();
		}
		return instance;
	}

	@Override
	public long create(Show object) {
		// TODO Auto-generated method stub
		this.validate(object);
		long result = ShowDAO.getInstance().create(object);
		ShowDAO.getInstance().createEventoArtista(object);
		return result;
	}

	@Override
	public boolean update(Show object) {
		// TODO Auto-generated method stub
		this.validate(object);
		boolean result = ShowDAO.getInstance().update(object);
		ShowDAO.getInstance().updateEventoArtista(object);
		return result;
	}

	@Override
	public boolean delete(Show object) {
		// TODO Auto-generated method stub
		boolean result = ShowDAO.getInstance().delete(object);
		ShowDAO.getInstance().deleteEventoArtista(object);
		return result;
	}

	@Override
	public Show retrieve(long id) {
		// TODO Auto-generated method stub
		Show result = ShowDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Show> retrieveAll() {
		// TODO Auto-generated method stub
		List<Show> result = ShowDAO.getInstance().retrieveAll();
		return result;
	}

	@Override
	protected void validate(Show object) {
		// TODO Auto-generated method stub
		super.validate(object);
		if( object.getEstilo() == null || object.getEstilo().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Estilo n�o preenchido" );
		}
	}
}