package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.TeatroDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;

public class TeatroBC extends AmbienteBC<Teatro> {
	private static TeatroBC instance;
	
	private TeatroBC() {
	}
	
	public static TeatroBC getInstance() {
		if( instance == null ) {
			instance = new TeatroBC();
		}
		return instance;
	}

	@Override
	public Teatro retrieve(long id) {
		// TODO Auto-generated method stub
		Teatro result = TeatroDAO.getInstance().retrieve(id);
		return result;
	}

	@Override
	public List<Teatro> retrieveAll() {
		// TODO Auto-generated method stub
		List<Teatro> result = TeatroDAO.getInstance().retrieveAll();
		return result;
	}
}