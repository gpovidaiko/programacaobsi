package br.pucpr.bsi.prog3.ticketsEventosBSI.bc;

import java.util.Calendar;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Usuario;

public abstract class UsuarioBC<E extends Usuario> extends PatternBC<E>{
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String TELEFONE_PATTERN = "^(UpdFU-)?[0-9]{8,}$";

	protected UsuarioBC() {
	}

	@Override
	public long create(E object) {
		// TODO Auto-generated method stub
		this.validate(object);
		return 0;
	}

	@Override
	public E retrieve(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<E> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean update(E object) {
		// TODO Auto-generated method stub
		this.validate(object);
		return false;
	}

	@Override
	public boolean delete(E object) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void validate(Usuario object) {
		// TODO Auto-generated method stub
		if( object == null ) {
			throw new TicketsEventosBSIException( "Usu�rio nulo" );
		}
		if( object.getNome() == null || object.getNome().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Nome do Usu�rio n�o preenchido" );
		}
		if( object.getCpf() == null || object.getCpf().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "CPF do Usu�rio n�o preenchido" );
		}
		if( object.getEmail() == null || object.getEmail().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "E-mail do Usu�rio n�o preenchido" );
		} else if( ! object.getEmail().matches( UsuarioBC.EMAIL_PATTERN ) ) {
			throw new TicketsEventosBSIException( "E-mail do Usu�rio inv�lido" );
		}
		if( object.getTelefone() == null || object.getTelefone().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Telefone do Usu�rio n�o preenchido" );
		} else if( ! object.getTelefone().matches( UsuarioBC.TELEFONE_PATTERN ) ) {
			throw new TicketsEventosBSIException( "Telefone do Usu�rio deve possuir ao menos 8 caracteres num�ricos" );
		}
		if( object.getDataNascimento() == null ) {
			throw new TicketsEventosBSIException( "Data de Nascimento do Usu�rio n�o preenchida" );
		} else if( object.getDataNascimento().after( Calendar.getInstance().getTime() ) ) {
			throw new TicketsEventosBSIException( "Data de Nascimento do Usu�rio n�o deve ser ap�s a data atual" );
		}
		if( object.getUser() == null || object.getUser().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "User do Usu�rio n�o preenchido" );
		}
		if( object.getSenha() == null || object.getSenha().trim().isEmpty() ) {
			throw new TicketsEventosBSIException( "Senha do Usu�rio n�o preenchido" );
		}
		EnderecoBC.getInstance().validate( object.getEndereco() );
	}
}