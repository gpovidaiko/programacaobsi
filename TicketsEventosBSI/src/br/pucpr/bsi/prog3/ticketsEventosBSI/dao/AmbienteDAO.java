package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public abstract class AmbienteDAO<E extends Ambiente> extends PatternDAO<E> {

	private static StringBuilder insertSQL = new StringBuilder()
		.append("INSERT INTO AMBIENTE ")
		.append("(ID_ENDERECO, TIPO, NOME, DESCRICAO) ")
		.append("VALUES ")
		.append("(?,?,?,?)");

	private static StringBuilder updateSQL = new StringBuilder()
		.append("UPDATE AMBIENTE ")
		.append("SET ID_ENDERECO = ?, TIPO = ?, NOME = ?, DESCRICAO = ? ")
		.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
		.append("DELETE FROM AMBIENTE ")
		.append("WHERE ID = ? ");

	public AmbienteDAO() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("rawtypes")
	public static AmbienteDAO getInstance(Ambiente ambiente){
		if(ambiente instanceof CasaShow)
			return CasaShowDAO.getInstance();
		else if(ambiente instanceof Cinema)
			return CinemaDAO.getInstance();
		else
			return TeatroDAO.getInstance();
	}

	@Override
	public long create(Ambiente object) {
		// TODO Auto-generated method stub
		EnderecoBC.getInstance().create(object.getEndereco());
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getEndereco().getId());
			ps.setString(2, String.valueOf(object.getTipoAmbiente().getTipo()));
			ps.setString(3, object.getNome());
			ps.setString(4, object.getDescricao());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		for (Setor setor : object.getSetores()) {
			SetorBC.getInstance().create(setor);
		}
		return object.getId();
	}

	@Override
	public boolean update(Ambiente object) {
		// TODO Auto-generated method stub
		EnderecoBC.getInstance().update(object.getEndereco());
		for (Setor setor : object.getSetores()) {
			SetorBC.getInstance().update(setor);
		}
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setDouble(1, object.getEndereco().getId());
			ps.setString(2, String.valueOf(object.getTipoAmbiente().getTipo()));
			ps.setString(3, object.getNome());
			ps.setString(4, object.getDescricao());
			ps.setLong(5, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do update
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Ambiente object) {
		// TODO Auto-generated method stub
		for (Setor setor : object.getSetores()) {
			SetorBC.getInstance().delete(setor);
		}
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setObject(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do delete
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		EnderecoBC.getInstance().delete(object.getEndereco());
		return true;
	}

}
