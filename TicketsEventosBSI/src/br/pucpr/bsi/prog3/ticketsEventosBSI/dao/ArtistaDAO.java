package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class ArtistaDAO extends PatternDAO<Artista> {
	private static StringBuilder insertSQL = new StringBuilder()
	.append("INSERT INTO ARTISTA ")
	.append("(NOME) ")
	.append("VALUES ")
	.append("(?)");

	private static StringBuilder updateSQL = new StringBuilder()
	.append("UPDATE ARTISTA ")
	.append("SET NOME = ? ")
	.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
	.append("DELETE FROM ARTISTA ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT ID, NOME ")
	.append("FROM ARTISTA ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectIdEventoSQL =  new StringBuilder()
	.append("SELECT a.ID, a.NOME ")
	.append("FROM ARTISTA AS a INNER JOIN ARTISTA_EVENTO AS ae ")
	.append("ON a.ID = ae.ID_ARTISTA WHERE ae.ID_EVENTO = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT ID, NOME ")
	.append("FROM ARTISTA ");

	private static ArtistaDAO instance = new ArtistaDAO();
	
	private ArtistaDAO() {
	}
	
	public static ArtistaDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Artista object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, object.getNome());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Artista object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, object.getNome());
			ps.setDouble(2, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Artista object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public Artista retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Artista artista = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				artista = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return artista;
	}

	@Override
	public List<Artista> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Artista> artistas = new ArrayList<Artista>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				artistas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return artistas;
	}

	public List<Artista> retrieveAll(long idEvento) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Artista> artistas = new ArrayList<Artista>();
		try {
			ps = connection.prepareStatement(selectIdEventoSQL.toString());
			ps.setLong(1, idEvento);
			rs = ps.executeQuery();
			if(rs.next()){
				artistas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return artistas;
	}

	@Override
	protected Artista populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Artista artista = new Artista();
		artista.setId(rs.getLong("ID"));
		artista.setNome(rs.getString("NOME"));
		return artista;
	}

}
