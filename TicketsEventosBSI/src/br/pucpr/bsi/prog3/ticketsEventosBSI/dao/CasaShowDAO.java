package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class CasaShowDAO extends AmbienteDAO<CasaShow> {

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT ID, ID_ENDERECO, TIPO, NOME, DESCRICAO ")
	.append("FROM AMBIENTE ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT ID, ID_ENDERECO, TIPO, NOME, DESCRICAO ")
	.append("FROM AMBIENTE ")
	.append("WHERE TIPO = \"s\" ");

	private static CasaShowDAO instance = new CasaShowDAO();
	
	private CasaShowDAO() {
	}
	
	public static CasaShowDAO getInstance() {
		return instance;
	}

	@Override
	public CasaShow retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		CasaShow casaShow = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				casaShow = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return casaShow;
	}

	@Override
	public List<CasaShow> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<CasaShow> casasShow = new ArrayList<CasaShow>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				casasShow.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return casasShow;
	}

	@Override
	protected CasaShow populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Endereco endereco = EnderecoBC.getInstance().retrieve(rs.getLong("ID_ENDERECO"));
		CasaShow casaShow = new CasaShow(endereco);
		casaShow.setId(rs.getLong("ID"));
		casaShow.setNome(rs.getString("NOME"));
		casaShow.setDescricao(rs.getString("DESCRICAO"));
		casaShow.setSetores(SetorBC.getInstance().retrieveAll(casaShow));
		return casaShow;
	}

}
