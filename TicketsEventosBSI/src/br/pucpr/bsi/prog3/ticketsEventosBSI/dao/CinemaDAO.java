package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class CinemaDAO extends AmbienteDAO<Cinema> {

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT ID, ID_ENDERECO, TIPO, NOME, DESCRICAO ")
	.append("FROM AMBIENTE ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT ID, ID_ENDERECO, TIPO, NOME, DESCRICAO ")
	.append("FROM AMBIENTE ")
	.append("WHERE TIPO = \"c\" ");

	private static CinemaDAO instance = new CinemaDAO();
	
	private CinemaDAO() {
	}
	
	public static CinemaDAO getInstance() {
		return instance;
	}

	@Override
	public Cinema retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cinema cinema = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				cinema = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return cinema;
	}

	@Override
	public List<Cinema> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Cinema> cinemas = new ArrayList<Cinema>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				cinemas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return cinemas;
	}

	@Override
	protected Cinema populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Endereco endereco = EnderecoBC.getInstance().retrieve(rs.getLong("ID_ENDERECO"));
		Cinema cinema = new Cinema(endereco);
		cinema.setId(rs.getLong("ID"));
		cinema.setNome(rs.getString("NOME"));
		cinema.setDescricao(rs.getString("DESCRICAO"));
		cinema.setSetores(SetorBC.getInstance().retrieveAll(cinema));
		return cinema;
	}

}
