package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class ClienteDAO extends UsuarioDAO<Cliente> {
	private static StringBuilder insertSQL = new StringBuilder()
			.append("INSERT INTO CLIENTE ")
			.append("(ID_ENDERECO, NOME, CPF, EMAIL, TELEFONE, DATA_NASCIMENTO, USUARIO, SENHA) ")
			.append("VALUES ")
			.append("(?,?,?,?,?,?,?,?)");

	private static StringBuilder updateSQL = new StringBuilder()
			.append("UPDATE CLIENTE ")
			.append("SET ID_ENDERECO = ?, NOME = ?, CPF = ?, EMAIL = ?, TELEFONE = ?, DATA_NASCIMENTO = ?, USUARIO = ?, SENHA = ? ")
			.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
			.append("DELETE FROM CLIENTE ")
			.append("WHERE ID = ? ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
			.append("SELECT ID, ID_ENDERECO, NOME, CPF, EMAIL, TELEFONE, DATA_NASCIMENTO, USUARIO, SENHA ")
			.append("FROM CLIENTE ")
			.append("WHERE ID = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
			.append("SELECT ID, ID_ENDERECO, NOME, CPF, EMAIL, TELEFONE, DATA_NASCIMENTO, USUARIO, SENHA ")
			.append("FROM CLIENTE ");

	private static ClienteDAO instance = new ClienteDAO();
	
	private ClienteDAO() {
	}
	
	public static ClienteDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Cliente object) {
		// TODO Auto-generated method stub
		EnderecoBC.getInstance().create(object.getEndereco());
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, object.getEndereco().getId());
			ps.setString(2, object.getNome());
			ps.setString(3, object.getCpf());
			ps.setString(4, object.getEmail());
			ps.setString(5, object.getTelefone());
//			ps.setDate(6, (Date) object.getDataNascimento());
			ps.setDate(6, new Date(object.getDataNascimento().getTime()));
			ps.setString(7, object.getUser());
			ps.setString(8, object.getSenha());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Cliente object) {
		// TODO Auto-generated method stub
		EnderecoBC.getInstance().update(object.getEndereco());
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setLong(1, object.getEndereco().getId());
			ps.setString(2, object.getNome());
			ps.setString(3, object.getCpf());
			ps.setString(4, object.getEmail());
			ps.setString(5, object.getTelefone());
//			ps.setDate(6, (Date) object.getDataNascimento());
			ps.setDate(6, new Date(object.getDataNascimento().getTime()));
			ps.setString(7, object.getUser());
			ps.setString(8, object.getSenha());
			ps.setLong(9, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Cliente object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setLong(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		EnderecoBC.getInstance().delete(object.getEndereco());
		return true;
	}

	@Override
	public Cliente retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cliente cliente = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				cliente = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return cliente;
	}

	@Override
	public List<Cliente> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Cliente> clientes = new ArrayList<Cliente>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				clientes.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return clientes;
	}

	@Override
	protected Cliente populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Endereco endereco = EnderecoBC.getInstance().retrieve(rs.getLong("ID_ENDERECO"));
		Cliente cliente = new Cliente(endereco);
		cliente.setId(rs.getLong("ID"));
		cliente.setNome(rs.getString("NOME"));
		cliente.setCpf(rs.getString("CPF"));
		cliente.setEmail(rs.getString("EMAIL"));
		cliente.setTelefone(rs.getString("TELEFONE"));
		cliente.setDataNascimento(rs.getDate("DATA_NASCIMENTO"));
		cliente.setUser(rs.getString("USUARIO"));
		cliente.setSenha(rs.getString("SENHA"));
		return cliente;
	}

}
