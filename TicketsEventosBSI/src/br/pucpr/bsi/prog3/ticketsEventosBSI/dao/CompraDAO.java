package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.IngressoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class CompraDAO extends PatternDAO<Compra> {

	private static StringBuilder insertSQL = new StringBuilder()
	.append("INSERT INTO COMPRA ")
	.append("(ID_CLIENTE, SITUACAO) ")
	.append("VALUES ")
	.append("(?, ?)");

	private static StringBuilder updateSQL = new StringBuilder()
	.append("UPDATE COMPRA ")
	.append("SET ID_CLIENTE = ?, SITUACAO = ? ")
	.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
	.append("DELETE FROM COMPRA ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT ID, ID_CLIENTE, SITUACAO ")
	.append("FROM COMPRA ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT ID, ID_CLIENTE, SITUACAO ")
	.append("FROM COMPRA ");

	private static CompraDAO instance = new CompraDAO();
	
	private CompraDAO() {
	}
	
	public static CompraDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Compra object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getCliente().getId());
			ps.setString(2, String.valueOf(object.getSituacao().getTipo()));
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Compra object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getCliente().getId());
			ps.setString(2, String.valueOf(object.getSituacao().getTipo()));
			ps.setDouble(3, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Compra object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public Compra retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Compra compra = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				compra = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return compra;
	}

	@Override
	public List<Compra> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Compra> compras = new ArrayList<Compra>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				compras.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return compras;
	}

	@Override
	protected Compra populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Cliente cliente = ClienteBC.getInstance().retrieve(rs.getLong("ID_CLIENTE"));
		List<Ingresso> ingressos = IngressoBC.getInstance().retrieveAll(rs.getLong("ID"));
		Compra compra = new Compra(cliente, ingressos);
		compra.setId(rs.getLong("ID"));
		if (rs.getString("SITUACAO").equals("r")) {
			compra.setSituacao(TipoCompraEnum.RESERVADO);
		} else {
			compra.setSituacao(TipoCompraEnum.VENDIDO);
		}
		return compra;
	}

}
