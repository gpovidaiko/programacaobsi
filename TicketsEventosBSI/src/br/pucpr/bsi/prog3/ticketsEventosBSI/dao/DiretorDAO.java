package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class DiretorDAO extends PatternDAO<Diretor>{
	private static StringBuilder insertSQL = new StringBuilder()
	.append("INSERT INTO DIRETOR ")
	.append("(NOME) ")
	.append("VALUES ")
	.append("(?)");

	private static StringBuilder updateSQL = new StringBuilder()
	.append("UPDATE DIRETOR ")
	.append("SET NOME = ? ")
	.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
	.append("DELETE FROM DIRETOR ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT ID, NOME ")
	.append("FROM DIRETOR ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT ID, NOME ")
	.append("FROM DIRETOR ");

	private static DiretorDAO instance = new DiretorDAO();
	
	private DiretorDAO() {
	}
	
	public static DiretorDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Diretor object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, object.getNome());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Diretor object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setString(1, object.getNome());
			ps.setLong(2, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do update
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Diretor object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setObject(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do delete
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public Diretor retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Diretor diretor = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				diretor = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return diretor;
	}

	@Override
	public List<Diretor> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Diretor> diretores = new ArrayList<Diretor>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				diretores.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return diretores;
	}

	@Override
	protected Diretor populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Diretor diretor = new Diretor();
		diretor.setId(rs.getLong("ID"));
		diretor.setNome(rs.getString("NOME"));
		return diretor;
	}

}
