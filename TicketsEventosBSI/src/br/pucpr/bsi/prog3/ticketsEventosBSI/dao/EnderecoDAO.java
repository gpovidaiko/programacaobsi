package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class EnderecoDAO extends PatternDAO<Endereco> {
	
	//////////////////////////////////////////
	// QUERIES
	//////////////////////////////////////////

	private static StringBuilder insertSQL = new StringBuilder()
			.append("INSERT INTO ENDERECO ")
			.append("	(RUA, NUMERO, COMPLEMENTO, BAIRRO, CIDADE, ESTADO, PAIS) ")
			.append("VALUES ")
			.append("	(?,?,?,?,?,?,?)");
	
	private static StringBuilder updateSQL = new StringBuilder()
			.append("UPDATE ENDERECO ")
			.append("SET RUA = ?, NUMERO = ?, COMPLEMENTO = ?, BAIRRO = ?, CIDADE = ?, ESTADO = ?, PAIS = ? ")
			.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
			.append("DELETE FROM ENDERECO ")
			.append("WHERE ID = ? ");
	
	private static StringBuilder selectIdSQL =  new StringBuilder()
		.append("SELECT ID, RUA, NUMERO, COMPLEMENTO, BAIRRO, CIDADE, ESTADO, PAIS ")
		.append("FROM ENDERECO ")
		.append("WHERE ID = ? ");
	
	private static StringBuilder selectAllSQL =  new StringBuilder()
		.append("SELECT ID, RUA, NUMERO, COMPLEMENTO, BAIRRO, CIDADE, ESTADO, PAIS ")
		.append("FROM ENDERECO ");
	
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	
	private static EnderecoDAO instance = new EnderecoDAO();
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	private EnderecoDAO() {
	}
	
	//////////////////////////////////////////
	// METODOS
	//////////////////////////////////////////
	
	public static EnderecoDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Endereco object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, object.getRua());
			ps.setInt(2, object.getNumero());
			ps.setString(3, object.getComplemento());
			ps.setString(4, object.getBairro());
			ps.setString(5, object.getCidade());
			ps.setString(6, object.getEstado());
			ps.setString(7, object.getPais());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Endereco object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setString(1, object.getRua());
			ps.setInt(2, object.getNumero());
			ps.setString(3, object.getComplemento());
			ps.setString(4, object.getBairro());
			ps.setString(5, object.getCidade());
			ps.setString(6, object.getEstado());
			ps.setString(7, object.getPais());
			ps.setLong(8, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do update
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Endereco object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setObject(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do delete
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}
	
	@Override
	public Endereco retrieve(long id) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Endereco endereco = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				endereco = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return endereco;
	}

	@Override
	public List<Endereco> retrieveAll() {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Endereco> enderecos = new ArrayList<Endereco>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				enderecos.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return enderecos;
	}

	@Override
	protected Endereco populateObject(ResultSet rs) throws SQLException {
		Endereco endereco = new Endereco();
		endereco.setBairro(rs.getString("BAIRRO"));
		endereco.setCidade(rs.getString("CIDADE"));
		endereco.setComplemento(rs.getString("COMPLEMENTO"));
		endereco.setEstado(rs.getString("ESTADO"));
		endereco.setId(rs.getLong("ID"));
		endereco.setNumero(rs.getInt("NUMERO"));
		endereco.setPais(rs.getString("PAIS"));
		endereco.setRua(rs.getString("RUA"));
		return endereco;
	}
}

