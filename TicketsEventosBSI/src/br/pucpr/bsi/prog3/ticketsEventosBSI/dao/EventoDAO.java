package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public abstract class EventoDAO<E extends Evento> extends PatternDAO<E> {

	private static StringBuilder insertSQL = new StringBuilder()
		.append("INSERT INTO EVENTO ")
		.append("(ID_AMBIENTE, ID_DIRETOR, TIPO, NOME, DESCRICAO, DATAINCLUSAO, DATAEVENTO, CENSURA) ")
		.append("VALUES ")
		.append("(?,?,?,?,?,?,?,?)");

	private static StringBuilder updateSQL = new StringBuilder()
		.append("UPDATE EVENTO ")
		.append("SET ID_AMBIENTE = ?, ID_DIRETOR = ?, TIPO = ?, NOME = ?, DESCRICAO = ?, DATAINCLUSAO = ?, DATAEVENTO = ?, CENSURA = ? ")
		.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
		.append("DELETE FROM EVENTO ")
		.append("WHERE ID = ? ");

	private static StringBuilder insertArtistaEventoSQL = new StringBuilder()
		.append("INSERT INTO ARTISTA_EVENTO ")
		.append("(ID_ARTISTA, ID_EVENTO) ")
		.append("VALUES ")
		.append("(?,?)");

	private static StringBuilder updateArtistaEventoSQL = new StringBuilder()
		.append("UPDATE ARTISTA_EVENTO ")
		.append("SET ID_ARTISTA = ? ")
		.append("WHERE ID_EVENTO = ? ");

	private static StringBuilder deleteArtistaEventoSQL = new StringBuilder()
		.append("DELETE FROM ARTISTA_EVENTO ")
		.append("WHERE ID_EVENTO = ? ");

	@Override
	public long create(Evento object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setLong(1, object.getAmbiente().getId());
			ps.setLong(2, object.getDiretor().getId());
			ps.setString(3, String.valueOf(object.getTipoEvento().getTipo()));
			ps.setString(4, object.getNome());
			ps.setString(5, object.getDescricao());
			ps.setDate(6, new Date(object.getDataInclusao().getTime()));
			ps.setDate(7, new Date(object.getDataEvento().getTime()));
			ps.setInt(8, object.getCensura());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(E object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setLong(1, object.getAmbiente().getId());
			ps.setLong(2, object.getDiretor().getId());
			ps.setString(3, String.valueOf(object.getTipoEvento().getTipo()));
			ps.setString(4, object.getNome());
			ps.setString(5, object.getDescricao());
			ps.setDate(6, new Date(object.getDataInclusao().getTime()));
			ps.setDate(7, new Date(object.getDataEvento().getTime()));
			ps.setInt(8, object.getCensura());
			ps.setLong(9, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(E object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setLong(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	public void createEventoArtista(E object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			for (Artista artista : object.getArtistas()) {
				ps = connection.prepareStatement(insertArtistaEventoSQL.toString());
				ps.setLong(1, artista.getId());
				ps.setLong(2, object.getId());
				
				ps.executeUpdate();
				
				//Realiza o commit da insercao
				connection.commit();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return;
	}
	
	public boolean updateEventoArtista(E object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			for (Artista artista : object.getArtistas()) {
				ps = connection.prepareStatement(updateArtistaEventoSQL.toString());
				ps.setLong(1, artista.getId());
				ps.setLong(2, object.getId());
				
				ps.executeUpdate();
				
				//Realiza o commit da insercao
				connection.commit();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}
	
	public boolean deleteEventoArtista(E object) {
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteArtistaEventoSQL.toString());
			ps.setLong(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

}
