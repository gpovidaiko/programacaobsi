package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.FilmeBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.PecaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class IngressoDAO extends PatternDAO<Ingresso> {
	private static StringBuilder insertSQL = new StringBuilder()
	.append("INSERT INTO INGRESSO ")
	.append("(ID_EVENTO, ID_COMPRA, ID_SETOR, CADEIRA_NUMERADA) ")
	.append("VALUES ")
	.append("(?, ?, ?, ?)");

	private static StringBuilder updateSQL = new StringBuilder()
	.append("UPDATE INGRESSO ")
	.append("SET ID_EVENTO = ?, ID_COMPRA = ?, ID_SETOR = ?, CADEIRA_NUMERADA = ? ")
	.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
	.append("DELETE FROM INGRESSO ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT i.ID, i.ID_EVENTO, i.ID_COMPRA, i.ID_SETOR, i.CADEIRA_NUMERADA, e.TIPO AS TIPO_EVENTO ")
	.append("FROM INGRESSO AS i INNER JOIN EVENTO AS e ")
	.append("ON i.ID_EVENTO = e.ID WHERE i.ID = ? ");

/*	private static StringBuilder selectIdEventoSQL =  new StringBuilder()
	.append("SELECT i.ID, i.ID_EVENTO, i.ID_COMPRA, i.ID_SETOR, i.CADEIRA_NUMERADA, e.TIPO AS TIPO_EVENTO ")
	.append("FROM i.INGRESSO INNER JOIN e.EVENTO ")
	.append(" ON i.ID_EVENTO = e.ID WHERE i.ID_EVENTO = ?"); */

	private static StringBuilder selectIdCompraSQL =  new StringBuilder()
	.append("SELECT i.ID, i.ID_EVENTO, i.ID_COMPRA, i.ID_SETOR, i.CADEIRA_NUMERADA, e.TIPO AS TIPO_EVENTO ")
	.append("FROM INGRESSO AS i INNER JOIN EVENTO AS e ")
	.append("ON i.ID_EVENTO = e.ID WHERE i.ID_COMPRA = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT i.ID, i.ID_EVENTO, i.ID_COMPRA, i.ID_SETOR, i.CADEIRA_NUMERADA, e.TIPO AS TIPO_EVENTO ")
	.append("FROM INGRESSO AS i INNER JOIN EVENTO AS e ")
	.append("ON i.ID_EVENTO = e.ID ");

	private static IngressoDAO instance = new IngressoDAO();
	
	private IngressoDAO() {
	}
	
	public static IngressoDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Ingresso object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getEvento().getId());
			ps.setDouble(2, object.getCompra().getId());
			ps.setDouble(3, object.getSetor().getId());
			ps.setInt(4, object.getCadeiraNumerada());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Ingresso object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setDouble(1, object.getEvento().getId());
			ps.setDouble(2, object.getCompra().getId());
			ps.setDouble(3, object.getSetor().getId());
			ps.setInt(4, object.getCadeiraNumerada());
			ps.setLong(5, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do update
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Ingresso object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setObject(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do delete
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public Ingresso retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Ingresso ingresso = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()){
				ingresso = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ingresso;
	}

	@Override
	public List<Ingresso> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ingresso> ingresso = new ArrayList<Ingresso>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				ingresso.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ingresso;
	}

/*	public List<Ingresso> retrieveAll(long idEvento) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ingresso> ingresso = new ArrayList<Ingresso>();
		try {
			ps = connection.prepareStatement(selectIdEventoSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				ingresso.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ingresso;
	} */

	public List<Ingresso> retrieveAll(long idCompra) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Ingresso> ingresso = new ArrayList<Ingresso>();
		try {
			ps = connection.prepareStatement(selectIdCompraSQL.toString());
			ps.setLong(1, idCompra);
			rs = ps.executeQuery();
			while(rs.next()){
				ingresso.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return ingresso;
	}

	@Override
	protected Ingresso populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Evento evento = null;
		if (rs.getString("TIPO_EVENTO").equals("f")) {
			evento = FilmeBC.getInstance().retrieve(rs.getLong("ID_EVENTO"));
		} else if (rs.getString("TIPO_EVENTO").equals("p")) {
			evento = PecaBC.getInstance().retrieve(rs.getLong("ID_EVENTO"));
		} else {
			evento = ShowBC.getInstance().retrieve(rs.getLong("ID_EVENTO"));
		}
		Setor setor = SetorBC.getInstance().retrieve(rs.getLong("ID_SETOR"));
		Ingresso ingresso = new Ingresso(evento, setor);
		ingresso.setId(rs.getLong("ID"));
		ingresso.setCadeiraNumerada(rs.getInt("CADEIRA_NUMERADA"));
		return ingresso;
	}

}
