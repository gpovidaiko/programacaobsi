package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Classe que representa o padrao DAO
 * @author Mauda
 */
public abstract class PatternDAO<E>{
	
	/////////////////////////////////////////
	// METODOS DML COM ALTERACAO NA BASE
	/////////////////////////////////////////
	
	/**
	 * Metodo responsavel por criar uma nova instancia da classe E no banco de
	 * dados. Deverá retornar o ID gerado pelo banco de dados
	 * @param object
	 * @return
	 */
	public abstract long create(E object);
	
	/**
	 * Metodo responsavel por atualizar uma instancia da classe E no banco de
	 * dados. Deverá retornar true se a operacao ocorreu com sucesso
	 * @param object
	 * @return
	 */
	public abstract boolean update(E object);
	
	/**
	 * Metodo responsavel por remover uma instancia da classe E do banco de
	 * dados. Deverá retornar true se a operacao ocorreu com sucesso
	 * @param object
	 * @return
	 */
	public abstract boolean delete(E object);
	
	/////////////////////////////////////////
	// METODOS DML DE RECUPERACAO DE INFORMACAO
	/////////////////////////////////////////
	
	/**
	 * Metodo responsavel por recuperar uma instancia E do banco de dados a
	 * partir do id passado como parametro. Caso não exista objeto retornar
	 * um objeto nulo
	 * @param object
	 * @return
	 */
	public abstract E retrieve(long id);
	
	/**
	 * Metodo responsavel por recuperar uma lista de instancia E do banco de
	 * dados. Caso nao hajam restricoes recuperara todas as linhas de uma
	 * tabela
	 * @param object
	 * @return
	 */
	public abstract List<E> retrieveAll();
	
	/**
	 * Metodo que obtem a chava primaria gerada apos o insert de um item no 
	 * @param rs
	 * @param ps
	 * @return
	 * @throws SQLException
	 */
	protected Long retrievePrimaryKeygenerated(ResultSet rs, PreparedStatement ps) throws SQLException{
		//Obtem o ID gerado pelo banco HSQLDB
		rs = ps.getGeneratedKeys();
		if(rs.next()){
			return rs.getLong(1);
		}
		return null;
	}
	
	/**
	 * Metodo responsavel por popular uma instancia E, a partir do ResultSet.
	 * Caso seja necessario fornecedor informacoes a mais para construir o
	 * objeto corretamente, utilizar os parametros variaveis, passando os 
	 * objetos necessarios
	 * @param rs
	 * @param object
	 * @return
	 * @throws SQLException
	 */
	protected abstract E populateObject(ResultSet rs) throws SQLException;
	

}
