package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CasaShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CinemaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.TeatroBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoEventoEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class PecaDAO extends EventoDAO<Peca>{

	private static StringBuilder insertSQL = new StringBuilder()
		.append("INSERT INTO PECA ")
		.append("(ID_EVENTO, GENERO, COMPANHIA) ")
		.append("VALUES ")
		.append("(?,?,?)");
	
	private static StringBuilder updateSQL = new StringBuilder()
		.append("UPDATE PECA ")
		.append("SET GENERO = ?, COMPANHIA = ? ")
		.append("WHERE ID_EVENTO = ?");
	
	private static StringBuilder deleteSQL = new StringBuilder()
		.append("DELETE FROM PECA ")
		.append("WHERE ID_EVENTO = ? ");
	
	private static StringBuilder selectIdSQL =  new StringBuilder()
		.append("SELECT e.ID, e.ID_AMBIENTE, e.ID_DIRETOR, e.TIPO, e.NOME, e.DESCRICAO, e.DATAINCLUSAO, e.DATAEVENTO, e.CENSURA, p.GENERO, p.COMPANHIA, a.TIPO AS TIPO_AMBIENTE ")
		.append("FROM EVENTO AS e INNER JOIN PECA AS p ON e.ID = p.ID_EVENTO ")
		.append("INNER JOIN AMBIENTE AS a ON e.ID_AMBIENTE = a.ID WHERE e.ID = ? ");
	
	private static StringBuilder selectAllSQL =  new StringBuilder()
		.append("SELECT e.ID, e.ID_AMBIENTE, e.ID_DIRETOR, e.TIPO, e.NOME, e.DESCRICAO, e.DATAINCLUSAO, e.DATAEVENTO, e.CENSURA, p.GENERO, p.COMPANHIA, a.TIPO AS TIPO_AMBIENTE ")
		.append("FROM EVENTO AS e INNER JOIN PECA AS p ON e.ID = p.ID_EVENTO ")
		.append("INNER JOIN AMBIENTE AS a ON e.ID_AMBIENTE = a.ID ");

	private static PecaDAO instance = new PecaDAO();
	
	private PecaDAO() {
	}
	
	public static PecaDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Peca object) {
		// TODO Auto-generated method stub
		super.create(object);

		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString());
			ps.setLong(1, object.getId());
			ps.setString(2, object.getGenero());
			ps.setString(3, object.getCompanhia());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Peca object) {
		// TODO Auto-generated method stub
		super.update(object);

		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setString(1, object.getGenero());
			ps.setString(2, object.getCompanhia());
			ps.setLong(3, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Peca object) {
		// TODO Auto-generated method stub
		super.update(object);

		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setLong(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public Peca retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Peca peca = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				peca = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return peca;
	}

	@Override
	public List<Peca> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Peca> pecas = new ArrayList<Peca>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				pecas.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return pecas;
	}

	@Override
	protected Peca populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Ambiente ambiente;
		if (rs.getString("TIPO_AMBIENTE").equals("c")) {
			Cinema cinema = CinemaBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = cinema;
		} else if (rs.getString("TIPO_AMBIENTE").equals("s")) {
			CasaShow casaShow = CasaShowBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = casaShow;
		} else {
			Teatro teatro = TeatroBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = teatro;
		}
		Diretor diretor = DiretorBC.getInstance().retrieve(rs.getLong("ID_DIRETOR"));
		List<Artista> artistas = ArtistaBC.getInstance().retrieveAll(rs.getLong("ID"));
		Peca peca = new Peca(ambiente, artistas, diretor);
		peca.setId(rs.getLong("ID"));
		peca.setNome(rs.getString("NOME"));
		peca.setDescricao(rs.getString("DESCRICAO"));
		peca.setDataInclusao(new Date(rs.getDate("DATAINCLUSAO").getTime()));
		peca.setDataEvento(new Date(rs.getDate("DATAEVENTO").getTime()));
		peca.setCensura(rs.getInt("CENSURA"));
		peca.setGenero(rs.getString("GENERO"));
		peca.setCompanhia(rs.getString("COMPANHIA"));
		if (rs.getString("TIPO").equals("f")) {
			peca.setTipoEvento(TipoEventoEnum.FILME);
		} else if (rs.getString("TIPO").equals("p")) {
			peca.setTipoEvento(TipoEventoEnum.PECA);
		} else {
			peca.setTipoEvento(TipoEventoEnum.SHOW);
		}
		return peca;
	}

}
