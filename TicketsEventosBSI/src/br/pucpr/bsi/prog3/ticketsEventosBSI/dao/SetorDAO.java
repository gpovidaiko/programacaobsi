package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CasaShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CinemaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.TeatroBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class SetorDAO extends PatternDAO<Setor> {
	private static StringBuilder insertSQL = new StringBuilder()
	.append("INSERT INTO SETOR ")
	.append("(ID_AMBIENTE, NOME, CAPACIDADE, PRECO) ")
	.append("VALUES ")
	.append("(?,?,?,?)");

	private static StringBuilder updateSQL = new StringBuilder()
	.append("UPDATE SETOR ")
	.append("SET ID_AMBIENTE = ?, NOME = ?, CAPACIDADE = ?, PRECO = ? ")
	.append("WHERE ID = ? ");

	private static StringBuilder deleteSQL = new StringBuilder()
	.append("DELETE FROM SETOR ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT s.ID, s.ID_AMBIENTE, s.NOME, s.CAPACIDADE, s.PRECO, a.TIPO AS TIPO_AMBIENTE ")
	.append("FROM SETOR AS s JOIN AMBIENTE AS a ")
	.append("ON a.ID = s.ID_AMBIENTE WHERE s.ID = ? ");

	private static StringBuilder selectIdAmbienteSQL =  new StringBuilder()
	.append("SELECT s.ID, s.ID_AMBIENTE, s.NOME, s.CAPACIDADE, s.PRECO, a.TIPO AS TIPO_AMBIENTE ")
	.append("FROM SETOR AS s JOIN AMBIENTE AS a ")
	.append("ON a.ID = s.ID_AMBIENTE WHERE s.ID_AMBIENTE = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT s.ID, s.ID_AMBIENTE, s.NOME, s.CAPACIDADE, s.PRECO, a.TIPO AS TIPO_AMBIENTE ")
	.append("FROM SETOR AS s JOIN AMBIENTE AS a ")
	.append("ON a.ID = s.ID_AMBIENTE ");

	private static SetorDAO instance = new SetorDAO();
	
	private SetorDAO() {
	}
	
	public static SetorDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Setor object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, object.getAmbiente().getId());
			ps.setString(2, object.getNome());
			ps.setInt(3, object.getCapacidade());
			ps.setDouble(4, object.getPreco());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
			//Obtem o ID gerado pelo banco HSQLDB
			object.setId(retrievePrimaryKeygenerated(rs, ps));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Setor object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setDouble(1, object.getAmbiente().getId());
			ps.setString(2, object.getNome());
			ps.setInt(3, object.getCapacidade());
			ps.setDouble(4, object.getPreco());
			ps.setLong(5, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do update
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Setor object) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setObject(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit do delete
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(null, ps, connection);
 		}
		return true;
	}

	@Override
	public Setor retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Setor setor = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				setor = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return setor;
	}

	@Override
	public List<Setor> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Setor> setores = new ArrayList<Setor>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				setores.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return setores;
	}

	public List<Setor> retrieveAll(Ambiente ambiente) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Setor> setores = new ArrayList<Setor>();
		try {
			ps = connection.prepareStatement(selectIdAmbienteSQL.toString());
			ps.setLong(1, ambiente.getId());
			rs = ps.executeQuery();
			while (rs.next()){
				setores.add(populateObject(rs, ambiente));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return setores;
	}

	@Override
	protected Setor populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Ambiente ambiente = null;
		if (rs.getString("TIPO_AMBIENTE").equals("c")) {
			Cinema cinema = CinemaBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = AmbienteBC.getInstance(cinema).retrieve(rs.getLong("ID_AMBIENTE"));
		} else if (rs.getString("TIPO_AMBIENTE").equals("s")) {
			CasaShow casaShow = CasaShowBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = AmbienteBC.getInstance(casaShow).retrieve(rs.getLong("ID_AMBIENTE"));
		} else {
			Teatro teatro = TeatroBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = AmbienteBC.getInstance(teatro).retrieve(rs.getLong("ID_AMBIENTE"));
		}
		Setor setor = new Setor(ambiente);
		setor.setId(rs.getLong("ID"));
		setor.setNome(rs.getString("NOME"));
		setor.setPreco(rs.getFloat("PRECO"));
		setor.setCapacidade(rs.getInt("CAPACIDADE"));
		return setor;
	}

	protected Setor populateObject(ResultSet rs, Ambiente ambiente) throws SQLException {
		// TODO Auto-generated method stub
		Setor setor = new Setor(ambiente);
		setor.setId(rs.getLong("ID"));
		setor.setNome(rs.getString("NOME"));
		setor.setPreco(rs.getFloat("PRECO"));
		setor.setCapacidade(rs.getInt("CAPACIDADE"));
		return setor;
	}
}
