package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CasaShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CinemaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.TeatroBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class ShowDAO extends EventoDAO<Show> {

	private static StringBuilder insertSQL = new StringBuilder()
		.append("INSERT INTO SHOW ")
		.append("(ID_EVENTO, ESTILO) ")
		.append("VALUES ")
		.append("(?,?)");
	
	private static StringBuilder updateSQL = new StringBuilder()
		.append("UPDATE SHOW ")
		.append("SET ESTILO = ? ")
		.append("WHERE ID_EVENTO = ?");
	
	private static StringBuilder deleteSQL = new StringBuilder()
		.append("DELETE FROM SHOW ")
		.append("WHERE ID_EVENTO = ? ");
	
	private static StringBuilder selectIdSQL =  new StringBuilder()
		.append("SELECT e.ID, e.ID_AMBIENTE, e.ID_DIRETOR, e.TIPO, e.NOME, e.DESCRICAO, e.DATAINCLUSAO, e.DATAEVENTO, e.CENSURA, s.ESTILO, a.TIPO AS TIPO_AMBIENTE ")
		.append("FROM EVENTO AS e INNER JOIN SHOW AS s ON e.ID = s.ID_EVENTO ")
		.append("INNER JOIN AMBIENTE AS a ON e.ID_AMBIENTE = a.ID WHERE e.ID = ? ");
	
	private static StringBuilder selectAllSQL =  new StringBuilder()
		.append("SELECT e.ID, e.ID_AMBIENTE, e.ID_DIRETOR, e.TIPO, e.NOME, e.DESCRICAO, e.DATAINCLUSAO, e.DATAEVENTO, e.CENSURA, s.ESTILO, a.TIPO AS TIPO_AMBIENTE ")
		.append("FROM EVENTO AS e INNER JOIN SHOW AS s ON e.ID = s.ID_EVENTO ")
		.append("INNER JOIN AMBIENTE AS a ON e.ID_AMBIENTE = a.ID ");

	private static ShowDAO instance = new ShowDAO();
	
	private ShowDAO() {
	}
	
	public static ShowDAO getInstance() {
		return instance;
	}

	@Override
	public long create(Show object) {
		// TODO Auto-generated method stub
		super.create(object);

		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(insertSQL.toString());
			ps.setLong(1, object.getId());
			ps.setString(2, object.getEstilo());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return object.getId();
	}

	@Override
	public boolean update(Show object) {
		// TODO Auto-generated method stub
		super.update(object);

		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(updateSQL.toString());
			ps.setString(1, object.getEstilo());
			ps.setLong(2, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public boolean delete(Show object) {
		// TODO Auto-generated method stub
		super.update(object);

		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//O commit nao eh realizado automaticamente - Inicio da Transacao
			connection.setAutoCommit(false);
			
			ps = connection.prepareStatement(deleteSQL.toString());
			ps.setLong(1, object.getId());
			
			ps.executeUpdate();
			
			//Realiza o commit da insercao
			connection.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return true;
	}

	@Override
	public Show retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Show show = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				show = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return show;
	}

	@Override
	public List<Show> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Show> shows = new ArrayList<Show>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				shows.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return shows;
	}

	@Override
	protected Show populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Ambiente ambiente;
		if (rs.getString("TIPO_AMBIENTE").equals("c")) {
			Cinema cinema = CinemaBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = cinema;
		} else if (rs.getString("TIPO_AMBIENTE").equals("s")) {
			CasaShow casaShow = CasaShowBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = casaShow;
		} else {
			Teatro teatro = TeatroBC.getInstance().retrieve(rs.getLong("ID_AMBIENTE"));
			ambiente = teatro;
		}
		Diretor diretor = DiretorBC.getInstance().retrieve(rs.getLong("ID_DIRETOR"));
		List<Artista> artistas = ArtistaBC.getInstance().retrieveAll(rs.getLong("ID"));
		Show show = new Show(ambiente, artistas, diretor);
		show.setId(rs.getLong("ID"));
//		show.setTipoEvento(TipoEventoEnum.valueOf(rs.getString("TIPO")));
		show.setNome(rs.getString("NOME"));
		show.setDescricao(rs.getString("DESCRICAO"));
		show.setDataInclusao(new Date(rs.getDate("DATAINCLUSAO").getTime()));
		show.setDataEvento(new Date(rs.getDate("DATAEVENTO").getTime()));
		show.setCensura(rs.getInt("CENSURA"));
		show.setEstilo(rs.getString("ESTILO"));
		return show;
	}

}
