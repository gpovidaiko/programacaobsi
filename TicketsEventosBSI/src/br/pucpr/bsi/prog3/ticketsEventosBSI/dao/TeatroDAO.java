package br.pucpr.bsi.prog3.ticketsEventosBSI.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.Conexao;

public class TeatroDAO extends AmbienteDAO<Teatro> {

	private static StringBuilder selectIdSQL =  new StringBuilder()
	.append("SELECT ID, ID_ENDERECO, TIPO, NOME, DESCRICAO ")
	.append("FROM AMBIENTE ")
	.append("WHERE ID = ? ");

	private static StringBuilder selectAllSQL =  new StringBuilder()
	.append("SELECT ID, ID_ENDERECO, TIPO, NOME, DESCRICAO ")
	.append("FROM AMBIENTE ")
	.append("WHERE TIPO = \"t\" ");

	private static TeatroDAO instance = new TeatroDAO();
	
	private TeatroDAO() {
	}
	
	public static TeatroDAO getInstance() {
		return instance;
	}

	@Override
	public Teatro retrieve(long id) {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Teatro teatro = null;
		try {
			ps = connection.prepareStatement(selectIdSQL.toString());
			ps.setObject(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				teatro = populateObject(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return teatro;
	}

	@Override
	public List<Teatro> retrieveAll() {
		// TODO Auto-generated method stub
		Connection connection = Conexao.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Teatro> teatros = new ArrayList<Teatro>();
		try {
			ps = connection.prepareStatement(selectAllSQL.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				teatros.add(populateObject(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new TicketsEventosBSIException("Problemas no sistema, por favor tente mais tarde");
		} finally {
			Conexao.close(rs, ps, connection);
 		}
		return teatros;
	}

	@Override
	protected Teatro populateObject(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Endereco endereco = EnderecoBC.getInstance().retrieve(rs.getLong("ID_ENDERECO"));
		Teatro teatro = new Teatro(endereco);
		teatro.setId(rs.getLong("ID"));
		teatro.setNome(rs.getString("NOME"));
		teatro.setDescricao(rs.getString("DESCRICAO"));
		teatro.setSetores(SetorBC.getInstance().retrieveAll(teatro));
		return teatro;
	}

}
