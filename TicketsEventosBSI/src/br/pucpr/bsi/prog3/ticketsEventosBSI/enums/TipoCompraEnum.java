package br.pucpr.bsi.prog3.ticketsEventosBSI.enums;

public enum TipoCompraEnum {
	RESERVADO('r', "reservado"),
	VENDIDO('v', "vendido");
	
	private char tipo;
	private Object descricao;

	private TipoCompraEnum(char tipo, String descricao) {
		// TODO Auto-generated constructor stub
		this.setTipo(tipo);
		this.setDescricao(descricao);
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public Object getDescricao() {
		return descricao;
	}

	public void setDescricao(Object descricao) {
		this.descricao = descricao;
	}
}
