package br.pucpr.bsi.prog3.ticketsEventosBSI.enums;

public enum TipoEventoEnum {
	FILME('f', "filme"),
	PECA('p', "peca"),
	SHOW('s', "show");
	
	private char tipo;
	private Object descricao;

	private TipoEventoEnum(char tipo, String descricao) {
		// TODO Auto-generated constructor stub
		this.setTipo(tipo);
		this.setDescricao(descricao);
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public Object getDescricao() {
		return descricao;
	}

	public void setDescricao(Object descricao) {
		this.descricao = descricao;
	}
}
