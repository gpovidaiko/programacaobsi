package br.pucpr.bsi.prog3.ticketsEventosBSI.interfaces;

public interface Autenticavel {
	public String getUser();
	
	public String getSenha();
}
