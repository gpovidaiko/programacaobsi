package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.ArrayList;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoAmbienteEnum;

public abstract class Ambiente {
	private long id;
	private int capacidade;
	private String nome;
	private String descricao;
	private Endereco endereco;
	private List<Evento> eventos;
	private List<Setor> setores;
	private TipoAmbienteEnum tipoAmbiente;
	
	public Ambiente(Endereco endereco) {
		this.setEndereco(endereco);
		this.setSetores(new ArrayList<Setor>());
		this.setEventos(new ArrayList<Evento>());
		if (this instanceof CasaShow) {
			this.setTipoAmbiente(TipoAmbienteEnum.CASASHOW);
		} else if (this instanceof Cinema) {
			this.setTipoAmbiente(TipoAmbienteEnum.CINEMA);
		} else {
			this.setTipoAmbiente(TipoAmbienteEnum.TEATRO);
		}
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public int getCapacidade() {
		capacidade = 0;
		if(this.setores != null && this.setores.size() > 0) {
			for (Setor setor: setores) {
				capacidade += setor.getCapacidade();
			}
		}
		return capacidade;
	}
	
	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public TipoAmbienteEnum getTipoAmbiente() {
		return tipoAmbiente;
	}

	public void setTipoAmbiente(TipoAmbienteEnum tipoAmbiente) {
		this.tipoAmbiente = tipoAmbiente;
	}

	public List<Setor> getSetores() {
		return setores;
	}

	public void setSetores(List<Setor> setores) {
		this.setores = setores;
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" + 
					"Id: " + this.getId() + ", " +
					"Capacidade: " + this.getCapacidade() + ", " +
					"Nome: " + this.getNome() + ", " +
					"Descri��o: " + this.getDescricao() + ", " +
					"Endere�o: " + this.getEndereco() + ", " +
					"Eventos: " + this.getEventos() + ", " +
					"Setores: " + this.getSetores() + ", " +
					"Tipo de Ambiente: " + this.getTipoAmbiente() + "]";
		return retorno;
	}
}