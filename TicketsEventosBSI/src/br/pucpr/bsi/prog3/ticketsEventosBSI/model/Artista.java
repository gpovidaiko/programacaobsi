package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.ArrayList;

public class Artista {
	private long id;
	private String nome;
	private ArrayList<Evento> eventos;

	public Artista() {
		this.setEventos(new ArrayList<Evento>());
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(ArrayList<Evento> eventos) {
		this.eventos = eventos;
	}

	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Nome: " + this.getNome() + "]";
		return retorno;
	}

	@Override
	public boolean equals(Object obj) {
//		return super.equals(arg0);
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Artista other = (Artista) obj;
		if (this.id != other.id)
			return false;
		return true;
	}
}
