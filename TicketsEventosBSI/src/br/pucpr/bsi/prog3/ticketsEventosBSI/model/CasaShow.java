package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

public class CasaShow extends Ambiente {

	public CasaShow(Endereco endereco) {
		super(endereco);
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					super.toString() + "]";
		return retorno;
	}
}
