package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

public class Cinema extends Ambiente {

	public Cinema(Endereco endereco) {
		super(endereco);
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					super.toString() + "]";
		return retorno;
	}
}
