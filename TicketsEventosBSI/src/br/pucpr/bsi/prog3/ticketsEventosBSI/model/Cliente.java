package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.ArrayList;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.PapelSistemaEnum;

public class Cliente extends Usuario {
	private ArrayList<Compra> compras;
	private static final PapelSistemaEnum PAPEL = null;

	public Cliente(Endereco endereco) {
		super(endereco);
		this.setCompras(new ArrayList<Compra>());
	}

	public static PapelSistemaEnum getPapel() {
		return PAPEL;
	}

	public ArrayList<Compra> getCompras() {
		return compras;
	}

	public void setCompras(ArrayList<Compra> compras) {
		this.compras = compras;
	}
}
