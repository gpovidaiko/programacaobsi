package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.ArrayList;
import java.util.List;
import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;

public class Compra {
	private long id;
	private Cliente cliente;
	private List<Ingresso> ingressos;
	private TipoCompraEnum situacao;
	
	public Compra(Cliente cliente, List<Ingresso> ingressos) {
		this.setCliente(cliente);
		this.setIngressos(ingressos);
		this.setSituacao(null);
	}
	
	public Compra(Cliente cliente, Ingresso ingresso) {
		this(cliente, new ArrayList<Ingresso>());
		this.addIngresso(ingresso);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public TipoCompraEnum getSituacao() {
		return situacao;
	}
	
	public void setSituacao(TipoCompraEnum situacao) {
		this.situacao = situacao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		if( cliente != null ) {
			cliente.getCompras().add(this);
		}
		this.cliente = cliente;
	}

	public List<Ingresso> getIngressos() {
		return ingressos;
	}

	public void setIngressos(List<Ingresso> ingressos) {
		if( ingressos != null ) {
			for (Ingresso ingresso : ingressos) {
				ingresso.setCompra(this);
			}
		}
		this.ingressos = ingressos;
	}
	
	public void addIngresso(Ingresso ingresso) {
		ingresso.setCompra(this);
		this.getIngressos().add(ingresso);
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Cliente: " + this.getCliente() + ", " +
					"Ingresso: " + this.getIngressos() + ", " +
					"Situacao: " + this.getSituacao() + "]";
		return retorno;
	}
}