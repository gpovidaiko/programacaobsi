package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.ArrayList;
import java.util.List;

public class Diretor {
	private long id;
	private String nome;
	private List<Evento> eventos;
	
	public Diretor() {
		this.setEventos(new ArrayList<Evento>());
	}
	
	public long getId() {
	
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}
	
	public void addEvento(Evento evento) {
		this.getEventos().add(evento);
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Nome: " + this.getNome() + "]";
		return retorno;
	}
}
