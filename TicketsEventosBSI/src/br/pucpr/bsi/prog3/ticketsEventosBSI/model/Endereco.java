package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

public class Endereco {
	private long id;
	private int numero;
	private String rua;
	private String complemento;
	private String bairro;
	private String cidade;
	private String estado;
	private String pais;

	public Endereco() { }
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getRua() {
		return rua;
	}
	
	public void setRua(String rua) {
		this.rua = rua;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getComplemento() {
		return complemento;
	}
	
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public String getBairro() {
		return bairro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getPais() {
		return pais;
	}
	
	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Rua: " + this.getRua() + ", " +
					"Numero: " + this.getNumero() + ", " +
					"Complemento: " + this.getComplemento() + ", " +
					"Bairro: " + this.getBairro() + ", " +
					"Cidade: " + this.getCidade() + ", " +
					"Estado: " + this.getEstado() + ", " +
					"Pais: " + this.getPais() + "]";
		return retorno;
	}
}
