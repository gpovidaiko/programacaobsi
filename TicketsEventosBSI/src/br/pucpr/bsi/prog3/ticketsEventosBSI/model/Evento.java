package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoEventoEnum;

public abstract class Evento {
	private long id;
	private int censura;
	private String nome;
	private String descricao;
	private Date dataInclusao;
	private Date dataEvento;
	private Diretor diretor;
	private Ambiente ambiente;
	private List<Ingresso> ingressos;
	private List<Artista> artistas;
	private TipoEventoEnum tipoEvento;
	
	public Evento(Ambiente ambiente, Artista artista, Diretor diretor) {
		this.setArtistas(new ArrayList<Artista>());
		this.setIngressos(new ArrayList<Ingresso>());
		this.addArtista(artista);
		this.setDiretor(diretor);
		this.setAmbiente(ambiente);
		this.setDataInclusao(new Date());
		if (this instanceof Filme) {
			this.setTipoEvento(TipoEventoEnum.FILME);
		} else if (this instanceof Peca) {
			this.setTipoEvento(TipoEventoEnum.PECA);
		} else {
			this.setTipoEvento(TipoEventoEnum.SHOW);
		}
	}
	
	public Evento(Ambiente ambiente, List<Artista> artistas, Diretor diretor) {
		this.setArtistas(artistas);
		this.setIngressos(new ArrayList<Ingresso>());
		this.setDiretor(diretor);
		this.setAmbiente(ambiente);
		this.setDataInclusao(new Date());
		this.setTipoEvento(null);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Date getDataInclusao() {
		return dataInclusao;
	}
	
	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	public Date getDataEvento() {
		return dataEvento;
	}
	
	public void setDataEvento(Date dataEvento) {
		this.dataEvento = dataEvento;
	}
	
	public int getCensura() {
		return censura;
	}
	
	public void setCensura(int censura) {
		this.censura = censura;
	}

	public List<Ingresso> getIngressos() {
		return ingressos;
	}

	public void setIngressos(List<Ingresso> ingressos) {
		this.ingressos = ingressos;
	}
	
	public void addIngresso(Ingresso ingresso) {
		this.getIngressos().add(ingresso);
	}

	public Diretor getDiretor() {
		return diretor;
	}

	public void setDiretor(Diretor diretor) {
		if( diretor != null ) {
			diretor.addEvento(this);
		}
		this.diretor = diretor;
	}

	public TipoEventoEnum getTipoEvento() {
		return tipoEvento;
	}

	public void setTipoEvento(TipoEventoEnum tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public List<Artista> getArtistas() {
		return artistas;
	}

	public void setArtistas(List<Artista> artistas) {
		this.artistas = artistas;
	}
	
	public void addArtista(Artista artista) {
		artista.getEventos().add(this);
		this.getArtistas().add(artista);
	}

	public Ambiente getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(Ambiente ambiente) {
		if( ambiente != null ) {
			ambiente.getEventos().add(this);
		}
		this.ambiente = ambiente;
	}

	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Nome: " + this.getNome() + ", " +
					"Censura: " + this.getCensura() + ", " +
					"DataEvento: " + this.getDataEvento() + ", " +
					"DataInclusao: " + this.getDataInclusao() + ", " +
					"Diretor: " + this.getDiretor() + ", " +
					"Artistas: " + this.getArtistas() + ", " +
					"TipoEvento: " + this.getTipoEvento() + "]";
		return retorno;
	}
}
