package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.List;

public class Filme extends Evento {
	private String genero;

	public Filme(Ambiente ambiente, Artista artista, Diretor diretor) {
		super(ambiente, artista, diretor);
	}

	public Filme(Ambiente ambiente, List<Artista> artistas, Diretor diretor) {
		// TODO Auto-generated constructor stub
		super(ambiente, artistas, diretor);
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	@Override
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					super.toString() + ", " +
					"Genero: " + this.getGenero() + "]";
		return retorno;
	}
}
