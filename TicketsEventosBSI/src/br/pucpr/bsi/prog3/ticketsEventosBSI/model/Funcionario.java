package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.PapelSistemaEnum;

public class Funcionario extends Usuario {
	private static final PapelSistemaEnum PAPEL = null;

	public Funcionario(Endereco endereco) {
		super(endereco);
	}

	public static PapelSistemaEnum getPapel() {
		return PAPEL;
	}
}
