package br.pucpr.bsi.prog3.ticketsEventosBSI.model;


public class Ingresso {
	private long id;
	private int cadeiraNumerada;
	private Compra compra;
	private Evento evento;
	private Setor setor;
	
	public Ingresso(Evento evento, Setor setor) {
		this.setEvento(evento);
		this.setSetor(setor);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getCadeiraNumerada() {
		return cadeiraNumerada;
	}
	
	public void setCadeiraNumerada(int cadeiraNumerada) {
		this.cadeiraNumerada = cadeiraNumerada;
	}
	
	public Setor getSetor() {
		return setor;
	}
	
	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		if( evento != null ) {
			evento.getIngressos().add(this);
		}
		this.evento = evento;
	}

	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"CadeiraNumerada: " + this.getCadeiraNumerada() + ", " +
					"Evento: " + this.getEvento() + ", " +
					"Setor: " + this.getSetor() + "]";
		return retorno;
	}

	@Override
	public boolean equals(Object obj) {
//		return super.equals(arg0);
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Ingresso other = (Ingresso) obj;
		if (this.id != other.id)
			return false;
		return true;
	}
}
