package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.List;

public class Peca extends Evento {
	private String companhia;
	private String genero;
	
	public Peca(Ambiente ambiente, Artista artista, Diretor diretor) {
		super(ambiente, artista, diretor);
	}

	public Peca(Ambiente ambiente, List<Artista> artistas, Diretor diretor) {
		super(ambiente, artistas, diretor);
	}

	public String getCompanhia() {
		return companhia;
	}
	
	public void setCompanhia(String companhia) {
		this.companhia = companhia;
	}
	
	public String getGenero() {
		return genero;
	}
	
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					super.toString() + ", " +
					"Companhia: " + this.companhia + ", " +
					"Genero: " + this.genero + "]";
		return retorno;
	}
}
