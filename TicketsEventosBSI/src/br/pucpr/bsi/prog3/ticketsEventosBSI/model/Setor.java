package br.pucpr.bsi.prog3.ticketsEventosBSI.model;


public class Setor {
	private long id;
	private String nome;
	private int capacidade;
	private double preco;
	private Ambiente ambiente;

	public Setor(Ambiente ambiente) {
		this.setAmbiente(ambiente);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getCapacidade() {
		return capacidade;
	}
	
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	
	public double getPreco() {
		return preco;
	}
	
	public void setPreco(double preco) {
		this.preco = preco;
	}

	public Ambiente getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(Ambiente ambiente) {
		if( ambiente != null ) {
			ambiente.getSetores().add(this);
		}
		this.ambiente = ambiente;
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Nome: " + this.getNome() + ", " +
					"Capacidade: " + this.getCapacidade() + ", " +
					"Preco: " + this.getPreco() + "]";
		return retorno;
	}

	@Override
	public boolean equals(Object obj) {
//		return super.equals(arg0);
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Setor other = (Setor) obj;
		if (this.id != other.id)
			return false;
		return true;
	}
}
