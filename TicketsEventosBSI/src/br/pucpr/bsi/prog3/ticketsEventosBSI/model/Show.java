package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.List;

public class Show extends Evento {
	private String estilo;

	public Show(Ambiente ambiente, Artista artista, Diretor diretor) {
		super(ambiente, artista, diretor);
	}

	public Show(Ambiente ambiente, List<Artista> artistas, Diretor diretor) {
		super(ambiente, artistas, diretor);
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}
	
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					super.toString() + ", " +
					"Estilo: " + this.getEstilo() + "]";
		return retorno;
	}
}
