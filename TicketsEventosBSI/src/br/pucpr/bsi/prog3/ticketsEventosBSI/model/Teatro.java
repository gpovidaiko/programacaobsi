package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

public class Teatro extends Ambiente {

	public Teatro(Endereco endereco) {
		super(endereco);
	}
	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					super.toString() + "]";
		return retorno;
	}
}
