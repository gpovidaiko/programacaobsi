package br.pucpr.bsi.prog3.ticketsEventosBSI.model;

import java.util.Date;

import br.pucpr.bsi.prog3.ticketsEventosBSI.interfaces.Autenticavel;

public abstract class Usuario implements Autenticavel {
	private long id;
	private String nome;
	private String cpf;
	private String email;
	private String telefone;
	private String user;
	private String senha;
	private Date dataNascimento;
	private Endereco endereco;
	
	public Usuario(Endereco endereco) {
		this.setEndereco(endereco);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public Date getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String toString() {
		String retorno = null;
		retorno = this.getClass().getName() + " [" +
					"Id: " + this.getId() + ", " +
					"Nome: " + this.getNome() + ", " +
					"CPF: " + this.getCpf() + ", " +
					"Email: " + this.getEmail() + ", " +
					"Telefone: " + this.getTelefone() + ", " +
					"User: " + this.getUser() + ", " +
					"Senha: " + this.getSenha() + ", " +
					"Datanascimento: " + this.getDataNascimento() + ", " +
					"Endereco: " + this.getEndereco() + "]";
		return retorno;
	}
}