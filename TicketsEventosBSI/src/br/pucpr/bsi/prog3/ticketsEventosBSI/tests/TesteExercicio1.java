package br.pucpr.bsi.prog3.ticketsEventosBSI.tests;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;

/**
 * Essa classe eh um teste JUnit para o laboratorio 1 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicio1 {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	
	private Endereco endereco;
	
	private Funcionario funcionario;
	private Cliente cliente;	
	
	private Setor setor;
	
	private CasaShow casaShow;
	private Cinema cinema;
	private Teatro teatro;
	
	private Artista artista;	
	private Diretor diretor;
	
	private Filme filme;
	private Peca peca;
	private Show show;
	
	private Ingresso ingresso;	
	private Compra compra;	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void TesteGeral(){
//		criarEndereco();
//		//Verifica se o endereco nao eh nulo
//		Assert.assertNotNull(endereco);
//		
//		criarFuncionario();
//		//Verifica se o funcionario nao eh nulo
//		Assert.assertNotNull(funcionario);
//
//		criarCliente();
//		//Verifica se o cliente nao eh nulo
//		Assert.assertNotNull(cliente);
//		
//		criarSetor();
//		//Verifica se o setor nao eh nulo
//		Assert.assertNotNull(setor);
//		
//		criarCasaShow();
//		//Verifica se a casashow nao eh nula
//		Assert.assertNotNull(casaShow);
//		
//		criarCinema();
//		//Verifica se o cinema nao eh nulo
//		Assert.assertNotNull(cinema);
//		
//		criarTeatro();
//		//Verifica se o teatro nao eh nulo
//		Assert.assertNotNull(teatro);
//		
//		criarArtista();
//		//Verifica se o artista nao eh nulo
//		Assert.assertNotNull(artista);
//		
//		criarDiretor();
//		//Verifica se o diretor nao eh nulo
//		Assert.assertNotNull(diretor);
//		
//		criarFilme();
//		//Verifica se o filme nao eh nulo
//		Assert.assertNotNull(filme);
//		
//		criarPeca();
//		//Verifica se a peca nao eh nula
//		Assert.assertNotNull(peca);
//		
//		criarShow();
//		//Verifica se o show nao eh nulo
//		Assert.assertNotNull(show);
//		
//		criarIngresso();
//		//verifica se o ingresso nao eh nulo
//		Assert.assertNotNull(ingresso);
//		
//		criarCompra();
//		//verifica se a compra nao eh nula
//		Assert.assertNotNull(compra);
//	}
//	
//	//////////////////////////////////////////
//	// METODOS AUXILIARES
//	//////////////////////////////////////////
//	
//	/**
//	 * Metodo responsavel por criar um endereco
//	 * @return
//	 */
//	public void criarEndereco(){
//		endereco = new Endereco();
//		endereco.setBairro("Afonso Pena");
//		endereco.setCidade("Sao Jose dos Pinhais");
//		endereco.setComplemento("Casa 10");
//		endereco.setEstado("Parana");
//		endereco.setNumero(200);
//		endereco.setPais("Brasil");
//		endereco.setRua("Afonso Pereira");
//		System.out.println(endereco.toString());
//	}
//	
//	
//	/**
//	 * Metodo responsavel por criar um funcionario
//	 */
//	private void criarFuncionario(){
//		funcionario = new Funcionario();
//		System.out.println(funcionario.toString());
//	}
//
//	/**
//	 * Metodo responsavel por criar um cliente
//	 */
//	public void criarCliente(){
//		cliente = new Cliente();
//		System.out.println(cliente.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar uma casashow
//	 */
//	public void criarSetor(){
//		setor = new Setor();
//		setor.setCapacidade(25);
//		setor.setNome("Setor A");
//		setor.setPreco(250.00);
//		System.out.println(setor.toString());
//	}
//	
//	
//	/**
//	 * Metodo responsavel por criar uma casashow
//	 */
//	public void criarCasaShow(){
//		casaShow = new CasaShow();
//		System.out.println(casaShow.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar um cinema
//	 */
//	public void criarCinema(){
//		cinema = new Cinema();
//		System.out.println(cinema.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar um teatro
//	 */
//	public void criarTeatro(){
//		teatro = new Teatro();
//		System.out.println(teatro.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar um artista
//	 * 
//	 */
//	public void criarArtista(){
//		artista = new Artista();
//		artista.setNome("Mc Donald");
//		System.out.println(artista.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar um diretor
//	 */
//	public void criarDiretor(){
//		diretor = new Diretor();
//		diretor.setNome("Fernando Meirelles");
//		System.out.println(diretor.toString());
//	}
//	
//	
//	/**
//	 * Metodo responsavel por criar um filme
//	 */
//	public void criarFilme(){
//		filme = new Filme();
//		filme.setGenero("Acao");
//		System.out.println(filme.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar um show
//	 */
//	public void criarShow(){
//		show = new Show();
//		show.setEstilo("Rock");
//		System.out.println(show.toString());
//	}
//	
//	/**
//	 * Metodo responsavel por criar uma peca
//	 */
//	public void criarPeca(){
//		peca = new Peca();
//		peca.setCompanhia("BSI");
//		peca.setGenero("Drama");
//		System.out.println(peca.toString());
//	}
//	
//	
//	/**
//	 * Metodo responsavel por criar um ingresso
//	 */
//	public void criarIngresso(){
//		ingresso = new Ingresso();
//		ingresso.setCadeiraNumerada(1);
//		System.out.println(ingresso.toString());
//		
//	}
//	
//	/**
//	 * Metodo responsavel por criar uma compra
//	 */
//	public void criarCompra(){
//		compra = new Compra();
//		compra.setSituacao(TipoCompraEnum.VENDIDO);
//		System.out.println(compra.toString());
	}
}
