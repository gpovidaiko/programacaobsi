package br.pucpr.bsi.prog3.ticketsEventosBSI.tests;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;

/**
 * Essa classe eh um teste JUnit para o laboratorio 2 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicio2 {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	
	private Funcionario funcionario;
	private Cliente cliente;	
	
	private CasaShow casaShow;
	private Cinema cinema;
	private Teatro teatro;
	
	private Filme filme;
	private Peca peca;
	private Show show;
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////
	
	@Test
	public void TesteGeral(){
		criaFuncionario();
		criaCasaShow();
		criaCinema();
		criaTeatro();
		criaFilme();
		criaPeca();
		criaShow();
		criaCliente();
		compraIngresso1();
		compraIngresso2();
	}
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	/**
	 * Metodo responsavel por criar um funcionario
	 */
	private void criaFuncionario(){
		//Cria um novo endereco
		Endereco endereco = this.novoEndereco1();
		//Verifica se o endereco nao eh nulo
		Assert.assertNotNull(endereco);
		
		//Metodo que cria um novo funcionario
		funcionario = new Funcionario(endereco);
		funcionario.setCpf("12308912388");
		funcionario.setDataNascimento(new Date());
		funcionario.setEmail("func1@ticketeventosbsi.com.br");
		funcionario.setNome("Funcionario 1");
		funcionario.setSenha("abc");
		funcionario.setTelefone("22334455");
		funcionario.setUser("func1");
		
		//Verifica se o funcionario nao eh nulo
		Assert.assertNotNull(funcionario);
		//Verifica se o endereco do funcionario eh igual ao endereco
		Assert.assertEquals(funcionario.getEndereco(), endereco);
		
		System.out.println(funcionario.toString());
	}
	
	/**
	 * Metodo responsavel por criar uma casashow
	 */
	private void criaCasaShow(){
		//Metodo que cria um novo endereco
		Endereco endereco = this.novoEndereco2();
		//Verifica se o endereco nao eh nulo
		Assert.assertNotNull(endereco);
		
		//Metodo que cria um ambiente
		casaShow = new CasaShow(endereco);
		casaShow.setDescricao("Casa de Shows CTBA");
		casaShow.setNome("Casa de Shows CTBA");
		
		
		
		//Verifica se a casashow nao eh nula
		Assert.assertNotNull(casaShow);
		//Verifica se o endereco da casashow eh igual ao endereco
		Assert.assertEquals(casaShow.getEndereco(), endereco);
		
		//Metodo que cria os setores do ambiente
		Setor setorA =  this.novoSetorA(casaShow);
		//Verifica se o setor nao eh nulo
		Assert.assertNotNull(setorA);
		//Verifica se a casashow contem o setorA
		Assert.assertTrue(casaShow.getSetores().contains(setorA));
		
		Setor setorB =  this.novoSetorB(casaShow);
		//Verifica se o setor nao eh nulo
		Assert.assertNotNull(setorB);
		//Verifica se a casashow contem o setorB
		Assert.assertTrue(casaShow.getSetores().contains(setorB));

		Setor setorC =  this.novoSetorC(casaShow);
		//Verifica se o setor nao eh nulo
		Assert.assertNotNull(setorC);
		//Verifica se a casashow contem o setorC
		Assert.assertTrue(casaShow.getSetores().contains(setorC));
		
		//Verifica se a capacidade da casaShow reflete a soma da capacidade dos setores
		Assert.assertEquals(casaShow.getCapacidade(), (setorA.getCapacidade() + setorB.getCapacidade() + setorC.getCapacidade()), 0);
		System.out.println(casaShow.toString());
	}
	
	/**
	 * Metodo responsavel por criar um cinema
	 */
	private void criaCinema(){
		//Metodo que cria um novo endereco
		Endereco endereco = this.novoEndereco3();
		//Verifica se o endereco nao eh nulo
		Assert.assertNotNull(endereco);
		
		//Metodo que cria um ambiente
		cinema = new Cinema(endereco);
		cinema.setDescricao("Cinema CTBA");
		cinema.setNome("Cinema CTBA");
		
		//Verifica se o cinema nao eh nulo
		Assert.assertNotNull(cinema);
		//Verifica se o endereco do cinema eh igual ao endereco
		Assert.assertEquals(cinema.getEndereco(), endereco);
		
		//Metodo que cria os setores do ambiente
		Setor setorC =  this.novoSetorC(cinema);
		//Verifica se o setor nao eh nulo
		Assert.assertNotNull(setorC);
		//Verifica se o cinema contem o setorC
		Assert.assertTrue(cinema.getSetores().contains(setorC));
		
		//Verifica se a capacidade do cinema reflete a soma da capacidade dos setores
		Assert.assertEquals(cinema.getCapacidade(), (setorC.getCapacidade()), 0);
		System.out.println(cinema.toString());
	}
	
	/**
	 * Metodo responsavel por criar um teatro
	 */
	private void criaTeatro(){
		//Metodo que cria um novo endereco
		Endereco endereco = this.novoEndereco1();
		//Verifica se o endereco nao eh nulo
		Assert.assertNotNull(endereco);
		
		//Metodo que cria um ambiente
		teatro = new Teatro(endereco);
		teatro.setDescricao("Teatro CTBA");
		teatro.setNome("Teatro CTBA");
		
		//Verifica se o teatro nao eh nulo
		Assert.assertNotNull(teatro);
		//Verifica se o endereco do teatro eh igual ao endereco
		Assert.assertEquals(teatro.getEndereco(), endereco);
		
		//Metodo que cria os setores do ambiente
		Setor setorB =  this.novoSetorB(teatro);
		//Verifica se o setor nao eh nulo
		Assert.assertNotNull(setorB);
		//Verifica se o teatro contem o setorB
		Assert.assertTrue(teatro.getSetores().contains(setorB));

		//Metodo que cria os setores do ambiente
		Setor setorC =  this.novoSetorC(teatro);
		//Verifica se o setor nao eh nulo
		Assert.assertNotNull(setorC);
		//Verifica se o teatro contem o setorC
		Assert.assertTrue(teatro.getSetores().contains(setorC));
		
		//Verifica se a capacidade do teatro reflete a soma da capacidade dos setores
		Assert.assertEquals(teatro.getCapacidade(), (setorB.getCapacidade() + setorC.getCapacidade()), 0);
		System.out.println(teatro.toString());
	}
	
	/**
	 * Metodo responsavel por criar um filme
	 */
	private void criaFilme(){
		//Metodo que cria um novo artista
		Artista artista = this.novoArtista();
		//Verifica se o artista nao eh nulo
		Assert.assertNotNull(artista);
		
		//Metodo que cria um novo diretor
		Diretor diretor = this.novoDiretor();
		//Verifica se o diretor nao eh nulo
		Assert.assertNotNull(diretor);
		
		//Metodo que cria um novo evento
		filme = new Filme(cinema, artista, diretor);
		filme.setCensura(18);
		filme.setDataEvento(new Date());
		filme.setDescricao("Filme para maiores");
		filme.setGenero("Acao");
		filme.setNome("CodeLine: Run for your lives!");
		
		//Verifica se o filme nao eh nulo
		Assert.assertNotNull(filme);
		//Verifica se o filme foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(filme));
		//Verifica se o filme foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(filme));

		System.out.println(filme.toString());
	}
	
	/**
	 * Metodo responsavel por criar um show
	 */
	private void criaShow(){
		//Metodo que cria um novo artista
		Artista artista = this.novoArtista();
		//Verifica se o artista nao eh nulo
		Assert.assertNotNull(artista);
		
		//Metodo que cria um novo diretor
		Diretor diretor = this.novoDiretor();
		//Verifica se o diretor nao eh nulo
		Assert.assertNotNull(diretor);
		
		//Metodo que cria um novo evento
		show = new Show(casaShow, artista, diretor);
		show.setCensura(18);
		show.setDataEvento(new Date());
		show.setDescricao("Show para maiores");
		show.setEstilo("Rock");
		show.setNome("Massacration");
		
		//Verifica se o show nao eh nulo
		Assert.assertNotNull(show);
		//Verifica se o show foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(show));
		//Verifica se o show foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(show));
		
		System.out.println(show.toString());
	}
	
	/**
	 * Metodo responsavel por criar uma peca
	 */
	private void criaPeca(){
		//Metodo que cria um novo artista
		Artista artista = this.novoArtista();
		//Verifica se o artista nao eh nulo
		Assert.assertNotNull(artista);
		
		//Metodo que cria um novo diretor
		Diretor diretor = this.novoDiretor();
		//Verifica se o diretor nao eh nulo
		Assert.assertNotNull(diretor);
		
		//Metodo que cria um novo evento
		peca = new Peca(teatro, artista, diretor);
		peca.setCensura(18);
		peca.setCompanhia("BSI");
		peca.setDataEvento(new Date());
		peca.setDescricao("Peca para maiores");
		peca.setGenero("Drama");
		peca.setNome("Trabalho de Programacao: Um monologo!");
		
		//Verifica se a peca nao eh nula
		Assert.assertNotNull(peca);
		//Verifica se a peca foi adicionada aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(peca));
		//Verifica se a peca foi adicionada aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(peca));
		
		System.out.println(peca.toString());
	}
	
	/**
	 * Metodo responsavel por criar um cliente
	 */
	private void criaCliente(){
		//Metodo que cria um novo endereco
		Endereco endereco = this.novoEndereco1();
		//Verifica se o endereco nao eh nulo
		Assert.assertNotNull(endereco);
		
		//Metodo que cria um novo cliente
		cliente = new Cliente(endereco);
		cliente.setCpf("12121212121");
		cliente.setDataNascimento(new Date());
		cliente.setEmail("cliente1@gmail.com");
		cliente.setNome("Cliente 1");
		cliente.setSenha("abc");
		cliente.setTelefone("22334455");
		cliente.setUser("cliente1");
		
		//Verifica se o cliente nao eh nulo
		Assert.assertNotNull(cliente);
		//Verifica se o endereco do cliente eh igual ao endereco
		Assert.assertEquals(cliente.getEndereco(), endereco);
		
		System.out.println(cliente.toString());
	}
	
	/**
	 * Metodo responsavel por criar uma compra de 1 ingresso para um cliente
	 */
	private void compraIngresso1(){
		Ingresso ingressoFilme = new Ingresso(filme, filme.getAmbiente().getSetores().get(0));
		ingressoFilme.setCadeiraNumerada(1);
		System.out.println(ingressoFilme.toString());
		
		Compra compra = new Compra(cliente, ingressoFilme);
		compra.setSituacao(TipoCompraEnum.RESERVADO);
		//Verifica se o cliente eh igual
		Assert.assertEquals(compra.getCliente(), cliente);
		//Verifica se o cliente possui a compra
		Assert.assertTrue(cliente.getCompras().contains(compra));
		
		//Verifica se existe o ingressoFilme na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoFilme));
		//Verifica se o ingressoFilme possui a compra
		Assert.assertEquals(ingressoFilme.getCompra(), compra);
		
		System.out.println(compra.toString());
	}
	
	/**
	 * Metodo responsavel por criar uma compra de 3 ingressos para um cliente
	 */
	private void compraIngresso2(){
		Ingresso ingressoFilme = new Ingresso(filme, filme.getAmbiente().getSetores().get(0));
		ingressoFilme.setCadeiraNumerada(2);
		System.out.println(ingressoFilme.toString());
		
		Ingresso ingressoPeca = new Ingresso(peca, peca.getAmbiente().getSetores().get(0));
		ingressoPeca.setCadeiraNumerada(1);
		System.out.println(ingressoPeca.toString());
		
		Ingresso ingressoShow = new Ingresso(show, show.getAmbiente().getSetores().get(0));
		ingressoShow.setCadeiraNumerada(1);
		System.out.println(ingressoShow.toString());
		
		List<Ingresso> lista = new ArrayList<Ingresso>();
		lista.add(ingressoFilme);
		lista.add(ingressoShow);
		lista.add(ingressoPeca);
		Compra compra = new Compra(cliente, lista);
		compra.setSituacao(TipoCompraEnum.VENDIDO);
		
		//Verifica se o cliente eh igual
		Assert.assertEquals(compra.getCliente(), cliente);
		//Verifica se o cliente possui a compra
		Assert.assertTrue(cliente.getCompras().contains(compra));
		
		//Verifica se existe o ingressoFilme na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoFilme));
		//Verifica se existe o ingressoShow na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoShow));
		//Verifica se existe o ingressoPeca na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoPeca));
		
		//Verifica se o ingressoFilme possui a compra
		Assert.assertEquals(ingressoFilme.getCompra(), compra);
		//Verifica se o ingressoShow possui a compra
		Assert.assertEquals(ingressoShow.getCompra(), compra);
		//Verifica se o ingressoPeca possui a compra
		Assert.assertEquals(ingressoPeca.getCompra(), compra);

		System.out.println(compra.toString());
	}
	
	/**
	 * Metodo para criar um novo artista
	 * @return
	 */
	private Artista novoArtista(){
		Artista artista = new Artista();
		artista.setNome("Mc Donald");
		return artista;
	}
	
	/**
	 * Metodo para criar um novo diretor
	 * @return
	 */
	private Diretor novoDiretor(){
		Diretor diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");
		return diretor;
	}
	
	/**
	 * Metodo para criar um novo endereco
	 * @return
	 */
	private Endereco novoEndereco1(){
		Endereco endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		return endereco;
	}
	
	/**
	 * Metodo para criar no novo endereco
	 * @return
	 */
	private Endereco novoEndereco2(){
		Endereco endereco = new Endereco();
		endereco.setBairro("Cabral");
		endereco.setCidade("Curitiba");
		endereco.setEstado("Parana");
		endereco.setNumero(340);
		endereco.setPais("Brasil");
		endereco.setRua("Alberto Ferreira");
		return endereco;
	}
	
	/**
	 * Metodo para criar no novo endereco
	 * @return
	 */
	private Endereco novoEndereco3(){
		Endereco endereco = new Endereco();
		endereco.setBairro("Batel");
		endereco.setCidade("Curitiba");
		endereco.setEstado("Parana");
		endereco.setNumero(345);
		endereco.setPais("Brasil");
		endereco.setRua("Rua do Batel");
		return endereco;
	}
	
	/**
	 * Cria um novo setor A
	 * @param ambiente
	 * @return
	 */
	private Setor novoSetorA(Ambiente ambiente){
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(25);
		setor.setNome("Setor A");
		setor.setPreco(250.00);
		return setor;
	}
	
	/**
	 * Cria um novo setor B
	 * @param ambiente
	 * @return
	 */
	private Setor novoSetorB(Ambiente ambiente){
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(100);
		setor.setNome("Setor B");
		setor.setPreco(100.00);
		return setor;
	}
	
	/**
	 * Cria um novo setor C
	 * @param ambiente
	 * @return
	 */
	private Setor novoSetorC(Ambiente ambiente){
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		return setor;
	}
}
