package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * Essa classe testa se nao ha erros na hora de criar um artista
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3Artista {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Artista artista;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarArtista(){
		artista = new Artista();
		artista.setNome("Artista");
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarArtistaNulo(){
		Artista artista = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(artista);
		ArtistaBC.getInstance().create(artista);
	}
	
	/**
	 * Valida o artista preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarArtistaEspacosBranco(){
		artista.setNome("                              ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(artista);		
		ArtistaBC.getInstance().create(artista);
	}
	
	/**
	 * Valida o artista sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarArtistaSemNome(){
		artista.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(artista);		
		ArtistaBC.getInstance().create(artista);
	}
	
	/**
	 * Metodo responsavel por criar um artista completo
	 */
	@Test
	public void criarArtistaCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(artista);		
		ArtistaBC.getInstance().create(artista);
	}
}
