package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CasaShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma casa de shows
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3CasaShow {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected static Endereco endereco;
	protected CasaShow casaShow;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarAmbiente(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");		
		
		casaShow = new CasaShow(endereco);
		casaShow.setDescricao("CasaShow CTBA");
		casaShow.setNome("CasaShow CTBA");
		
		Setor setor = new Setor(casaShow);
		setor.setCapacidade(25);
		setor.setNome("Setor A");
		setor.setPreco(250.00);
		
		setor = new Setor(casaShow);
		setor.setCapacidade(100);
		setor.setNome("Setor B");
		setor.setPreco(100.00);
		
		setor = new Setor(casaShow);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida a casaShow nula
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowNula(){
		CasaShow casaShow = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Valida a casaShow com endereco nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowEnderecoNula(){
		casaShow.setEndereco(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Valida a casaShow sem setor
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowSetorNula(){
		casaShow.setSetores(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Valida a casaShow preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowEspacosBranco(){
		//Metodo que cria um novo casaShow
		CasaShow casaShow = new CasaShow(endereco);
		casaShow.setNome("                              ");
		casaShow.setDescricao("                              ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Valida a casaShow sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowSemNome(){
		casaShow.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Valida a casaShow sem descricao
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowSemDescricao(){
		casaShow.setDescricao(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Valida a casaShow sem capacidade
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowSemCapacidade(){
		Setor setor = new Setor(casaShow);
		setor.setCapacidade(0);
		List<Setor> list = new ArrayList<Setor>();
		list.add(setor);
		casaShow.setSetores(list);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
			
	/**
	 * Valida a casaShow com capacidade negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCasaShowComCapacidadeNegativa(){
		Setor setor = new Setor(casaShow);
		setor.setCapacidade(-200);
		List<Setor> list = new ArrayList<Setor>();
		list.add(setor);
		casaShow.setSetores(list);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
	
	/**
	 * Metodo responsavel por criar uma casaShow completa
	 */
	@Test
	public void criarCasaShowCompleto(){
		//Verifica se o endereco do casaShow eh igual ao endereco
		Assert.assertEquals(casaShow.getEndereco(), endereco);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		CasaShowBC.getInstance().create(casaShow);
	}
}