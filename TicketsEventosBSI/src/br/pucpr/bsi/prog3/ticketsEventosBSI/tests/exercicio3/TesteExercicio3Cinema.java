package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CinemaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um cinema
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3Cinema {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected static Endereco endereco;
	protected Cinema cinema;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarAmbiente(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
		cinema = new Cinema(endereco);
		cinema.setDescricao("Cinema CTBA");
		cinema.setNome("Cinema CTBA");
		
		Setor setor = new Setor(cinema);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o cinema nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaNulo(){
		Cinema cinema = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Valida o cinema com endereco nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaEnderecoNulo(){
		cinema.setEndereco(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Valida o cinema sem setor
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaSetorNulo(){
		cinema.setSetores(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Valida o cinema preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaEspacosBranco(){
		//Metodo que cria um novo cinema
		Cinema cinema = new Cinema(endereco);
		cinema.setNome("                              ");
		cinema.setDescricao("                              ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Valida o cinema sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaSemNome(){
		cinema.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Valida o cinema sem descricao
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaSemDescricao(){
		cinema.setDescricao(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Valida o cinema sem capacidade
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaSemCapacidade(){
		Setor setor = new Setor(cinema);
		setor.setCapacidade(0);
		List<Setor> list = new ArrayList<Setor>();
		list.add(setor);
		cinema.setSetores(list);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
			
	/**
	 * Valida o cinema com capacidade negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCinemaComCapacidadeNegativa(){
		Setor setor = new Setor(cinema);
		setor.setCapacidade(-200);
		List<Setor> list = new ArrayList<Setor>();
		list.add(setor);
		cinema.setSetores(list);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
	
	/**
	 * Metodo responsavel por criar um cinema completo
	 */
	@Test
	public void criarCinemaCompleto(){
		//Verifica se o endereco do cinema eh igual ao endereco
		Assert.assertEquals(cinema.getEndereco(), endereco);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		CinemaBC.getInstance().create(cinema);
	}
}