package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * Essa classe testa se nao ha erros na hora de criar um diretor
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3Diretor {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Diretor diretor;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarArtista(){
		diretor = new Diretor();
		diretor.setNome("Diretor");
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o diretor nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarDiretorNulo(){
		Diretor diretor = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(diretor);
		DiretorBC.getInstance().create(diretor);
	}
	
	/**
	 * Valida o diretor preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarDiretorEspacosBranco(){
		diretor.setNome("                              ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(diretor);		
		DiretorBC.getInstance().create(diretor);
	}
	
	/**
	 * Valida o diretor sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarDiretorSemNome(){
		diretor.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(diretor);		
		DiretorBC.getInstance().create(diretor);
	}
	
	/**
	 * Metodo responsavel por criar um diretor completo
	 */
	@Test
	public void criarDiretorCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(diretor);		
		DiretorBC.getInstance().create(diretor);
	}
}
