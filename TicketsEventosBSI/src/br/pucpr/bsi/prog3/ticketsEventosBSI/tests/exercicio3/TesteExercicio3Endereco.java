package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um endereco
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3Endereco {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected Endereco endereco;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarEndereco(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
	}
	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida o endereco nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoNulo(){
		Endereco endereco = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco preenchido com espaços em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoEspacosBranco(){
		Endereco endereco = new Endereco();
		endereco.setBairro("                         ");
		endereco.setCidade("                         ");
		endereco.setComplemento("                    ");
		endereco.setEstado("                         ");
		endereco.setNumero(0);
		endereco.setPais("                           ");
		endereco.setRua("                            ");

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}	
	
	/**
	 * Valida o endereco sem o nome da rua
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemRua(){
		endereco.setRua(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco sem o numero
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemNumero(){
		endereco.setNumero(0);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco com o numero negativo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoComNumeroNegativo(){
		endereco.setNumero(-200);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco sem complemento
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemComplemento(){
		endereco.setComplemento(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco sem bairro
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemBairro(){
		endereco.setBairro(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco sem cidade
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemCidade(){
		endereco.setCidade(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco sem estado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemEstado(){
		endereco.setEstado(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco sem pais
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarEnderecoSemPais(){
		endereco.setPais(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o endereco completo
	 */
	@Test
	public void criarEnderecoCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		EnderecoBC.getInstance().create(endereco);
	}
}
