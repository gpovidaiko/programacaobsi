package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um setor
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3Setor {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected static Endereco endereco;
	protected Setor setor;
	protected Ambiente ambiente;
	
	//////////////////////////////////////////
	// METODO DE EXECUCAO ANTES DO INICIO DOS TESTES
	//////////////////////////////////////////		
	
	/**
	 * Metodo para criar um novo endereco
	 * @return
	 */
	@BeforeClass
	public static void criarAmbiente(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
	}	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarSetor(){
		ambiente = new Cinema(endereco);
		ambiente.setNome("Cinema Setor");
		ambiente.setDescricao("Ambiente para o Teste Setor");
		
		setor = new Setor(ambiente);
		setor.setCapacidade(25);
		setor.setNome("Setor A");
		setor.setPreco(250.00);
	}
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida o Setor nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorNulo(){
		Setor endereco = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		SetorBC.getInstance().create(endereco);
	}
	
	/**
	 * Valida o setor preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorEspacosBranco(){
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(0);
		setor.setNome("                         ");
		setor.setPreco(0);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}	
	
	/**
	 * Valida o setor sem o nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorSemNome(){
		setor.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
	
	/**
	 * Valida o setor sem a capacidade
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorSemNumero(){
		setor.setCapacidade(0);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
	
	/**
	 * Valida o setor com a capacidade negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorComNumeroNegativo(){
		setor.setCapacidade(-200);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
	
	/**
	 * Valida o setor sem o preco
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorSemPreco(){
		setor.setPreco(0);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
	
	/**
	 * Valida o setor com o preco negativo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorComPrecoNegativo(){
		setor.setPreco(-200);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
	
	/**
	 * Valida o setor com ambiente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarSetorAmbienteNulo(){
		setor.setAmbiente(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
	
	/**
	 * Valida o setor completo
	 */
	@Test
	public void criarSetorCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		SetorBC.getInstance().create(setor);
	}
}
