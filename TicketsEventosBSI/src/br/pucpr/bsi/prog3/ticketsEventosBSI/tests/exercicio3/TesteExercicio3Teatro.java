package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.TeatroBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um teatro
 * 
 * @author Mauda
 *
 */

public class TesteExercicio3Teatro {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected static Endereco endereco;
	protected Teatro teatro;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarAmbiente(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");		
		
		teatro = new Teatro(endereco);
		teatro.setDescricao("Teatro CTBA");
		teatro.setNome("Teatro CTBA");
		
		Setor setor = new Setor(teatro);
		setor.setCapacidade(100);
		setor.setNome("Setor B");
		setor.setPreco(100.00);
		
		setor = new Setor(teatro);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o teatro nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroNulo(){
		Teatro teatro = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Valida o teatro com endereco nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroEnderecoNulo(){
		teatro.setEndereco(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Valida o teatro sem setor
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroSetorNulo(){
		teatro.setSetores(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Valida o teatro preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroEspacosBranco(){
		//Metodo que cria um novo teatro
		Teatro teatro = new Teatro(endereco);
		teatro.setNome("                              ");
		teatro.setDescricao("                              ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Valida o teatro sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroSemNome(){
		teatro.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Valida o teatro sem descricao
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroSemDescricao(){
		teatro.setDescricao(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Valida o teatro sem capacidade
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroSemCapacidade(){
		Setor setor = new Setor(teatro);
		setor.setCapacidade(0);
		List<Setor> list = new ArrayList<Setor>();
		list.add(setor);
		teatro.setSetores(list);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
			
	/**
	 * Valida o teatro com capacidade negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarTeatroComCapacidadeNegativa(){
		Setor setor = new Setor(teatro);
		setor.setCapacidade(-200);
		List<Setor> list = new ArrayList<Setor>();
		list.add(setor);
		teatro.setSetores(list);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
	
	/**
	 * Metodo responsavel por criar um teatro completo
	 */
	@Test
	public void criarTeatroCompleto(){
		//Verifica se o endereco do teatro eh igual ao endereco
		Assert.assertEquals(teatro.getEndereco(), endereco);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		TeatroBC.getInstance().create(teatro);
	}
}