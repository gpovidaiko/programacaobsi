package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio milestone final da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFArtista extends TesteExercicio3Artista {
	
	/**
	 * Metodo responsavel por criar um artista completo
	 */
	@Test
	@Override
	public void criarArtistaCompleto(){
		// INSERT
		PrintUtils.imprimeNomeMetodoChamadorEClasse(artista);		
		long idEndereco = ArtistaBC.getInstance().create(artista);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, artista.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(artista);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaArtista(ArtistaBC.getInstance().retrieve(idEndereco), artista);
	}
	
	/**
	 * Metodo responsavel por atualizar um artista completo
	 */
	@Test
	public void updateArtistaCompleto(){
		//Realiza um insert do filme para iniciar a verificacao do update
		criarArtistaCompleto();
		
		//Atualiza filme
		Util.atualizaArtista(artista, "UPDATE FI - ");
		
		//Atualiza o BD
		ArtistaBC.getInstance().update(artista);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaArtista(ArtistaBC.getInstance().retrieve(artista.getId()), artista);
	}
	
	/**
	 * Metodo responsavel por deletar um artista completo
	 */
	@Test
	public void deletarArtistaCompleto(){
		//Realiza um insert do filme para iniciar a verificacao do update
		criarArtistaCompleto();

		ArtistaBC.getInstance().delete(artista);
		Assert.assertNull(ArtistaBC.getInstance().retrieve(artista.getId()));
	}
}
