package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CasaShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma casa de shows
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFCasaShow extends TesteExercicio3CasaShow{
	
	/**
	 * Metodo responsavel por criar uma casaShow completa
	 */
	@Test
	@Override
	public void criarCasaShowCompleto(){
		
		////////////////////////
		// INSERT
		////////////////////////
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		long idBD = CasaShowBC.getInstance().create(casaShow);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idBD > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idBD, casaShow.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaAmbiente(CasaShowBC.getInstance().retrieve(idBD), casaShow);
	}
	
	/**
	 * Metodo responsavel por atualizar uma casaShow completa
	 */
	@Test
	public void updateCasaShowCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		long idBD = CasaShowBC.getInstance().create(casaShow);
		
		//Atualiza casaShow
		Util.atualizaAmbiente(casaShow, "Update CS - ");
		
		//Atualiza o BD
		CasaShowBC.getInstance().update(casaShow);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaAmbiente(CasaShowBC.getInstance().retrieve(idBD), casaShow);
	}
	
	/**
	 * Metodo responsavel por deletar uma casaShow completa
	 */
	@Test
	public void deletarCasaShowCompleto(){
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);		
		long idBD = CasaShowBC.getInstance().create(casaShow);

		//Imprime para verificar se o objeto foi atualizado
		PrintUtils.imprimeNomeMetodoChamadorEClasse(casaShow);
		
		CasaShowBC.getInstance().delete(casaShow);
		Assert.assertNull(CasaShowBC.getInstance().retrieve(idBD));
	}
	
	
}