package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CinemaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma casa de shows
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFCinema extends TesteExercicio3Cinema{
	
	/**
	 * Metodo responsavel por criar um cinema completo
	 */
	@Test
	@Override
	public void criarCinemaCompleto(){
		
		////////////////////////
		// INSERT
		////////////////////////
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		long idEndereco = CinemaBC.getInstance().create(cinema);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, cinema.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaAmbiente(CinemaBC.getInstance().retrieve(idEndereco), cinema);
	}
	
	/**
	 * Metodo responsavel por atualizar um cinema completo
	 */
	@Test
	public void updateCinemaCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		long idEndereco = CinemaBC.getInstance().create(cinema);
		
		//Atualiza cinema
		Util.atualizaAmbiente(cinema, "Update CN - ");
		
		//Atualiza o BD
		CinemaBC.getInstance().update(cinema);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaAmbiente(CinemaBC.getInstance().retrieve(idEndereco), cinema);
	}
	
	/**
	 * Metodo responsavel por deletar um cinema completo
	 */
	@Test
	public void deletarCinemaCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);		
		long idEndereco = CinemaBC.getInstance().create(cinema);

		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cinema);
		
		CinemaBC.getInstance().delete(cinema);
		Assert.assertNull(CinemaBC.getInstance().retrieve(idEndereco));
	}
}