package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma casa de shows
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFCliente extends TesteExercicioM1Cliente{
	
	/**
	 * Metodo responsavel por criar uma cliente completa
	 */
	@Test
	@Override
	public void criarClienteCompleto(){
		
		////////////////////////
		// INSERT
		////////////////////////
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		long idEndereco = ClienteBC.getInstance().create(cliente);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, cliente.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaUsuario(ClienteBC.getInstance().retrieve(idEndereco), cliente);
	}
	
	/**
	 * Metodo responsavel por atualizar uma cliente completa
	 */
	@Test
	public void updateClienteCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		long idEndereco = ClienteBC.getInstance().create(cliente);
		
		//Atualiza cliente
		Util.atualizaUsuario(cliente, "UpdCL-");
		
		//Atualiza o BD
		ClienteBC.getInstance().update(cliente);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaUsuario(ClienteBC.getInstance().retrieve(idEndereco), cliente);
	}
	
	/**
	 * Metodo responsavel por deletar uma cliente completa
	 */
	@Test
	public void deletarClienteCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		long idEndereco = ClienteBC.getInstance().create(cliente);

		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);
		
		ClienteBC.getInstance().delete(cliente);
		Assert.assertNull(ClienteBC.getInstance().retrieve(idEndereco));
	}
}