package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CompraBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.IngressoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Compra1Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio milestone final da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFCompra1Ingresso extends TesteExercicioM1Compra1Ingresso {
	
	/**
	 * Metodo responsavel por criar um compra completo
	 */
	@SuppressWarnings("unchecked")
	@Test
	@Override
	public void criarCompraCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(ambiente).create(ambiente);		
		
		//Eh necessario inserir um diretor primeiro para nao ferir a integridade do banco
		DiretorBC.getInstance().create(diretor);
		
		//Eh necessario inserir os artistas primeiro para nao ferir a integridade do banco
		ArtistaBC.getInstance().create(artista);
		
		//Eh necessario inserir o evento primeiro para nao ferir a integridade do banco
		ShowBC.getInstance().create(evento);
		
		//Eh necessario inserir o cliente primeiro para nao ferir a integridade do banco
		ClienteBC.getInstance().create(cliente);
		
		// INSERT COMPRA
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		long idCompra = CompraBC.getInstance().create(compra);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idCompra > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idCompra, compra.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);
		
		
		//INSERT INGRESSO
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		long idIngresso = IngressoBC.getInstance().create(ingresso);
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(idIngresso > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idIngresso, ingresso.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);
		
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaCompra(CompraBC.getInstance().retrieve(idCompra), compra);
	}
	
	/**
	 * Metodo responsavel por atualizar um compra completo
	 */
	@Test
	public void updateCompraCompleta(){
		//Realiza um insert da compra para iniciar a verificacao do update
		criarCompraCompleto();
		
		//Atualiza Ingresso
		Util.atualizaIngresso(ingresso, "UPDATE IN - ");
		
		//Atualiza Ingresso
		Util.atualizaCompra(compra, "UPDATE CO - ");
		
		//Atualiza o BD
		IngressoBC.getInstance().update(ingresso);
		CompraBC.getInstance().update(compra);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaCompra(CompraBC.getInstance().retrieve(compra.getId()), compra);
	}
	
	/**
	 * Metodo responsavel por deletar um compra completo
	 */
	@Test
	public void deletarDiretorCompleto(){
		//Realiza um insert da compra para iniciar a verificacao do update
		criarCompraCompleto();

		IngressoBC.getInstance().delete(ingresso);
		Assert.assertNull(IngressoBC.getInstance().retrieve(ingresso.getId()));
		
		CompraBC.getInstance().delete(compra);
		Assert.assertNull(CompraBC.getInstance().retrieve(compra.getId()));
	}
}
