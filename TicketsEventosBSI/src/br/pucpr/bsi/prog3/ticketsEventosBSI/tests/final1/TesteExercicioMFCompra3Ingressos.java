package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CompraBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EventoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.IngressoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Compra3Ingressos;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio milestone final da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFCompra3Ingressos extends TesteExercicioM1Compra3Ingressos {
	
	/**
	 * Metodo responsavel por criar um compra completo
	 */
	@SuppressWarnings("unchecked")
	@Test
	@Override
	public void criarCompraCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(casaShow).create(casaShow);		
		
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(cinema).create(cinema);		
		
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(teatro).create(teatro);		
		
		//Eh necessario inserir um diretor primeiro para nao ferir a integridade do banco
		DiretorBC.getInstance().create(diretor);
		
		//Eh necessario inserir os artistas primeiro para nao ferir a integridade do banco
		ArtistaBC.getInstance().create(artista);
		
		//Eh necessario inserir o evento primeiro para nao ferir a integridade do banco
		EventoBC.getInstance(filme).create(filme);
		
		//Eh necessario inserir o evento primeiro para nao ferir a integridade do banco
		EventoBC.getInstance(peca).create(peca);
		
		//Eh necessario inserir o evento primeiro para nao ferir a integridade do banco
		EventoBC.getInstance(show).create(show);
		
		//Eh necessario inserir o cliente primeiro para nao ferir a integridade do banco
		ClienteBC.getInstance().create(cliente);
		
		// INSERT COMPRA
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		long idCompra = CompraBC.getInstance().create(compra);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idCompra > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idCompra, compra.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);
		
		
		//INSERT INGRESSO FILME
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingressoFilme);		
		long idIngressoFilme = IngressoBC.getInstance().create(ingressoFilme);
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(idIngressoFilme > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idIngressoFilme, ingressoFilme.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingressoFilme);
		
		
		//INSERT INGRESSO PECA
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingressoPeca);		
		long idIngressoPeca = IngressoBC.getInstance().create(ingressoPeca);
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(idIngressoPeca > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idIngressoPeca, ingressoPeca.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingressoPeca);
		
		
		//INSERT INGRESSO SHOW
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingressoShow);		
		long idIngressoShow = IngressoBC.getInstance().create(ingressoShow);
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(idIngressoShow > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idIngressoShow, ingressoShow.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingressoShow);
		
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaCompra(CompraBC.getInstance().retrieve(idCompra), compra);
	}
	
	/**
	 * Metodo responsavel por atualizar um compra completo
	 */
	@Test
	public void updateCompraCompleta(){
		//Realiza um insert da compra para iniciar a verificacao do update
		criarCompraCompleto();
		
		//Atualiza Ingresso Filme
		Util.atualizaIngresso(ingressoFilme, "UPDATE IN - ");
		
		//Atualiza Ingresso Peca
		Util.atualizaIngresso(ingressoPeca, "UPDATE IN - ");
		
		//Atualiza Ingresso Show
		Util.atualizaIngresso(ingressoShow, "UPDATE IN - ");

		
		//Atualiza Ingresso
		Util.atualizaCompra(compra, "UPDATE CO - ");
		
		//Atualiza o BD
		IngressoBC.getInstance().update(ingressoFilme);
		IngressoBC.getInstance().update(ingressoPeca);
		IngressoBC.getInstance().update(ingressoShow);
		CompraBC.getInstance().update(compra);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaCompra(CompraBC.getInstance().retrieve(compra.getId()), compra);
	}
	
	/**
	 * Metodo responsavel por deletar um compra completo
	 */
	@Test
	public void deletarDiretorCompleto(){
		//Realiza um insert da compra para iniciar a verificacao do update
		criarCompraCompleto();

		IngressoBC.getInstance().delete(ingressoFilme);
		Assert.assertNull(IngressoBC.getInstance().retrieve(ingressoFilme.getId()));
		
		IngressoBC.getInstance().delete(ingressoPeca);
		Assert.assertNull(IngressoBC.getInstance().retrieve(ingressoPeca.getId()));

		IngressoBC.getInstance().delete(ingressoShow);
		Assert.assertNull(IngressoBC.getInstance().retrieve(ingressoShow.getId()));

		CompraBC.getInstance().delete(compra);
		Assert.assertNull(CompraBC.getInstance().retrieve(compra.getId()));
	}
}
