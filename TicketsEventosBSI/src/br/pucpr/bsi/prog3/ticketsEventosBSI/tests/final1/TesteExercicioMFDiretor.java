package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio milestone final da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFDiretor extends TesteExercicio3Diretor {
	
	/**
	 * Metodo responsavel por criar um diretor completo
	 */
	@Test
	@Override
	public void criarDiretorCompleto(){
		// INSERT
		PrintUtils.imprimeNomeMetodoChamadorEClasse(diretor);		
		long idEndereco = DiretorBC.getInstance().create(diretor);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, diretor.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(diretor);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaDiretor(DiretorBC.getInstance().retrieve(idEndereco), diretor);
	}
	
	/**
	 * Metodo responsavel por atualizar um diretor completo
	 */
	@Test
	public void updateDiretorCompleto(){
		//Realiza um insert do filme para iniciar a verificacao do update
		criarDiretorCompleto();
		
		//Atualiza filme
		Util.atualizaDiretor(diretor, "UPDATE FI - ");
		
		//Atualiza o BD
		DiretorBC.getInstance().update(diretor);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaDiretor(DiretorBC.getInstance().retrieve(diretor.getId()), diretor);
	}
	
	/**
	 * Metodo responsavel por deletar um diretor completo
	 */
	@Test
	public void deletarDiretorCompleto(){
		//Realiza um insert do filme para iniciar a verificacao do update
		criarDiretorCompleto();

		DiretorBC.getInstance().delete(diretor);
		Assert.assertNull(DiretorBC.getInstance().retrieve(diretor.getId()));
	}
}
