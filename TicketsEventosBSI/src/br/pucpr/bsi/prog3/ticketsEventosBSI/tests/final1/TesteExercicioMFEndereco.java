package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.EnderecoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para a apresentacao final da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um endereco
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFEndereco extends TesteExercicio3Endereco {
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida o endereco completo
	 */
	@Test
	@Override
	public void criarEnderecoCompleto(){
		
		////////////////////////
		// INSERT
		////////////////////////
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		long idEndereco = EnderecoBC.getInstance().create(endereco);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, endereco.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaEndereco(EnderecoBC.getInstance().retrieve(idEndereco), endereco);		
	}
	
	/**
	 * Valida o endereco completo
	 */
	@Test
	public void updateEnderecoCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		long idEndereco = EnderecoBC.getInstance().create(endereco);
		
		//Atualiza objeto
		Util.atualizaEndereco(endereco, "Update EN - ");
		
		//Atualiza o BD
		EnderecoBC.getInstance().update(endereco);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaEndereco(EnderecoBC.getInstance().retrieve(idEndereco), endereco);		
	}
	
	/**
	 * Valida o endereco completo
	 */
	@Test
	public void deleteEnderecoCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);		
		long idEndereco = EnderecoBC.getInstance().create(endereco);

		//Imprime para verificar se o endereco foi atualizado
		PrintUtils.imprimeNomeMetodoChamadorEClasse(endereco);
		
		//Deleta o endereco do BD
		EnderecoBC.getInstance().delete(endereco);
		Assert.assertNull(EnderecoBC.getInstance().retrieve(idEndereco));
	}
			
	
	
}
