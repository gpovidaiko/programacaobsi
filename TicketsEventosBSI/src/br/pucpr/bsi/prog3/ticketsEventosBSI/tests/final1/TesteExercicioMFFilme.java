package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.FilmeBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFFilme extends TesteExercicioM1Filme {
	
	/**
	 * Metodo responsavel por criar um filme completo
	 */
	@SuppressWarnings("unchecked")
	@Test
	@Override
	public void criarFilmeCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(ambiente).create(ambiente);		
		
		//Eh necessario inserir um diretor primeiro para nao ferir a integridade do banco
		DiretorBC.getInstance().create(diretor);
		
		//Eh necessario inserir os artistas primeiro para nao ferir a integridade do banco
		ArtistaBC.getInstance().create(artista);
		
		// INSERT
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		long idEndereco = FilmeBC.getInstance().create(filme);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, filme.getId());
		
		//Verifica se o ambiente do filme eh igual ao cinema
		Assert.assertEquals(filme.getAmbiente(), ambiente);
		//Verifica se o filme foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(filme));
		//Verifica se o filme foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(filme));
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaFilme(FilmeBC.getInstance().retrieve(idEndereco), filme);
	}
	
	/**
	 * Metodo responsavel por atualizar um filme completo
	 */
	@Test
	public void updateFilmeCompleto(){
		//Realiza um insert do filme para iniciar a verificacao do update
		criarFilmeCompleto();
		
		//Atualiza filme
		Util.atualizaFilme(filme, "UPDATE FI - ");
		
		//Atualiza o BD
		FilmeBC.getInstance().update(filme);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaFilme(FilmeBC.getInstance().retrieve(filme.getId()), filme);
	}
	
	/**
	 * Metodo responsavel por deletar um filme completo
	 */
	@Test
	public void deletarFilmeCompleto(){
		//Realiza um insert do filme para iniciar a verificacao do update
		criarFilmeCompleto();

		FilmeBC.getInstance().delete(filme);
		Assert.assertNull(FilmeBC.getInstance().retrieve(filme.getId()));
	}
}
