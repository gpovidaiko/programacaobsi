package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.FuncionarioBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Funcionario;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma casa de shows
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFFuncionario extends TesteExercicioM1Funcionario{
	
	/**
	 * Metodo responsavel por criar uma funcionario completa
	 */
	@Test
	@Override
	public void criarFuncionarioCompleto(){
		
		////////////////////////
		// INSERT
		////////////////////////
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		long idEndereco = FuncionarioBC.getInstance().create(funcionario);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, funcionario.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaUsuario(FuncionarioBC.getInstance().retrieve(idEndereco), funcionario);
	}
	
	/**
	 * Metodo responsavel por atualizar uma funcionario completa
	 */
	@Test
	public void updateFuncionarioCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		long idEndereco = FuncionarioBC.getInstance().create(funcionario);
		
		//Atualiza funcionario
		Util.atualizaUsuario(funcionario, "UpdFU-");
		
		//Atualiza o BD
		FuncionarioBC.getInstance().update(funcionario);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaUsuario(FuncionarioBC.getInstance().retrieve(idEndereco), funcionario);
	}
	
	/**
	 * Metodo responsavel por deletar uma funcionario completa
	 */
	@Test
	public void deletarFuncionarioCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		long idEndereco = FuncionarioBC.getInstance().create(funcionario);

		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);
		
		FuncionarioBC.getInstance().delete(funcionario);
		Assert.assertNull(FuncionarioBC.getInstance().retrieve(idEndereco));
	}
}