package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.PecaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFPeca extends TesteExercicioM1Peca {
	
	/**
	 * Metodo responsavel por criar um peca completo
	 */
	@SuppressWarnings("unchecked")
	@Test
	@Override
	public void criarPecaCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(ambiente).create(ambiente);		
		
		//Eh necessario inserir um diretor primeiro para nao ferir a integridade do banco
		DiretorBC.getInstance().create(diretor);
		
		//Eh necessario inserir os artistas primeiro para nao ferir a integridade do banco
		ArtistaBC.getInstance().create(artista);
		
		// INSERT
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		long idEndereco = PecaBC.getInstance().create(peca);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, peca.getId());
		
		//Verifica se o ambiente do peca eh igual ao cinema
		Assert.assertEquals(peca.getAmbiente(), ambiente);
		//Verifica se o peca foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(peca));
		//Verifica se o peca foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(peca));
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaPeca(PecaBC.getInstance().retrieve(idEndereco), peca);
	}
	
	/**
	 * Metodo responsavel por atualizar um peca completo
	 */
	@Test
	public void updatePecaCompleto(){
		//Realiza um insert do peca para iniciar a verificacao do update
		criarPecaCompleto();
		
		//Atualiza peca
		Util.atualizaPeca(peca, "UPDATE FI - ");
		
		//Atualiza o BD
		PecaBC.getInstance().update(peca);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaPeca(PecaBC.getInstance().retrieve(peca.getId()), peca);
	}
	
	/**
	 * Metodo responsavel por deletar um peca completo
	 */
	@Test
	public void deletarPecaCompleto(){
		//Realiza um insert do peca para iniciar a verificacao do update
		criarPecaCompleto();

		PecaBC.getInstance().delete(peca);
		Assert.assertNull(PecaBC.getInstance().retrieve(peca.getId()));
	}
}
