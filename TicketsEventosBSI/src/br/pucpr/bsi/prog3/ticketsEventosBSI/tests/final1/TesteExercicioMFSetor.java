package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.SetorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.dao.CinemaDAO;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um setor
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFSetor extends TesteExercicio3Setor {
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida o setor completo
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Test
	public void criarSetorCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(ambiente).create(ambiente);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		long idBD = SetorBC.getInstance().create(setor);
	
		//Verifica se o id eh maior que zero
		Assert.assertTrue(idBD > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idBD, setor.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaSetor(SetorBC.getInstance().retrieve(idBD), setor);		
	}
	
	/**
	 * Atualiza um setor completo
	 */
	@Test
	public void updateSetorCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		CinemaDAO.getInstance().create((Cinema)ambiente);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		long idBD = SetorBC.getInstance().create(setor);
			
		//Atualiza objeto
		Util.atualizaSetor(setor, "UPDATE SE - ");
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);

		//Atualiza o BD
		SetorBC.getInstance().update(setor);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaSetor(SetorBC.getInstance().retrieve(idBD), setor);
	}
	
	/**
	 * Atualiza um setor completo
	 */
	@Test
	public void deleteSetorCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		CinemaDAO.getInstance().create((Cinema)ambiente);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);		
		long idBD = SetorBC.getInstance().create(setor);
		
		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(setor);
		
		//Delete o setor do BD
		SetorBC.getInstance().delete(setor);
		Assert.assertNull(SetorBC.getInstance().retrieve(idBD));
		
	}
}
