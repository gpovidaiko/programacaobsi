package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1.TesteExercicioM1Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFShow extends TesteExercicioM1Show {
	
	/**
	 * Metodo responsavel por criar um show completo
	 */
	@SuppressWarnings("unchecked")
	@Test
	@Override
	public void criarShowCompleto(){
		//Eh necessario inserir um ambiente primeiro para nao ferir a integridade do banco
		AmbienteBC.getInstance(ambiente).create(ambiente);		
		
		//Eh necessario inserir um diretor primeiro para nao ferir a integridade do banco
		DiretorBC.getInstance().create(diretor);
		
		//Eh necessario inserir os artistas primeiro para nao ferir a integridade do banco
		ArtistaBC.getInstance().create(artista);
		
		// INSERT
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		long idEndereco = ShowBC.getInstance().create(show);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, show.getId());
		
		//Verifica se o ambiente do show eh igual ao cinema
		Assert.assertEquals(show.getAmbiente(), ambiente);
		//Verifica se o show foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(show));
		//Verifica se o show foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(show));
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaShow(ShowBC.getInstance().retrieve(idEndereco), show);
	}
	
	/**
	 * Metodo responsavel por atualizar um show completo
	 */
	@Test
	public void updateShowCompleto(){
		//Realiza um insert do show para iniciar a verificacao do update
		criarShowCompleto();
		
		//Atualiza show
		Util.atualizaShow(show, "UPDATE FI - ");
		
		//Atualiza o BD
		ShowBC.getInstance().update(show);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaShow(ShowBC.getInstance().retrieve(show.getId()), show);
	}
	
	/**
	 * Metodo responsavel por deletar um show completo
	 */
	@Test
	public void deletarShowCompleto(){
		//Realiza um insert do show para iniciar a verificacao do update
		criarShowCompleto();

		ShowBC.getInstance().delete(show);
		Assert.assertNull(ShowBC.getInstance().retrieve(show.getId()));
	}
}
