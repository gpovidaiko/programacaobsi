package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.TeatroBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.tests.exercicio3.TesteExercicio3Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma casa de shows
 * 
 * @author Mauda
 *
 */

public class TesteExercicioMFTeatro extends TesteExercicio3Teatro{
	
	/**
	 * Metodo responsavel por criar um teatro completo
	 */
	@Test
	@Override
	public void criarTeatroCompleto(){
		
		////////////////////////
		// INSERT
		////////////////////////
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		long idEndereco = TeatroBC.getInstance().create(teatro);

		//Verifica se o id eh maior que zero
		Assert.assertTrue(idEndereco > 0);
		//Verifica se os Ids sao iguais
		Assert.assertEquals(idEndereco, teatro.getId());
		
		//Imprime para verificar se o id foi inserido dentro do objeto endereco
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaAmbiente(TeatroBC.getInstance().retrieve(idEndereco), teatro);
	}
	
	/**
	 * Metodo responsavel por atualizar um teatro completo
	 */
	@Test
	public void updateTeatroCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		long idEndereco = TeatroBC.getInstance().create(teatro);
		
		//Atualiza teatro
		Util.atualizaAmbiente(teatro, "Update TE - ");
		
		//Atualiza o BD
		TeatroBC.getInstance().update(teatro);
		
		//Obtem o objeto do BD para as comparacoes basicas
		Util.verificaAmbiente(TeatroBC.getInstance().retrieve(idEndereco), teatro);
	}
	
	/**
	 * Metodo responsavel por deletar um teatro completo
	 */
	@Test
	public void deletarTeatroCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);		
		long idEndereco = TeatroBC.getInstance().create(teatro);

		//Imprime para verificar se o id foi inserido dentro do objeto
		PrintUtils.imprimeNomeMetodoChamadorEClasse(teatro);
		
		TeatroBC.getInstance().delete(teatro);
		Assert.assertNull(TeatroBC.getInstance().retrieve(idEndereco));
	}
}