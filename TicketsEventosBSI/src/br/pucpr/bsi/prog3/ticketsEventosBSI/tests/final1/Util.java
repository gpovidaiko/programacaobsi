package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.final1;

import java.util.Date;
import java.util.List;

import org.junit.Assert;

import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Evento;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Usuario;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.DateUtils;

/**
 * Classe responsavel por atualizar informacoes para update e comparacoes
 * @author Mauda
 *
 */
public class Util {
	
	///////////////////////////////////////////////////////
	// METODOS DE ATUALIZACAO
	///////////////////////////////////////////////////////
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param ambiente
	 * @param cod
	 */
	protected static void atualizaAmbiente(Ambiente ambiente, String cod){
		//Atualiza o ambiente
		ambiente.setDescricao(cod + ambiente.getDescricao());
		ambiente.setNome(cod + ambiente.getNome());
		
		//Atualiza o endereco
		atualizaEndereco(ambiente.getEndereco(), cod);
		
		//Atualiza os setores
		for (Setor setor : ambiente.getSetores()) {
			atualizaSetor(setor, cod);
		}
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param artista
	 * @param cod
	 */
	protected static void atualizaArtista(Artista artista, String cod){
		artista.setNome(cod + artista.getNome());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param endereco
	 * @param cod
	 */
	protected static void atualizaCompra(Compra endereco, String cod){
		//Atualiza o endereco
		endereco.setSituacao(TipoCompraEnum.VENDIDO);;
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param diretor
	 * @param cod
	 */
	protected static void atualizaDiretor(Diretor diretor, String cod){
		diretor.setNome(cod + diretor.getNome());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param endereco
	 * @param cod
	 */
	protected static void atualizaEndereco(Endereco endereco, String cod){
		//Atualiza o endereco
		endereco.setBairro(cod + endereco.getBairro());
		endereco.setCidade(cod + endereco.getCidade());
		endereco.setComplemento(cod + endereco.getComplemento());
		endereco.setEstado(cod + endereco.getEstado());
		endereco.setNumero(100000);
		endereco.setPais(cod + endereco.getPais());
		endereco.setRua(cod + endereco.getRua());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param endereco
	 * @param cod
	 */
	protected static void atualizaIngresso(Ingresso endereco, String cod){
		//Atualiza o endereco
		endereco.setCadeiraNumerada(1000);
	}
	
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param setor
	 * @param cod
	 */
	protected static void atualizaSetor(Setor setor, String cod){ 
		//Atualiza o setor
		setor.setCapacidade(50);
		setor.setNome(cod + setor.getNome());
		setor.setPreco(500.00);
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param usuario
	 * @param cod
	 */
	protected static void atualizaUsuario(Usuario usuario, String cod){
		//Atualiza o ambiente
		usuario.setCpf(				"63874112705");
		usuario.setDataNascimento(	new Date());
		usuario.setEmail(cod 		+ usuario.getEmail());
		usuario.setNome(cod 		+ usuario.getNome());
		usuario.setSenha(cod 		+ usuario.getSenha());
		usuario.setTelefone(cod 	+ usuario.getTelefone());
		usuario.setUser(cod 		+ usuario.getUser());

		//Atualiza o endereco
		atualizaEndereco(usuario.getEndereco(), cod);
		
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param evento
	 * @param cod
	 */
	protected static void atualizaEvento(Evento evento, String cod){
		//Atualiza o evento
		evento.setCensura(80);
		evento.setDataEvento(		(new Date(evento.getDataEvento().getTime()+40000000)));
		evento.setDescricao(cod		+ evento.getDescricao());
		evento.setNome(cod			+ evento.getNome());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param filme
	 * @param cod
	 */
	protected static void atualizaFilme(Filme filme, String cod){
		//Atualiza o evento
		atualizaEvento(filme, cod);
		filme.setGenero(cod	+ filme.getGenero());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param peca
	 * @param cod
	 */
	protected static void atualizaPeca(Peca peca, String cod){
		//Atualiza o evento
		atualizaEvento(peca, cod);
		peca.setGenero(cod	+ peca.getGenero());
		peca.setCompanhia(cod	+ peca.getCompanhia());
	}
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param show
	 * @param cod
	 */
	protected static void atualizaShow(Show show, String cod){
		//Atualiza o evento
		atualizaEvento(show, cod);
		show.setEstilo(cod	+ show.getEstilo());
	}
	
	///////////////////////////////////////////////////////
	// METODOS DE VERIFICACAO
	///////////////////////////////////////////////////////
	
	protected static void verificaAmbiente(Ambiente ambienteBD, Ambiente ambiente){
		//Verifica o ambiente
		Assert.assertEquals(ambienteBD.getDescricao(), 	ambiente.getDescricao());
		Assert.assertEquals(ambienteBD.getId(), 		ambiente.getId());
		Assert.assertEquals(ambienteBD.getNome(),		ambiente.getNome());
		
		//Verifica o endereco
		verificaEndereco(ambienteBD.getEndereco(), 		ambiente.getEndereco());
		
		//Verifica os setores
		List<Setor> setoresBD = ambienteBD.getSetores();
		for (Setor setor : ambiente.getSetores()) {
			verificaSetor(setoresBD.get(setoresBD.indexOf(setor)), setor);
		}
	}
	
	protected static void verificaArtista(Artista artistaBD, Artista artista){
		Assert.assertEquals(artistaBD.getId(), 			artista.getId());
		Assert.assertEquals(artistaBD.getNome(), 		artista.getNome());
	}
	
	protected static void verificaCompra(Compra compraBD, Compra compra){
		Assert.assertEquals(compraBD.getId(), 			compra.getId());
		Assert.assertEquals(compraBD.getSituacao(), 	compra.getSituacao());
		
		verificaUsuario(compraBD.getCliente(), compra.getCliente());
		
		List<Ingresso> ingressosBD = compraBD.getIngressos();
		for (Ingresso ingresso : compra.getIngressos()) {
			verificaIngresso(ingressosBD.get(ingressosBD.indexOf(ingresso)), ingresso);
		}
	}

	protected static void verificaDiretor(Diretor diretorBD, Diretor diretor){
		Assert.assertEquals(diretorBD.getId(), 			diretor.getId());
		Assert.assertEquals(diretorBD.getNome(), 		diretor.getNome());
	}
	
	protected static void verificaEndereco(Endereco enderecoBD, Endereco endereco){
		Assert.assertEquals(enderecoBD.getBairro(), 		endereco.getBairro());
		Assert.assertEquals(enderecoBD.getCidade(), 		endereco.getCidade());
		Assert.assertEquals(enderecoBD.getComplemento(), 	endereco.getComplemento());
		Assert.assertEquals(enderecoBD.getEstado(), 		endereco.getEstado());
		Assert.assertEquals(enderecoBD.getId(), 			endereco.getId());
		Assert.assertEquals(enderecoBD.getNumero(),			endereco.getNumero());
		Assert.assertEquals(enderecoBD.getPais(), 			endereco.getPais());
		Assert.assertEquals(enderecoBD.getRua(), 			endereco.getRua());
	}
	
	protected static void verificaEvento(Evento eventoBD, Evento evento){
		//Verifica o evento
		Assert.assertEquals(eventoBD.getCensura(), 					evento.getCensura());
		Assert.assertEquals(eventoBD.getDataEvento(), 		 		DateUtils.minimizeDate(evento.getDataEvento()));
		Assert.assertEquals(eventoBD.getDataInclusao(),				DateUtils.minimizeDate(evento.getDataInclusao()));
		Assert.assertEquals(eventoBD.getDescricao(), 				evento.getDescricao());
		Assert.assertEquals(eventoBD.getId(), 						evento.getId());
		Assert.assertEquals(eventoBD.getNome(), 					evento.getNome());
		
		//Verifica Diretor
		verificaDiretor(eventoBD.getDiretor(), evento.getDiretor());
		
		//verifica os Artistas
		List<Artista> artistasBD = eventoBD.getArtistas();
		for (Artista artista : evento.getArtistas()) {
			verificaArtista(artistasBD.get(artistasBD.indexOf(artista)), artista);
		}
		
		//Verifica o ambiente
		verificaAmbiente(eventoBD.getAmbiente(), evento.getAmbiente());
	}
	
	protected static void verificaFilme(Filme filmeBD, Filme filme){
		verificaEvento(filmeBD, filme);
		Assert.assertEquals(filmeBD.getGenero(), 		filme.getGenero());
	}
	
	protected static void verificaIngresso(Ingresso ingressoBD, Ingresso ingresso){
		Assert.assertEquals(ingressoBD.getId(), 					ingresso.getId());
		Assert.assertEquals(ingressoBD.getCadeiraNumerada(), 		ingresso.getCadeiraNumerada());
		
		verificaEvento(ingressoBD.getEvento(), ingresso.getEvento());
		
		verificaSetor(ingressoBD.getSetor(), ingresso.getSetor());
	}
	
	
	protected static void verificaPeca(Peca pecaBD, Peca peca){
		verificaEvento(pecaBD, peca);
		Assert.assertEquals(pecaBD.getGenero(), 		peca.getGenero());
		Assert.assertEquals(pecaBD.getCompanhia(), 		peca.getCompanhia());
	}

	protected static void verificaSetor(Setor setorBD, Setor setor){
		Assert.assertEquals(setorBD.getCapacidade(), 	setor.getCapacidade());
		Assert.assertEquals(setorBD.getId(), 			setor.getId());
		Assert.assertEquals(setorBD.getNome(), 			setor.getNome());
		Assert.assertEquals(setorBD.getPreco(), 		setor.getPreco(), 0);
	}
	
	protected static void verificaShow(Show showBD, Show show){
		verificaEvento(showBD, show);
		Assert.assertEquals(showBD.getEstilo(), 		show.getEstilo());
	}

	protected static void verificaUsuario(Usuario usuarioBD, Usuario usuario){
		//Verifica o Usuario
		Assert.assertEquals(usuarioBD.getCpf(), 			usuario.getCpf());
		Assert.assertEquals(usuarioBD.getDataNascimento(), 	DateUtils.minimizeDate(usuario.getDataNascimento()));
		Assert.assertEquals(usuarioBD.getEmail(), 			usuario.getEmail());
		Assert.assertEquals(usuarioBD.getId(), 				usuario.getId());
		Assert.assertEquals(usuarioBD.getNome(),			usuario.getNome());
		Assert.assertEquals(usuarioBD.getSenha(), 			usuario.getSenha());
		Assert.assertEquals(usuarioBD.getTelefone(), 		usuario.getTelefone());
		Assert.assertEquals(usuarioBD.getUser(), 			usuario.getUser());
		
		//Verifica o endereco
		verificaEndereco(usuarioBD.getEndereco(), 			usuario.getEndereco());
	}
}
