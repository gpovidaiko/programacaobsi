package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Cliente {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected static Endereco endereco;
	protected Cliente cliente;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarCliente(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");		
		
		cliente = new Cliente(endereco);
		cliente.setNome("Cliente 1");
		cliente.setCpf("12308912388");
		cliente.setEmail("cliente1@gmail.com");
		cliente.setTelefone("22334455");
		cliente.setDataNascimento(new Date());
		cliente.setUser("cliente1");
		cliente.setSenha("abc");
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o cliente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteNulo(){
		Cliente cliente = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente com endereco nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteEnderecoNulo(){
		cliente.setEndereco(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteEspacosBranco(){
		//Metodo que cria um novo cliente
		Cliente cliente = new Cliente(endereco);
		cliente.setNome("                              ");
		cliente.setCpf("                               ");
		cliente.setEmail("                             ");
		cliente.setTelefone("                          ");
		cliente.setUser("                              ");
		cliente.setSenha("                             ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemNome(){
		cliente.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem cpf
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemCpf(){
		cliente.setCpf(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem email
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemEmail(){
		cliente.setEmail(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
			
	/**
	 * Valida o cliente sem @ no email
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteComEmailSemArroba(){
		cliente.setEmail("cliente1ticketeventosbsi.com.br");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem . apos o @ no email
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteComEmailSemPonto(){
		cliente.setEmail("cliente.1@ticketeventosbsi");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem telefone
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemTelefone(){
		cliente.setTelefone(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}	
	
	/**
	 * Valida o cliente sem data nascimento
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemDataNascimento(){
		cliente.setDataNascimento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente com data nascimento após a data atual
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteComDataNascimentoAposDataAtual(){
		cliente.setDataNascimento(new Date(new Date().getTime()+40000000));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem user
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemUser(){
		cliente.setUser(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Valida o cliente sem senha
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarClienteSemSenha(){
		cliente.setSenha(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
	
	/**
	 * Metodo responsavel por criar um cliente completo
	 */
	@Test
	public void criarClienteCompleto(){
		//Verifica se o endereco do cliente eh igual ao endereco
		Assert.assertEquals(cliente.getEndereco(), endereco);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(cliente);		
		ClienteBC.getInstance().create(cliente);
	}
}
