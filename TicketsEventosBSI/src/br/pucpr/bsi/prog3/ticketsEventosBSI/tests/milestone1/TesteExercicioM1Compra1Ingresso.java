package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CompraBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma compra
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Compra1Ingresso {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Artista artista;
	protected CasaShow ambiente;
	protected Diretor diretor;
	protected Endereco endereco;
	protected Show evento;
	protected Ingresso ingresso;
	protected Cliente cliente;
	protected Compra compra;
	
	
	@Before
	public void criarCompra(){
		//Cria endereco
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
		//Cria artista
		artista = new Artista();
		artista.setNome("Mc Donald");

		//cria diretor
		diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");

		//cria casaShow
		ambiente = new CasaShow(endereco);
		ambiente.setDescricao("CasaShow CTBA");
		ambiente.setNome("CasaShow CTBA");
		
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		evento = new Show(ambiente, artista, diretor);
		evento.setCensura(18);
		evento.setDataEvento((new Date(new Date().getTime()+40000000)));
		evento.setDescricao("Show para maiores");
		evento.setEstilo("Rock");
		evento.setId(0);
		evento.setNome("CodeLine: Run for your lives!");
		
		ingresso = new Ingresso(evento, evento.getAmbiente().getSetores().get(0));
		ingresso.setCadeiraNumerada(1);
		
		Endereco endereco1 = new Endereco();
		endereco1.setBairro("Cabral");
		endereco1.setCidade("Curitiba");
		endereco1.setComplemento("Casa 10");
		endereco1.setEstado("Parana");
		endereco1.setNumero(340);
		endereco1.setPais("Brasil");
		endereco1.setRua("Alberto Ferreira");
		
		cliente = new Cliente(endereco1);
		cliente.setCpf("12121212121");
		cliente.setDataNascimento(new Date());
		cliente.setEmail("cliente1@gmail.com");
		cliente.setId(0);
		cliente.setNome("Cliente 1");
		cliente.setSenha("abc");
		cliente.setTelefone("22334455");
		cliente.setUser("cliente1");
	
		compra = new Compra(cliente, ingresso);
		compra.setSituacao(TipoCompraEnum.RESERVADO);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida a compra nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraNulo(){
		Compra compra = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra sem a cadeira numerada
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraSemSituacao(){
		compra.setSituacao(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra com lista ingressos nula
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraListaIngressosNulo(){
		compra.setIngressos(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra com ingresso nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowArtistaNulo(){
		compra.getIngressos().clear();
		compra.getIngressos().add(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra com cliente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraClienteNulo(){
		compra.setCliente(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}	
	
	/**
	 * Valida a compra completa
	 */
	@Test
	public void criarCompraCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
		
		//Verifica se o cliente eh igual
		Assert.assertEquals(compra.getCliente(), cliente);
		//Verifica se o cliente possui a compra
		Assert.assertTrue(cliente.getCompras().contains(compra));
		
		//Verifica se existe o ingressoFilme na compra
		Assert.assertTrue(compra.getIngressos().contains(ingresso));
		//Verifica se o ingressoFilme possui a compra
		Assert.assertEquals(ingresso.getCompra(), compra);
	}
}
