package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CompraBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoCompraEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar uma compra
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Compra3Ingressos {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Endereco endereco1;
	protected Endereco endereco2;
	protected Endereco endereco3;
	protected Endereco endereco4;
	
	protected Artista artista;
	protected Diretor diretor;
	protected Cliente cliente;
	
	protected CasaShow casaShow;
	protected Cinema cinema;
	protected Teatro teatro;
	
	protected Filme filme;
	protected Peca peca;
	protected Show show;	
	
	protected Ingresso ingressoFilme;
	protected Ingresso ingressoPeca;
	protected Ingresso ingressoShow;
	protected Compra compra;
	
	
	//////////////////////////////////////////
	// METODO DE EXECUCAO ANTES DO INICIO DOS TESTES
	//////////////////////////////////////////		
	
	/**
	 * Metodo para criar um novo endereco, novo artista, novo diretor, novo casaShow
	 * @return
	 */
	@Before
	public void criarCompra(){
		//Cria artista
		artista = new Artista();
		artista.setNome("Mc Donald");

		//cria diretor
		diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");
		
		///////////////////////////////////////////
		// CASA SHOW - SHOW
		///////////////////////////////////////////
		endereco1 = new Endereco();
		endereco1.setBairro("Afonso Pena");
		endereco1.setCidade("Sao Jose dos Pinhais");
		endereco1.setComplemento("Casa 10");
		endereco1.setEstado("Parana");
		endereco1.setNumero(200);
		endereco1.setPais("Brasil");
		endereco1.setRua("Afonso Pereira");
		
		casaShow = new CasaShow(endereco1);
		casaShow.setDescricao("CasaShow CTBA");
		casaShow.setNome("CasaShow CTBA");
		
		Setor setor = new Setor(casaShow);
		setor.setCapacidade(25);
		setor.setNome("Setor A");
		setor.setPreco(250.00);
		
		setor = new Setor(casaShow);
		setor.setCapacidade(100);
		setor.setNome("Setor B");
		setor.setPreco(100.00);
		
		setor = new Setor(casaShow);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		show = new Show(casaShow, artista, diretor);
		show.setCensura(18);
		show.setDataEvento((new Date(new Date().getTime()+40000000)));
		show.setDescricao("Show para maiores");
		show.setEstilo("Rock");
		show.setNome("CodeLine: Run for your lives!");
		
		///////////////////////////////////////////
		// CINEMA - FILME
		///////////////////////////////////////////
		endereco2 = new Endereco();
		endereco2.setBairro("Cabral");
		endereco2.setCidade("Curitiba");
		endereco2.setEstado("Parana");
		endereco2.setComplemento("Casa");
		endereco2.setNumero(340);
		endereco2.setPais("Brasil");
		endereco2.setRua("Alberto Ferreira");
		
		cinema = new Cinema(endereco2);
		cinema.setDescricao("Cinema CTBA");
		cinema.setNome("Cinema CTBA");
		
		setor = new Setor(cinema);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		filme = new Filme(cinema, artista, diretor);
		filme.setCensura(18);
		filme.setDataEvento((new Date(new Date().getTime()+40000000)));
		filme.setDescricao("Filme para maiores");
		filme.setGenero("Acao");
		filme.setNome("CodeLine: Run for your lives!");
		
		///////////////////////////////////////////
		// TEATRO - PECA
		///////////////////////////////////////////
		endereco3 = new Endereco();
		endereco3.setBairro("Batel");
		endereco3.setCidade("Curitiba");
		endereco3.setEstado("Parana");
		endereco3.setComplemento("Casa");
		endereco3.setNumero(345);
		endereco3.setPais("Brasil");
		endereco3.setRua("Rua do Batel");
		
		//cria teatro
		teatro = new Teatro(endereco3);
		teatro.setDescricao("Teatro CTBA");
		teatro.setNome("Teatro CTBA");
		
		setor = new Setor(teatro);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		setor = new Setor(teatro);
		setor.setCapacidade(100);
		setor.setNome("Setor B");
		setor.setPreco(100.00);
		
		peca = new Peca(teatro, artista, diretor);
		peca.setCensura(18);
		peca.setCompanhia("BSI Pecas");
		peca.setDataEvento((new Date(new Date().getTime()+40000000)));
		peca.setDescricao("Peca para maiores");
		peca.setGenero("Acao");
		peca.setNome("CodeLine: Run for your lives!");
		
		endereco4 = new Endereco();
		endereco4.setBairro("Afonso Pena");
		endereco4.setCidade("Sao Jose dos Pinhais");
		endereco4.setComplemento("Casa 10");
		endereco4.setEstado("Parana");
		endereco4.setNumero(200);
		endereco4.setPais("Brasil");
		endereco4.setRua("Afonso Pereira");
		
		cliente = new Cliente(endereco4);
		cliente.setCpf("12121212121");
		cliente.setDataNascimento(new Date());
		cliente.setEmail("cliente1@gmail.com");
		cliente.setNome("Cliente 1");
		cliente.setSenha("abc");
		cliente.setTelefone("22334455");
		cliente.setUser("cliente1");
	
		ingressoFilme = new Ingresso(filme, filme.getAmbiente().getSetores().get(0));
		ingressoFilme.setCadeiraNumerada(1);
		
		ingressoPeca = new Ingresso(peca, peca.getAmbiente().getSetores().get(0));
		ingressoPeca.setCadeiraNumerada(1);
		
		ingressoShow = new Ingresso(show, show.getAmbiente().getSetores().get(0));
		ingressoShow.setCadeiraNumerada(1);
		
		List<Ingresso> lista = new ArrayList<Ingresso>();
		lista.add(ingressoFilme);
		lista.add(ingressoShow);
		lista.add(ingressoPeca);
		compra = new Compra(cliente, lista);
		compra.setSituacao(TipoCompraEnum.RESERVADO);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida a compra nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraNulo(){
		Compra compra = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra sem a cadeira numerada
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraSemSituacao(){
		compra.setSituacao(null);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra com lista ingressos nula
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraListaIngressosNulo(){
		compra.setIngressos(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra com ingresso nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowArtistaNulo(){
		compra.getIngressos().clear();
		compra.getIngressos().add(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}
	
	/**
	 * Valida a compra com cliente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarCompraClienteNulo(){
		compra.setCliente(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
	}	
	
	/**
	 * Valida a compra completa
	 */
	@Test
	public void criarCompraCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(compra);		
		CompraBC.getInstance().create(compra);
		
		//Verifica se o cliente eh igual
		Assert.assertEquals(compra.getCliente(), cliente);
		//Verifica se o cliente possui a compra
		Assert.assertTrue(cliente.getCompras().contains(compra));
		
		//Verifica se existe o ingressoFilme na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoFilme));
		//Verifica se existe o ingressoShow na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoShow));
		//Verifica se existe o ingressoPeca na compra
		Assert.assertTrue(compra.getIngressos().contains(ingressoPeca));
		
		//Verifica se o ingressoFilme possui a compra
		Assert.assertEquals(ingressoFilme.getCompra(), compra);
		//Verifica se o ingressoShow possui a compra
		Assert.assertEquals(ingressoShow.getCompra(), compra);
		//Verifica se o ingressoPeca possui a compra
		Assert.assertEquals(ingressoPeca.getCompra(), compra);		
	}
}
