package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.FilmeBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Filme;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Filme {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Artista artista;
	protected Ambiente ambiente;
	protected Diretor diretor;
	protected Endereco endereco;
	protected Filme filme;
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarFilme(){
		//Cria endereco
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
		//Cria artista
		artista = new Artista();
		artista.setNome("Mc Donald");

		//cria diretor
		diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");

		//cria cinema
		ambiente = new Cinema(endereco);
		ambiente.setDescricao("Cinema CTBA");
		ambiente.setNome("Cinema CTBA");
		
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		filme = new Filme(ambiente, artista, diretor);
		filme.setCensura(18);
		filme.setDataEvento((new Date(new Date().getTime()+40000000)));
		filme.setDescricao("Filme para maiores");
		filme.setGenero("Acao");
		filme.setId(0);
		filme.setNome("CodeLine: Run for your lives!");
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o filme nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeNulo(){
		Filme filme = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme com ambiente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeAmbienteNulo(){
		filme.setAmbiente(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme com lista artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeListaArtistaNulo(){
		filme.setArtistas(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme com artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeArtistaNulo(){
		filme.getArtistas().clear();
		filme.getArtistas().add(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	
	/**
	 * Valida o filme com diretor nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeDiretorNulo(){
		filme.setDiretor(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeEspacosBranco(){
		//Metodo que cria um novo filme
		Filme filme = new Filme(ambiente, artista, diretor);
		filme.setNome("                              ");
		filme.setGenero("                            ");
		filme.setDescricao("                         ");
		filme.setDataEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeSemNome(){
		filme.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme sem descricao
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeSemDescricao(){
		filme.setDescricao(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme sem censura
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeSemCensura(){
		filme.setCensura(0);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme com censura negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeComCensuraNegativa(){
		filme.setCensura(-20);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme sem data evento
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeSemDataEvento(){
		filme.setDataEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme com data evento antes da data atual
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeComDataEventoAntesDataAtual(){
		filme.setDataEvento((new Date(new Date().getTime()-40000000)));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Valida o filme sem genero
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFilmeSemGenero(){
		filme.setGenero(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
	
	/**
	 * Metodo responsavel por criar um filme completo
	 */
	@Test
	public void criarFilmeCompleto(){
		//Verifica se o ambiente do filme eh igual ao cinema
		Assert.assertEquals(filme.getAmbiente(), ambiente);
		//Verifica se o filme foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(filme));
		//Verifica se o filme foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(filme));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(filme);		
		FilmeBC.getInstance().create(filme);
	}
}
