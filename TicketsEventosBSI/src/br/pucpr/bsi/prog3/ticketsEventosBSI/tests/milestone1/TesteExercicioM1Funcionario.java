package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.FuncionarioBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um funcionario
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Funcionario {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected static Endereco endereco;
	protected Funcionario funcionario;
	
	//////////////////////////////////////////
	// METODO DE EXECUCAO ANTES DO INICIO DOS TESTES
	//////////////////////////////////////////		
	
	/**
	 * Metodo para criar um novo endereco
	 * @return
	 */
	@BeforeClass
	public static void criarEndereco(){
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
	}	
	
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarFuncionario(){
		funcionario = new Funcionario(endereco);
		funcionario.setNome("Funcionario 1");
		funcionario.setCpf("12308912388");
		funcionario.setEmail("func1@ticketeventosbsi.com.br");
		funcionario.setTelefone("22334455");
		funcionario.setDataNascimento(new Date());
		funcionario.setUser("func1");
		funcionario.setSenha("abc");
	}	
	
	
	//////////////////////////////////////////
	// METODO DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o funcionario nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioNulo(){
		Funcionario funcionario = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario com endereco nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioEnderecoNulo(){
		funcionario.setEndereco(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioEspacosBranco(){
		//Metodo que cria um novo funcionario
		Funcionario funcionario = new Funcionario(endereco);
		funcionario.setNome("                              ");
		funcionario.setCpf("                               ");
		funcionario.setEmail("                             ");
		funcionario.setTelefone("                          ");
		funcionario.setUser("                              ");
		funcionario.setSenha("                             ");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemNome(){
		funcionario.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem cpf
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemCpf(){
		funcionario.setCpf(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem email
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemEmail(){
		funcionario.setEmail(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
			
	/**
	 * Valida o funcionario sem @ no email
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioComEmailSemArroba(){
		funcionario.setEmail("func1ticketeventosbsi.com.br");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem . apos o @ no email
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioComEmailSemPonto(){
		funcionario.setEmail("func.1@ticketeventosbsi");
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem telefone
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemTelefone(){
		funcionario.setTelefone(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}	
	
	/**
	 * Valida o funcionario sem data nascimento
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemDataNascimento(){
		funcionario.setDataNascimento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario com data nascimento após a data atual
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioComDataNascimentoAposDataAtual(){
		funcionario.setDataNascimento(new Date(new Date().getTime()+40000000));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem user
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemUser(){
		funcionario.setUser(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Valida o funcionario sem senha
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarFuncionarioSemSenha(){
		funcionario.setSenha(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
	
	/**
	 * Metodo responsavel por criar um funcionario completo
	 */
	@Test
	public void criarFuncionarioCompleto(){
		//Verifica se o endereco do funcionario eh igual ao endereco
		Assert.assertEquals(funcionario.getEndereco(), endereco);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(funcionario);		
		FuncionarioBC.getInstance().create(funcionario);
	}
}