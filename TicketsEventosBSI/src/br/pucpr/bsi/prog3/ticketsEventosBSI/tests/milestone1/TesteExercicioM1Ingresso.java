package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.IngressoBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Compra;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ingresso;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * Essa classe testa se nao ha erros na hora de criar um ingresso
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Ingresso {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	private static Artista artista;
	private static CasaShow casaShow;
	private static Diretor diretor;
	private static Endereco endereco;
	private static Show show;
	private Ingresso ingresso;
	
	//////////////////////////////////////////
	// METODO DE EXECUCAO ANTES DO INICIO DOS TESTES
	//////////////////////////////////////////		
	
	/**
	 * Metodo para criar um novo endereco, novo artista, novo diretor, novo casaShow
	 * @return
	 */
	@BeforeClass
	public static void criarItens(){
		//Cria endereco
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
		//Cria artista
		artista = new Artista();
		artista.setNome("Mc Donald");

		//cria diretor
		diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");

		//cria casaShow
		casaShow = new CasaShow(endereco);
		casaShow.setDescricao("CasaShow CTBA");
		casaShow.setNome("CasaShow CTBA");
		
		Setor setor = new Setor(casaShow);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		show = new Show(casaShow, artista, diretor);
		show.setCensura(18);
		show.setDataEvento((new Date(new Date().getTime()+40000000)));
		show.setDescricao("Show para maiores");
		show.setEstilo("Rock");
		show.setId(0);
		show.setNome("CodeLine: Run for your lives!");
		
	}
	
	//////////////////////////////////////////
	// METODOS AUXILIARES
	//////////////////////////////////////////
	
	@Before
	public void criarIngresso(){
		ingresso = new Ingresso(show, show.getAmbiente().getSetores().get(0));
		ingresso.setCadeiraNumerada(1);
		new Compra(new Cliente(endereco), ingresso);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida o ingresso nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarIngressoNulo(){
		Ingresso ingresso = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}
	
	/**
	 * Valida o ingresso sem a cadeira numerada
	 * Como aqui numero eh um tipo primitivo sera setado o 0, indicando que
	 * este nao foi modificado
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarIngressoSemCadeiraNumerada(){
		ingresso.setCadeiraNumerada(0);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}
	
	/**
	 * Valida o ingresso com o numero da cadeira numerada negativo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarIngressoComCadeiraNumeradaNegativa(){
		ingresso.setCadeiraNumerada(-200);

		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}
	
	/**
	 * Valida o ingresso com setor nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarIngressoSetorNulo(){
		ingresso.setSetor(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}	
	
	/**
	 * Valida o ingresso com evento nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarIngressoEventoNulo(){
		ingresso.setEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}
	
	/**
	 * Valida o ingresso com compra nula
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarIngressoCompraNula(){
		ingresso.setCompra(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}
	
	/**
	 * Valida o ingresso completo
	 */
	@Test
	public void criarIngressoCompleto(){
		PrintUtils.imprimeNomeMetodoChamadorEClasse(ingresso);		
		IngressoBC.getInstance().create(ingresso);
	}
}
