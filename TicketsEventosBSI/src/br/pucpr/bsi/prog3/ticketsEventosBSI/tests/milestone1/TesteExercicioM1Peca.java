package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.PecaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Peca;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio de milestone da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Peca {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Artista artista;
	protected Teatro ambiente;
	protected Diretor diretor;
	protected Endereco endereco;
	protected Peca peca;
	
	//////////////////////////////////////////
	// METODO DE EXECUCAO ANTES DO INICIO DOS TESTES
	//////////////////////////////////////////		
	
	/**
	 * Metodo para criar um novo endereco, novo artista, novo diretor, novo teatro
	 * @return
	 */
	@Before
	public void criarPeca(){
		//Cria endereco
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
		//Cria artista
		artista = new Artista();
		artista.setNome("Mc Donald");

		//cria diretor
		diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");

		//cria teatro
		ambiente = new Teatro(endereco);
		ambiente.setDescricao("Teatro CTBA");
		ambiente.setNome("Teatro CTBA");
		
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
		
		setor = new Setor(ambiente);
		setor.setCapacidade(100);
		setor.setNome("Setor B");
		setor.setPreco(100.00);

		peca = new Peca(ambiente, artista, diretor);
		peca.setCensura(18);
		peca.setCompanhia("BSI Pecas");
		peca.setDataEvento((new Date(new Date().getTime()+40000000)));
		peca.setDescricao("Peca para maiores");
		peca.setGenero("Acao");
		peca.setId(0);
		peca.setNome("CodeLine: Run for your lives!");
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o peca nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaNulo(){
		Peca peca = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca com ambiente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaAmbienteNulo(){
		peca.setAmbiente(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca com lista artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaListaArtistaNulo(){
		peca.setArtistas(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca com artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaArtistaNulo(){
		peca.getArtistas().clear();
		peca.getArtistas().add(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	
	/**
	 * Valida o peca com diretor nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaDiretorNulo(){
		peca.setDiretor(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaEspacosBranco(){
		//Metodo que cria um novo peca
		Peca peca = new Peca(ambiente, artista, diretor);
		peca.setNome("                              ");
		peca.setGenero("                            ");
		peca.setDescricao("                         ");
		peca.setDataEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaSemNome(){
		peca.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca sem descricao
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaSemDescricao(){
		peca.setDescricao(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca sem censura
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaSemCensura(){
		peca.setCensura(0);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca com censura negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaComCensuraNegativa(){
		peca.setCensura(-20);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca sem data evento
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaSemDataEvento(){
		peca.setDataEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca com data evento antes da data atual
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaComDataEventoAntesDataAtual(){
		peca.setDataEvento((new Date(new Date().getTime()-40000000)));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca sem companhia
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaSemCompanhia(){
		peca.setCompanhia(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Valida o peca sem genero
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarPecaSemGenero(){
		peca.setGenero(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
	
	/**
	 * Metodo responsavel por criar um peca completo
	 */
	@Test
	public void criarPecaCompleto(){
		//Verifica se o ambiente do peca eh igual ao teatro
		Assert.assertEquals(peca.getAmbiente(), ambiente);
		//Verifica se o peca foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(peca));
		//Verifica se o peca foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(peca));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(peca);		
		PecaBC.getInstance().create(peca);
	}
}
