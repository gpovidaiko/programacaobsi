package br.pucpr.bsi.prog3.ticketsEventosBSI.tests.milestone1;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Show;
import br.pucpr.bsi.prog3.ticketsEventosBSI.utils.PrintUtils;

/**
 * Essa classe eh um teste JUnit para o laboratorio 3 da disciplina
 * de programacao 3 do curso de BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteExercicioM1Show {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////		
	
	protected Artista artista;
	protected CasaShow ambiente;
	protected Diretor diretor;
	protected Endereco endereco;
	protected Show show;
	
	//////////////////////////////////////////
	// METODO DE EXECUCAO ANTES DO INICIO DOS TESTES
	//////////////////////////////////////////		
	
	/**
	 * Metodo para criar um novo endereco, novo artista, novo diretor, novo casaShow
	 * @return
	 */
	@Before
	public void criarShow(){
		//Cria endereco
		endereco = new Endereco();
		endereco.setBairro("Afonso Pena");
		endereco.setCidade("Sao Jose dos Pinhais");
		endereco.setComplemento("Casa 10");
		endereco.setEstado("Parana");
		endereco.setId(0);
		endereco.setNumero(200);
		endereco.setPais("Brasil");
		endereco.setRua("Afonso Pereira");
		
		//Cria artista
		artista = new Artista();
		artista.setNome("Mc Donald");

		//cria diretor
		diretor = new Diretor();
		diretor.setNome("Fernando Meirelles");

		//cria casaShow
		ambiente = new CasaShow(endereco);
		ambiente.setDescricao("CasaShow CTBA");
		ambiente.setNome("CasaShow CTBA");
		
		Setor setor = new Setor(ambiente);
		setor.setCapacidade(500);
		setor.setNome("Setor C");
		setor.setPreco(50.00);
	
		show = new Show(ambiente, artista, diretor);
		show.setCensura(18);
		show.setDataEvento((new Date(new Date().getTime()+40000000)));
		show.setDescricao("Show para maiores");
		show.setEstilo("Rock");
		show.setId(0);
		show.setNome("CodeLine: Run for your lives!");
	}

	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////	
	
	/**
	 * Valida o show nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowNulo(){
		Show show = null;
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show com ambiente nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowAmbienteNulo(){
		show.setAmbiente(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show com lista artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowListaArtistaNulo(){
		show.setArtistas(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show com artista nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowArtistaNulo(){
		show.getArtistas().clear();
		show.getArtistas().add(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	
	/**
	 * Valida o show com diretor nulo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowDiretorNulo(){
		show.setDiretor(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show preenchido com espacos em branco
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowEspacosBranco(){
		//Metodo que cria um novo show
		Show show = new Show(ambiente, artista, diretor);
		show.setNome("                              ");
		show.setEstilo("                            ");
		show.setDescricao("                         ");
		show.setDataEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show sem nome
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowSemNome(){
		show.setNome(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show sem descricao
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowSemDescricao(){
		show.setDescricao(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show sem censura
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowSemCensura(){
		show.setCensura(0);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show com censura negativa
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowComCensuraNegativa(){
		show.setCensura(-20);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show sem data evento
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowSemDataEvento(){
		show.setDataEvento(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show com data evento antes da data atual
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowComDataEventoAntesDataAtual(){
		show.setDataEvento((new Date(new Date().getTime()-40000000)));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Valida o show sem estilo
	 */
	@Test(expected = TicketsEventosBSIException.class)
	public void validarShowSemEstilo(){
		show.setEstilo(null);
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
	
	/**
	 * Metodo responsavel por criar um show completo
	 */
	@Test
	public void criarShowCompleto(){
		//Verifica se o ambiente do show eh igual ao casaShow
		Assert.assertEquals(show.getAmbiente(), ambiente);
		//Verifica se o show foi adicionado aos eventos do artista
		Assert.assertTrue(artista.getEventos().contains(show));
		//Verifica se o show foi adicionado aos eventos do diretor
		Assert.assertTrue(diretor.getEventos().contains(show));
		
		PrintUtils.imprimeNomeMetodoChamadorEClasse(show);		
		ShowBC.getInstance().create(show);
	}
}
