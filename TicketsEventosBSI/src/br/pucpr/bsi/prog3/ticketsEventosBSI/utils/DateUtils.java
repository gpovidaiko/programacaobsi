package br.pucpr.bsi.prog3.ticketsEventosBSI.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Classe Utilitaria para tratamento de datas
 * @author Mauda
 *
 */

public class DateUtils {
	
	public static Date minimizeDate(Date _date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);

		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	public static Date maxmizeDate(Date _date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(_date);

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		// o milesecundos é 998, porque o banco de dados faz confusão quando passa 999 e passa para o dia seguinte.
		// não é um erro de implementação, mas um erro do java e do banco de dados.
		calendar.set(Calendar.MILLISECOND, 998);

		return calendar.getTime();
	}
}
