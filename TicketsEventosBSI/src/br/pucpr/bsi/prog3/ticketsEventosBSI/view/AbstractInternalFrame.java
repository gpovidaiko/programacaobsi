package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

/**
 * Frame que representa uma JInternalFrame, mas com alguns metodos proprios
 * para ajudar na criacao das telas internas
 * @author mauda
 *
 */
public abstract class AbstractInternalFrame extends JInternalFrame {
	
	private static final long serialVersionUID = 4953145512859044129L;
	
	protected JPanel painelFields;
	protected JPanel painelButtons;
	
	/**
	 * Construtor para respeitar o construtor da classe pai
	 * @param title
	 * @param resizable
	 * @param closable
	 * @param maximizable
	 * @param iconifiable
	 */
	public AbstractInternalFrame(String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable){
		super(title, resizable, closable, maximizable, iconifiable);
		initializePanels();
		initializeFields();
		initializeButtons();
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * Metodo que inicializa os paineis para os campos e botoes
	 */
	protected void initializePanels() {
		painelFields= new JPanel();
		painelFields.setLayout(new GridBagLayout());
		painelFields.setBorder(BorderFactory.createTitledBorder("Campos"));
		
		painelButtons = new JPanel();
		painelButtons.setLayout(new FlowLayout());
		painelButtons.setBorder(BorderFactory.createTitledBorder("Acoes"));

		getContentPane().setLayout(new BorderLayout(5, 5));
		getContentPane().add(painelFields, BorderLayout.CENTER);
		getContentPane().add(painelButtons, BorderLayout.SOUTH);
	}
	
	/**
	 * Metodo abstrato para inicializar os campos de uma tela
	 */
	protected abstract void initializeFields();
	
	/**
	 * Metodo abstrato para inicializar as acoes de uma tela
	 */
	protected abstract void initializeButtons();
	
}
