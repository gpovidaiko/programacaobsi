package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyVetoException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.hsqldb.lib.ArrayUtil;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.AmbienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CasaShowBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.CinemaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.FuncionarioBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.TeatroBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.enums.TipoAmbienteEnum;
import br.pucpr.bsi.prog3.ticketsEventosBSI.exception.TicketsEventosBSIException;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.CasaShow;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cinema;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Funcionario;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Teatro;

/**
 * Classe que representa a tela de cadastro de um funcion�rio
 * 
 * @author Gledson Povidaiko
 *
 */
public class AmbienteView extends AbstractInternalFrame implements PossuiEndereco, PossuiSetor {

	private static final long serialVersionUID = -5545042975331013776L;

	private JDesktopPane desktop;
	private Endereco endereco;
	private Ambiente ambiente;
	private SetorTableModel setorTableModel;
	private List<Setor> setores;
	private Setor setorSelecionado;
	private int indexSetorSelecionado;
	private JLabel enderecoLB;
	private JComboBox<Object> ambienteCB;
	private JTextField nomeTF;
	private JTextField descricaoTF;
	private JTable jTable;

	private JPanel painelSetoresFields;
	private JPanel painelSetoresButtons;
	
	
	public AmbienteView(JDesktopPane desktop) {
		super("Funcion�rio", true, true, true, true);
		setSize(500, 500);
		setLocation(10, 10);
		setVisible(true);
		this.desktop = desktop;
	}

	@Override
	protected void initializeFields() {
		GridBagConstraints c = new GridBagConstraints();
		
		//*****************************
		// Label Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel ambienteLB = new JLabel("Ambiente:");
		painelFields.add(ambienteLB, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel nomeLB = new JLabel("Nome:");
		painelFields.add(nomeLB, c);
		
		c.gridx = 0;
		c.gridy = 4;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel cpfLB = new JLabel("Descri��o:");
		painelFields.add(cpfLB, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 6;
		c.anchor = GridBagConstraints.NORTHWEST;
		enderecoLB = new JLabel("Endere�o:");
		painelFields.add(enderecoLB, c);
		
		//*****************************
		// TextField Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		List<Object> itemsCB = new ArrayList<Object>();
		itemsCB.add("Selecione");
		itemsCB.addAll(Arrays.asList(TipoAmbienteEnum.values()));
		ambienteCB = new JComboBox<Object>(itemsCB.toArray());
		painelFields.add(ambienteCB, c);

		ambienteCB.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if (e.getItem().getClass().equals(TipoAmbienteEnum.class)) {
						if (((TipoAmbienteEnum) e.getItem()).getTipo() == 'c') {
							AmbienteView.this.ambiente = new Cinema(AmbienteView.this.endereco);
						} else if (((TipoAmbienteEnum) e.getItem()).getTipo() == 's') {
							AmbienteView.this.ambiente = new CasaShow(AmbienteView.this.endereco);
						} else if (((TipoAmbienteEnum) e.getItem()).getTipo() == 't') {
							AmbienteView.this.ambiente = new Teatro(AmbienteView.this.endereco);
						}
					} else {
						AmbienteView.this.ambiente = null;
					}
				}
			}
		});
		
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		nomeTF = new JTextField(20);
		painelFields.add(nomeTF, c);
		
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 5;
		c.anchor = GridBagConstraints.CENTER;
		descricaoTF = new JTextField(20);
		painelFields.add(descricaoTF, c);
		
		this.initializePainelSetores();
	}
	
	@Override
	protected void initializeButtons() {
		ActionListener enderecoBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				EnderecoView enderecoView = new EnderecoView(AmbienteView.this);
				desktop.add(enderecoView);
				try {
					enderecoView.setSelected(true);
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		};
		
		JButton enderecoBT = new JButton("Endere�o");
		enderecoBT.addActionListener(enderecoBTAction);
		painelButtons.add(enderecoBT);

		ActionListener salvarBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(ambiente == null){
					throw new TicketsEventosBSIException("Selecione um tipo de ambiente.");
				} else {
					ambiente.setNome(nomeTF.getText());
					ambiente.setDescricao(descricaoTF.getText());
					ambiente.setSetores(AmbienteView.this.setores);
					ambiente.setEndereco(endereco);
					if (ambiente.getClass().equals(Cinema.class)) {
						CinemaBC.getInstance().create((Cinema) ambiente);
						JOptionPane.showInternalMessageDialog(painelFields, "Cinema criado com sucesso!");
					} else if (ambiente.getClass().equals(CasaShow.class)) {
						CasaShowBC.getInstance().create((CasaShow) ambiente);
						JOptionPane.showInternalMessageDialog(painelFields, "Casa de show criada com sucesso!");
					} else if (ambiente.getClass().equals(Teatro.class)) {
						TeatroBC.getInstance().create((Teatro) ambiente);
						JOptionPane.showInternalMessageDialog(painelFields, "Teatro criado com sucesso!");
					} else {
						throw new TicketsEventosBSIException("Selecione um tipo de ambiente.");
					}
				}
				dispose();
			}
		};
		
		JButton salvarBT = new JButton("Salvar");
		salvarBT.addActionListener(salvarBTAction);
		painelButtons.add(salvarBT);
	}

	protected void initializePainelSetores() {
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 10;
		c.anchor = GridBagConstraints.CENTER;

		painelSetoresFields = new JPanel();
		painelSetoresFields.setLayout(new BorderLayout(5, 5));
		painelSetoresFields.setBorder(BorderFactory.createTitledBorder("Setores"));
		painelFields.add(painelSetoresFields, c);
		
		painelSetoresButtons = new JPanel();
		painelSetoresButtons.setLayout(new FlowLayout());
		painelSetoresButtons.setBorder(BorderFactory.createTitledBorder("Acoes"));
		painelSetoresFields.add(painelSetoresButtons, BorderLayout.SOUTH);

		ListSelectionListener listSelectionListener = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				// TODO Auto-generated method stub
				DefaultListSelectionModel jTable = (DefaultListSelectionModel) arg0.getSource();
				indexSetorSelecionado = jTable.getLeadSelectionIndex();
			}
		};
		
		setores = new ArrayList<Setor>();
		setorTableModel = new SetorTableModel(setores);
		jTable = new JTable(setorTableModel);
		jTable.setPreferredScrollableViewportSize(new Dimension(300, 48));
		jTable.setFillsViewportHeight(true);
		jTable.getSelectionModel().addListSelectionListener(listSelectionListener);
		painelSetoresFields.add(new JScrollPane(jTable), BorderLayout.CENTER);
		
		ActionListener incluirSetorBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setorSelecionado = null;
				SetorView setorView = new SetorView(AmbienteView.this);
				desktop.add(setorView);
				try {
					setorView.setSelected(true);
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		};
		
		JButton incluirSetorBT = new JButton("Incluir");
		incluirSetorBT.addActionListener(incluirSetorBTAction);
		painelSetoresButtons.add(incluirSetorBT);

		ActionListener editarSetorBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setorSelecionado = setores.get(indexSetorSelecionado);
				SetorView setorView = new SetorView(AmbienteView.this);
				desktop.add(setorView);
				try {
					setorView.setSelected(true);
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		};
		
		JButton editarSetorBT = new JButton("Editar");
		editarSetorBT.addActionListener(editarSetorBTAction);
		painelSetoresButtons.add(editarSetorBT);
		
		ActionListener excluirSetorBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				/* Obter Setor Selecionado e remover da lista de setores da tabela */
				setores.remove(indexSetorSelecionado);
				jTable.repaint();
			}
		};
		
		JButton excluirSetorBT = new JButton("Excluir");
		excluirSetorBT.addActionListener(excluirSetorBTAction);
		painelSetoresButtons.add(excluirSetorBT);
	}
	
	@Override
	public void repintar() {
		String labelContent = "";
		jTable.repaint();
		if (endereco == null) {
			labelContent = "Endere�o:";
		} else {
			labelContent = "Endere�o: "
					+ "Rua=" + endereco.getRua() + ", "
					+ "N�mero=" + endereco.getNumero() + ", "
					+ "Complemento=" + endereco.getComplemento() + ", "
					+ "Bairro=" + endereco.getBairro() + ", "
					+ "Cidade=" + endereco.getCidade() + ", "
					+ "Estado=" + endereco.getEstado() + ", "
					+ "Pa�s=" + endereco.getPais();
			if (endereco.toString().length() > 40) {
				labelContent = labelContent.substring(0, 37) + "...";
			}
		}
		enderecoLB.setText(labelContent);
		enderecoLB.repaint();
	}

	@Override
	public Endereco getEndereco() {
		return endereco;
	}

	@Override
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public Setor getSetor() {
		// TODO Auto-generated method stub
		return setorSelecionado;
	}

	@Override
	public void setSetor(Setor setor) {
		// TODO Auto-generated method stub
		if(this.setorSelecionado == null) {
			this.setores.add(setor);
		} else {
			this.setores.set(this.indexSetorSelecionado, setor);
		}
	}

	@Override
	public Ambiente getAmbiente() {
		// TODO Auto-generated method stub
		return ambiente;
	}
}
