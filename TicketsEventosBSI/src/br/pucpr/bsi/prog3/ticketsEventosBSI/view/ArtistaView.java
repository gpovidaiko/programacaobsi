package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ArtistaBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Artista;

/**
 * Classe que representa a tela de cadastro de um artista
 * @author mauda
 *
 */
public class ArtistaView extends AbstractInternalFrame {
	
	private static final long serialVersionUID = 2636580625356765061L;
	
	//Normalmente sao declarados apenas campos que sao necessarios para a 
	//obtencao de informacoes como JTextFields, JCombos, JTextAreas
	private JTextField nomeArtistaTF;

	public ArtistaView(){
		super("Artista", true, true, true, true);
		setSize(250, 160);
		setLocation(10, 10);
		setVisible(true);
	}
	
	protected void initializeFields(){
		GridBagConstraints c = new GridBagConstraints();
		
		//*****************************
		// Label Artista
		//*****************************
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel nomeArtistaLB = new JLabel("Nome:");
		
		//Adiciona o label ao panel de campos
		painelFields.add(nomeArtistaLB, c);
		
		//*****************************
		// TextField Artista
		//*****************************
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		nomeArtistaTF = new JTextField(20);
		
		//Adiciona o botao ao panel de campos
		painelFields.add(nomeArtistaTF, c);
	}
	
	protected void initializeButtons(){
		//*****************************
		// Bot�o Salvar
		//*****************************
		
		ActionListener salvarBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Artista artista = new Artista();
				artista.setNome(nomeArtistaTF.getText());
				ArtistaBC.getInstance().create(artista);
				JOptionPane.showInternalMessageDialog(painelFields, "Artista Inserido com Sucesso!");
				dispose();
			}
		};
		
		JButton salvarBT = new JButton("Salvar");
		salvarBT.addActionListener(salvarBTAction);
		
		//Adiciona o botao ao painel de botoes
		painelButtons.add(salvarBT);
	}
}
