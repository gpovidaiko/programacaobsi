package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.ClienteBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Cliente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;

/**
 * Classe que representa a tela de cadastro de um cliente
 * 
 * @author Gledson Povidaiko
 *
 */
public class ClienteView extends AbstractInternalFrame implements PossuiEndereco {

	private static final long serialVersionUID = -5545042975331013776L;

	private JDesktopPane desktop;
	private Endereco endereco;
	private JLabel enderecoLB;
	private JTextField nomeTF;
	private JTextField cpfTF;
	private JTextField emailTF;
	private JTextField telefoneTF;
	private JTextField dataNascimentoTF;
	private JTextField usuarioTF;
	private JTextField senhaTF;
	
	public ClienteView(JDesktopPane desktop) {
		super("Cliente", true, true, true, true);
		setSize(500, 300);
		setLocation(10, 10);
		setVisible(true);
		this.desktop = desktop;
	}

	@Override
	protected void initializeFields() {
		GridBagConstraints c = new GridBagConstraints();
		
		//*****************************
		// Label Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel nomeLB = new JLabel("Nome:");
		painelFields.add(nomeLB, c);
		
		c.gridx = 1;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel cpfLB = new JLabel("CPF:");
		painelFields.add(cpfLB, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel emailLB = new JLabel("Email:");
		painelFields.add(emailLB, c);
		
		c.gridx = 0;
		c.gridy = 4;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel telefoneLB = new JLabel("Telefone:");
		painelFields.add(telefoneLB, c);
		
		c.gridx = 1;
		c.gridy = 4;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel dataNascimentoLB = new JLabel("Data Nascimento:");
		painelFields.add(dataNascimentoLB, c);
		
		c.gridx = 0;
		c.gridy = 6;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel usuarioLB = new JLabel("Usuario:");
		painelFields.add(usuarioLB, c);
		
		c.gridx = 1;
		c.gridy = 6;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel senhaLB = new JLabel("Senha:");
		painelFields.add(senhaLB, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 8;
		c.anchor = GridBagConstraints.NORTHWEST;
		enderecoLB = new JLabel("Endere�o:");
		painelFields.add(enderecoLB, c);
		
		//*****************************
		// TextField Diretor
		//*****************************
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		nomeTF = new JTextField(20);
		painelFields.add(nomeTF, c);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		cpfTF = new JTextField(20);
		painelFields.add(cpfTF, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		emailTF = new JTextField(40);
		painelFields.add(emailTF, c);
		
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 5;
		c.anchor = GridBagConstraints.CENTER;
		telefoneTF = new JTextField(20);
		painelFields.add(telefoneTF, c);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 5;
		c.anchor = GridBagConstraints.CENTER;
		dataNascimentoTF = new JTextField(20);
		painelFields.add(dataNascimentoTF, c);
		
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 7;
		c.anchor = GridBagConstraints.CENTER;
		usuarioTF = new JTextField(20);
		painelFields.add(usuarioTF, c);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 7;
		c.anchor = GridBagConstraints.CENTER;
		senhaTF = new JTextField(20);
		painelFields.add(senhaTF, c);
	}

	@Override
	protected void initializeButtons() {
		ActionListener enderecoBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				EnderecoView enderecoView = new EnderecoView(ClienteView.this);
				desktop.add(enderecoView);
				try {
					enderecoView.setSelected(true);
				} catch (PropertyVetoException e) {
					e.printStackTrace();
				}
			}
		};
		
		JButton enderecoBT = new JButton("Endere�o");
		enderecoBT.addActionListener(enderecoBTAction);
		painelButtons.add(enderecoBT);

		ActionListener salvarBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/mm/yyyy");

				Cliente cliente = new Cliente(endereco);
				cliente.setNome(nomeTF.getText());
				cliente.setCpf(cpfTF.getText());
				cliente.setEmail(emailTF.getText());
				cliente.setTelefone(telefoneTF.getText());
				try {
					cliente.setDataNascimento(simpleDateFormat.parse(dataNascimentoTF.getText()));
				} catch (ParseException e) {
					throw new RuntimeException("A data de nascimento deve estar no formato dd/mm/yyyy.");
				}
				cliente.setUser(usuarioTF.getText());
				cliente.setSenha(senhaTF.getText());
				ClienteBC.getInstance().create(cliente);
				JOptionPane.showInternalMessageDialog(painelFields, "Funcion�rio criado com sucesso!");
				dispose();
			}
		};
		
		JButton salvarBT = new JButton("Salvar");
		salvarBT.addActionListener(salvarBTAction);
		painelButtons.add(salvarBT);
	}

	@Override
	public void repintar() {
		String labelContent = "";
		if (endereco == null) {
			labelContent = "Endere�o:";
		} else {
			labelContent = "Endere�o: "
					+ "Rua=" + endereco.getRua() + ", "
					+ "N�mero=" + endereco.getNumero() + ", "
					+ "Complemento=" + endereco.getComplemento() + ", "
					+ "Bairro=" + endereco.getBairro() + ", "
					+ "Cidade=" + endereco.getCidade() + ", "
					+ "Estado=" + endereco.getEstado() + ", "
					+ "Pa�s=" + endereco.getPais();
		}
		if (endereco.toString().length() > 40) {
			labelContent = labelContent.substring(0, 37) + "...";
		}
		enderecoLB.setText(labelContent);
		enderecoLB.repaint();
	}

	@Override
	public Endereco getEndereco() {
		return endereco;
	}

	@Override
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
