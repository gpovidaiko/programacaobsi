package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsEventosBSI.bc.DiretorBC;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Diretor;

/**
 * Classe que representa a tela de cadastro de um diretor
 * @author diogenes
 *
 */
public class DiretorView extends AbstractInternalFrame {
	
	private static final long serialVersionUID = 2636580625356765061L;
	
	//Normalmente sao declarados apenas campos que sao necessarios para a 
	//obtencao de informacoes como JTextFields, JCombos, JTextAreas
	private JTextField nomeDiretorTF;

	public DiretorView(){
		super("Diretor", true, true, true, true);
		setSize(250, 160);
		setLocation(10, 10);
		setVisible(true);
	}
	
	protected void initializeFields(){
		GridBagConstraints c = new GridBagConstraints();
		
		//*****************************
		// Label Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel nomeDiretorLB = new JLabel("Nome:");
		
		//Adiciona o label ao panel de campos
		painelFields.add(nomeDiretorLB, c);
		
		//*****************************
		// TextField Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		nomeDiretorTF = new JTextField(20);
		
		//Adiciona o botao ao panel de campos
		painelFields.add(nomeDiretorTF, c);
	}
	
	protected void initializeButtons(){
		//*****************************
		// Bot�o Salvar
		//*****************************
		
		ActionListener salvarBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Diretor diretor = new Diretor();
				diretor.setNome(nomeDiretorTF.getText());
				DiretorBC.getInstance().create(diretor);
				JOptionPane.showInternalMessageDialog(painelFields, "Diretor inserido com Sucesso!");
				dispose();
			}
		};
		
		JButton salvarBT = new JButton("Salvar");
		salvarBT.addActionListener(salvarBTAction);
		
		//Adiciona o botao ao painel de botoes
		painelButtons.add(salvarBT);
	}
}

