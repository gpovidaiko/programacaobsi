package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;

/**
 * Classe que representa a tela de cadastro de endere�o
 * 
 * @author Gledson Povidaiko
 *
 */
public class EnderecoView extends AbstractInternalFrame {

	private static final long serialVersionUID = -4368576549077633966L;

	private PossuiEndereco possuiEndereco;
	private JTextField ruaTF;
	private JTextField numeroTF;
	private JTextField complementoTF;
	private JTextField bairroTF;
	private JTextField cidadeTF;
	private JTextField estadoTF;
	private JTextField paisTF;
	
	public EnderecoView(PossuiEndereco possuiEndereco) {
		super("Endere�o", true, true, true, true);
		setSize(500, 300);
		setLocation(10, 10);
		setVisible(true);

		this.possuiEndereco = possuiEndereco;
		if (possuiEndereco.getEndereco() != null) {
			this.populateFields(possuiEndereco.getEndereco());
		}
	}

	@Override
	protected void initializeFields() {
		GridBagConstraints c = new GridBagConstraints();
		
		//*****************************
		// Label Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel ruaLB = new JLabel("Rua:");
		painelFields.add(ruaLB, c);
		
		c.gridx = 2;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel numeroLB = new JLabel("N�mero:");
		painelFields.add(numeroLB, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel complementoLB = new JLabel("Complemento:");
		painelFields.add(complementoLB, c);
		
		c.gridx = 2;
		c.gridy = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel bairroLB = new JLabel("Bairro:");
		painelFields.add(bairroLB, c);
		
		c.gridx = 0;
		c.gridy = 4;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel cidadeLB = new JLabel("Cidade:");
		painelFields.add(cidadeLB, c);
		
		c.gridx = 0;
		c.gridy = 6;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel estadoLB = new JLabel("Estado:");
		painelFields.add(estadoLB, c);
		
		c.gridx = 2;
		c.gridy = 6;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel paisLB = new JLabel("Pa�s:");
		painelFields.add(paisLB, c);
		
		//*****************************
		// TextField Diretor
		//*****************************
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		ruaTF = new JTextField(20);
		painelFields.add(ruaTF, c);
		
		c.gridwidth = 2;
		c.gridx = 2;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		numeroTF = new JTextField(20);
		painelFields.add(numeroTF, c);
		
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		complementoTF = new JTextField(20);
		painelFields.add(complementoTF, c);
		
		c.gridwidth = 2;
		c.gridx = 2;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		bairroTF = new JTextField(20);
		painelFields.add(bairroTF, c);
		
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 5;
		c.anchor = GridBagConstraints.CENTER;
		cidadeTF = new JTextField(40);
		painelFields.add(cidadeTF, c);
		
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 7;
		c.anchor = GridBagConstraints.CENTER;
		estadoTF = new JTextField(20);
		painelFields.add(estadoTF, c);
		
		c.gridwidth = 2;
		c.gridx = 2;
		c.gridy = 7;
		c.anchor = GridBagConstraints.CENTER;
		paisTF = new JTextField(20);
		painelFields.add(paisTF, c);
	}

	@Override
	protected void initializeButtons() {
		ActionListener salvarBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Endereco endereco = new Endereco();
				endereco.setRua(ruaTF.getText());
				endereco.setNumero(Integer.parseInt(numeroTF.getText()));
				endereco.setComplemento(complementoTF.getText());
				endereco.setBairro(bairroTF.getText());
				endereco.setCidade(cidadeTF.getText());
				endereco.setEstado(estadoTF.getText());
				endereco.setPais(paisTF.getText());
				String mensagem = "";
				if (possuiEndereco.getEndereco() == null) {
					mensagem = "Endere�o criado com Sucesso!";
				} else {
					mensagem = "Endere�o atualizado com Sucesso!";
				}
				possuiEndereco.setEndereco(endereco);
				possuiEndereco.repintar();
				JOptionPane.showInternalMessageDialog(painelFields, mensagem);
				dispose();
			}
		};
		
		JButton salvarBT = new JButton("Salvar");
		salvarBT.addActionListener(salvarBTAction);
		painelButtons.add(salvarBT);
	}
	
	private void populateFields(Endereco endereco) {
		ruaTF.setText(endereco.getRua());
		numeroTF.setText(String.valueOf(endereco.getNumero()));
		complementoTF.setText(endereco.getComplemento());
		bairroTF.setText(endereco.getBairro());
		cidadeTF.setText(endereco.getCidade());
		estadoTF.setText(endereco.getEstado());
		paisTF.setText(endereco.getPais());
	}
}
