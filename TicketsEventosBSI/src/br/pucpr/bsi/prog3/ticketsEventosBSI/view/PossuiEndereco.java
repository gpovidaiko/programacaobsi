package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Endereco;

/**
 * Interface utilizada para indicar que a tela necessita de uma instancia de
 * endereco, ou seja, que precisa de um endereco cadastrado para criar uma
 * instancia
 * @author mauda
 *
 */
public interface PossuiEndereco extends Repintar{
	/**
	 * Obtem o endereco
	 * @return
	 */
 	public Endereco getEndereco();
 	
 	/**
 	 * Seta o endereco
 	 * @param endereco
 	 */
	public void setEndereco(Endereco endereco);
}
 
