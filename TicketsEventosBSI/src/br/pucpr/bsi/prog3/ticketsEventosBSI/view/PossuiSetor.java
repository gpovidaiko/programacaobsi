package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Ambiente;
import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;

public interface PossuiSetor extends Repintar {
	Setor getSetor();
	void setSetor(Setor setor);
	Ambiente getAmbiente();
}
