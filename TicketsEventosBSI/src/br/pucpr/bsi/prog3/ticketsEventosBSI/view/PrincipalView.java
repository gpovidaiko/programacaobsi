package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 * Classe que abrigarah o menu do sistema. Realizarah tambem o papel da JDesktopPane
 * para o abrigo as telas de JInternalPane
 * @author Mauda
 *
 */

public class PrincipalView extends JFrame {

	private static final long serialVersionUID = -4945333243653799172L;

	private JDesktopPane desktop;
	
	/**
	 * Construtor da classe
	 */
	public PrincipalView() {
		super("Tickets Eventos BSI");
		iniciarDesktopPane();
		criarMenu();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Ajusta para a 83% da �rea util do desktop
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize((int)(screenSize.getWidth()/1.2), (int)(screenSize.getHeight()/1.2));
	}
	
	/**
	 * Instancia o DesktopPane, o qual eh utilizado para abrir as JInternalFrame
	 */
	private void iniciarDesktopPane() {
		desktop = new JDesktopPane();
		desktop.setBackground(Color.DARK_GRAY);
		getContentPane().add(desktop);
	}
	
	/**
	 * Metodo responsavel por criar o menu do sistema
	 */
	private void criarMenu(){
		JMenuBar menuBar = new JMenuBar();
		//Adiciona o JMenuBar ao JFrame da tela 
		setJMenuBar(menuBar);
		
		//Aqui deverao ser criados todos os JMenu e JItemMenu, de acordo
		//com o documento do laboratorio 01
		
		//CRIA MENU CADASTRO
		JMenu menuCadastro = new JMenu("Cadastro");
		menuCadastro.setMnemonic(KeyEvent.VK_C);
		menuBar.add(menuCadastro);
		
		//CRIA ITEM USUARIO DO MENU CADASTRO
		JMenuItem usuario = new JMenuItem("Usu�rio", KeyEvent.VK_U); 
		usuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
		usuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ClienteView clienteView = new ClienteView(desktop);
				desktop.add(clienteView);
			}
		});
		menuCadastro.add(usuario);			

		//CRIA ITEM FUNCIONARIO DO MENU CADASTRO
		JMenuItem funcionario = new JMenuItem("Funcion�rio", KeyEvent.VK_F); 
		funcionario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
		funcionario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				FuncionarioView funcionarioView = new FuncionarioView(desktop);
				desktop.add(funcionarioView);
			}
		});
		menuCadastro.add(funcionario);
		
		//CRIA ITEM AMBIENTE DO MENU CADASTRO
		JMenuItem ambiente = new JMenuItem("Ambiente", KeyEvent.VK_A); 
		ambiente.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
		ambiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				AmbienteView ambienteView = new AmbienteView(desktop);
				desktop.add(ambienteView);
			}
		});
		menuCadastro.add(ambiente);

				
		//CRIA ITEM EVENTO DO MENU CADASTRO
		JMenuItem evento = new JMenuItem("Evento", KeyEvent.VK_E); 
		evento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
/*		evento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(null, "Metal");
			}
		}); */
		menuCadastro.add(evento);
		
		//CRIA ITEM DIRETOR DO MENU CADASTRO
		JMenuItem diretor = new JMenuItem("Diretor", KeyEvent.VK_D); 
		diretor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
		diretor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DiretorView diretorView = new DiretorView();
				desktop.add(diretorView);
			}
		}); 
		menuCadastro.add(diretor);
		
		//CRIA ITEM ARTISTA DO MENU CADASTRO
		JMenuItem artista = new JMenuItem("Artista", KeyEvent.VK_R); 
		artista.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
		artista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ArtistaView artistaView = new ArtistaView();
				desktop.add(artistaView);
			}
		});
		menuCadastro.add(artista);
		
		//CRIA MENU VENDAS
		JMenu menuVendas = new JMenu("Vendas");
		menuVendas.setMnemonic(KeyEvent.VK_V);
		menuBar.add(menuVendas);
		
		//CRIA ITEM INGRESSO DO MENU VENDAS
		JMenuItem ingresso = new JMenuItem("Ingresso", KeyEvent.VK_I); 
		ingresso.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
		//Adicionando um action listener para o item
/*		ingresso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(null, "Metal");
			}
		}); */
		menuVendas.add(ingresso);
	}
	
	/**
	 * Metodo main pra iniciar a execucao do sistema
	 * @param args
	 */
	public static void main(String[] args) {
		final PrincipalView frame = new PrincipalView();
		frame.setVisible(true);
		
		//Essa parte eh utilizada para obter qualquer excecao gerada no sistema e
		//apresentar ao usuario. Experimente comentar esse codigo e ver o que
		//acontece ao gerar um erro
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
		    public void uncaughtException(Thread t, Throwable e) {
		    	JOptionPane.showMessageDialog(frame, e.getMessage());
		    }
		});
	}
}
