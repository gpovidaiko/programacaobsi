package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

/**
 * Interface utilizada para chamar o metodo de repintar a tela
 * @author mauda
 *
 */
public interface Repintar {
	
	/**
	 * Metodo utilizado para indicar que existe a necessidade de repintar a tela
	 */
 	public void repintar();
}
 
