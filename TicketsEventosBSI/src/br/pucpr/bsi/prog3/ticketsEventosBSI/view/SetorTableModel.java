package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.util.List;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;

/**
 * Classe que representa a TableModel para a tabela de setores que sera
 * utilizada na tela de AmbienteView
 * @author mauda
 *
 */
public class SetorTableModel extends ViewAbstractTableModel<Setor>{
	
	private static final long serialVersionUID = 2636580625356765061L;
	
	public SetorTableModel(List<Setor> setores) {
		super(setores);
		colunas = new String[]{"Nome", "Preco", "Capacidade"};
	}
	
	/**
	 * Valores das colunas
	 * NOME(0), PRECO(1), CAPACIDADE(2)
	 */
	public Object getValueAt(int row, int col) {
		Setor setor = lista.get(row);
		switch (col) {
		case 0:
			return setor.getNome();
		case 1:
			return setor.getPreco();
		case 2:
			return setor.getCapacidade();
		default:
			return null;
		}
	}
	
}
