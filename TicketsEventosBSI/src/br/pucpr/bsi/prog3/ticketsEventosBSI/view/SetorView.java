package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.pucpr.bsi.prog3.ticketsEventosBSI.model.Setor;

/**
 * Classe que representa a tela de cadastro de setor
 * 
 * @author Gledson Povidaiko
 *
 */
public class SetorView extends AbstractInternalFrame {

	private static final long serialVersionUID = -4368576549077633966L;

	private PossuiSetor possuiSetor;
	private JTextField nomeTF;
	private JTextField capacidadeTF;
	private JTextField precoTF;
	
	public SetorView(PossuiSetor possuiSetor) {
		super("Setor", true, true, true, true);
		setSize(500, 300);
		setLocation(10, 10);
		setVisible(true);

		this.possuiSetor = possuiSetor;
		if (possuiSetor.getSetor() != null) {
			this.populateFields(possuiSetor.getSetor());
		}
	}

	@Override
	protected void initializeFields() {
		GridBagConstraints c = new GridBagConstraints();
		
		//*****************************
		// Label Diretor
		//*****************************
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel nomeLB = new JLabel("Nome:");
		painelFields.add(nomeLB, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel capacidadeLB = new JLabel("Capacidade:");
		painelFields.add(capacidadeLB, c);
		
		c.gridx = 1;
		c.gridy = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		JLabel precoLB = new JLabel("Pre�o:");
		painelFields.add(precoLB, c);
		
		//*****************************
		// TextField Diretor
		//*****************************
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.CENTER;
		nomeTF = new JTextField(20);
		painelFields.add(nomeTF, c);
		
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		capacidadeTF = new JTextField(10);
		painelFields.add(capacidadeTF, c);
		
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		precoTF = new JTextField(10);
		painelFields.add(precoTF, c);
	}

	@Override
	protected void initializeButtons() {
		ActionListener salvarBTAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Setor setor = new Setor(possuiSetor.getAmbiente());
				setor.setNome(nomeTF.getText());
				setor.setCapacidade(Integer.parseInt(capacidadeTF.getText()));
				setor.setPreco(Double.parseDouble(precoTF.getText()));
				String mensagem = "";
				if (possuiSetor.getSetor() == null) {
					mensagem = "Setor criado com Sucesso!";
				} else {
					mensagem = "Setor atualizado com Sucesso!";
				}
				possuiSetor.setSetor(setor);
				JOptionPane.showInternalMessageDialog(painelFields, mensagem);
				possuiSetor.repintar();
				dispose();
			}
		};
		
		JButton salvarBT = new JButton("Salvar");
		salvarBT.addActionListener(salvarBTAction);
		painelButtons.add(salvarBT);
	}
	
	private void populateFields(Setor setor) {
		nomeTF.setText(setor.getNome());
		capacidadeTF.setText(String.valueOf(setor.getCapacidade()));
		precoTF.setText(String.valueOf(setor.getPreco()));
	}
}
