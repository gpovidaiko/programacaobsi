package br.pucpr.bsi.prog3.ticketsEventosBSI.view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Classe que representa uma abstracao da classe AbstractTableModel, com
 * algumas caracteristicas de evolu��o de c�digo.
 * 
 * @author mauda
 *
 * @param <E>
 */
public abstract class ViewAbstractTableModel<E> extends AbstractTableModel{
	
	private static final long serialVersionUID = 2636580625356765061L;
	protected List<E> lista;
	
	// Necessario inicializar o array de colunas no construtor da classe filha
	protected String[] colunas;
	
	/**
	 * Construtor basico que recebe a lista de objetos a serem populados na table
	 * @param lista
	 */
	public ViewAbstractTableModel(List<E> lista) {
		this.lista = lista;
	}
	
	@Override
	public int getColumnCount() {
		return colunas.length;
	}
	
	@Override
	public int getRowCount() {
		return lista.size();
	}
	
	@Override
	public String getColumnName(int column) {
		if(column < colunas.length)
			return colunas[column];
		else
			return super.getColumnName(column);
	}	
	
	public E getObjectAtRow(int row){
		return lista.get(row);
	}
	
}
